import numpy as np


def iou(point_1a, point_1b, point_2a, point_2b):
    """
    Calculates IoU (intersection over union) ratio of rectangles.

    :param point_1a: vertex of first rectangle
    :param point_1b: vertex of first rectangle opposite to point_1a
    :param point_2a: vertex of second rectangle
    :param point_2b: vertex of second rectangle opposite to point_2a
    :return: IoU ratio
    """
    if point_1a[0] > point_1b[0]:
        tmp = point_1b
        point_1b = point_1a
        point_1a = tmp

    if point_2a[0] > point_2b[0]:
        tmp = point_2b
        point_2b = point_2a
        point_2a = tmp

    x_1a = point_1a[0]
    y_1a = point_1a[1]

    x_1b = point_1b[0]
    y_1b = point_1b[1]

    x_2a = point_2a[0]
    y_2a = point_2a[1]

    x_2b = point_2b[0]
    y_2b = point_2b[1]

    x_a = max(x_1a, x_2a)
    y_a = max(y_1a, y_2a)

    x_b = min(x_1b, x_2b)
    y_b = min(y_1b, y_2b)

    x_intersection_length = max(x_b - x_a + 1, 0)
    y_intersection_length = max(y_b - y_a + 1, 0)

    intersection_area = x_intersection_length * y_intersection_length

    area_1 = (x_1b - x_1a) * (y_1b - y_1a)
    area_2 = (x_2b - x_2a) * (y_2b - y_2a)

    union_area = area_1 + area_2 - intersection_area

    iou_ratio = intersection_area / union_area

    return iou_ratio


def convert_bbox_to_z(bbox):
    """
    Takes a bounding box in the form [x1,y1,x2,y2] and returns z in the form
    [x,y,s,r] where x,y is the centre of the box and s is the scale/area and r is
    the aspect ratio
    """
    w = bbox[2]-bbox[0]
    h = bbox[3]-bbox[1]
    x = bbox[0]+w/2.
    y = bbox[1]+h/2.
    s = w*h
    r = w/float(h)
    return np.array([x, y, s, r]).reshape((4, 1))


def convert_x_to_bbox(x, score=None):
    """
    Takes a bounding box in the centre form [x,y,s,r] and returns it in the form
    [x1,y1,x2,y2] where x1,y1 is the top left and x2,y2 is the bottom right
    """
    w = np.sqrt(x[2]*x[3])
    h = x[2]/w
    if score is None:
        return np.array([x[0]-w/2., x[1]-h/2., x[0]+w/2., x[1]+h/2.]).reshape((1, 4))
    else:
        return np.array([x[0]-w/2., x[1]-h/2., x[0]+w/2., x[1]+h/2., score]).reshape((1, 5))
