from random import randint

from . import BoxKalmanFilter
from src.tracker_queue import TrackerData
from copy import deepcopy


class IdTracker:
    """
    Class that represents a single tracked identity.
    It tracks and updates the state of single identity (person).
    """

    def __init__(self, feature_vector, detection, tracker_id, producer, camera_id):
        """
        Initialize IdTracker instance using feature_vector and object_detection as initial state of tracker.
        New IdTracker instance should be created only when new person is detected.

        :param feature_vector: initial descriptor for the tracked object
        :param detection: first detection of a new identity
        :param producer: producer used for publishing state of the tracker (feature_vector, id, ...)
        """
        self.object_detection = detection
        self.count = 0
        self.color = (randint(0, 255), randint(0, 255), randint(0, 255))

        self.producer = producer
        self.trackerData = TrackerData(
            tracker_id=tracker_id,
            feature_vector=feature_vector,
            kalman_filter=BoxKalmanFilter(detection.point1 + detection.point2),
            camera_id=camera_id
        )

    def __update_feature_vector(self, feature_vector):
        """Updates feature vector to new state based on feature vector of the last detection."""
        self.trackerData.feature_vector = (0.94 * self.trackerData.feature_vector + 0.06 * feature_vector)

    def __update_ratio(self, detection):
        """Updates kalman filter."""
        self.trackerData.kalman_filter.update(detection.point1 + detection.point2)

    def update(self, feature_vector, detection):
        """Updates the state of tracked object.

        :param feature_vector: feature vector of the last detection of the tracked ID
        :param detection: last detection of the tracked ID
        """
        self.object_detection = detection
        self.__update_feature_vector(feature_vector)
        self.__update_ratio(detection)
        self.count += 1

        if self.count > 20:
            self.producer.enqueue(deepcopy(self.trackerData))

    def get_data(self):
        return self.trackerData
