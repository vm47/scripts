import numpy as np

from . import IdTracker
from ..feature_extraction.utils import squared_euclidean_distance, greedy_match
from .utils import iou
from src.tracker_queue import TrackerQueueFactory
from src.object_detection.base_detector import BaseDetector
from src.feature_extraction.base_feature_extractor import BaseFeatureExtractor


class MultiIdTracker:
    """
    Multi-target tracker. Used to track multiple persons in the entire scene.
    """

    def __init__(self, detector: BaseDetector, descriptor: BaseFeatureExtractor):
        """
        Initialize new multi-target tracker.

        :param detector: detector used for target detection
        :param descriptor: feature extractor used for feature vector extraction
        :param producer: producer used for publishing single person tracker states (person features)
        """
        self.detector = detector
        self.descriptor = descriptor
        self.single_id_trackers = []
        self.count = 0
        self.camera_id = camera_id

        self.producer = TrackerQueueFactory.create_tracker().create_producer()

    @staticmethod
    def __rds(detections, features):
        """
        Redundant detection suppression. If there are two or more detections of the same object this function should
        suppress the one with lower confidence and in that way ensure that there is only one detection of any single
        object. Detections in the output of this function should all represent different objects.

        The algorithm works in the following way: all detections are compared with one another and if their IoU ratio
        is higher than given threshold and feature distance is lower than a given threshold the detection with lower
        confidence is suppressed.

        :param detections: detections outputted directly from detector
        :param features: features of outputted detections
        :return: detections that were not suppressed, features of non-suppressed detections
        """
        iou_threshold = 0.3
        dist_threshold = 100.0
        dist_matrix = squared_euclidean_distance(features, features)

        for i in range(len(detections)):
            det_i = detections[i]
            if det_i:
                for j in range(i+1, len(detections)):
                    det_j = detections[j]
                    if det_j:
                        iou_ratio = iou(det_i.point1, det_i.point2, det_j.point1, det_j.point2)
                        dist = dist_matrix[i, j]
                        if iou_ratio > iou_threshold and dist < dist_threshold:
                            if det_i.confidence > det_j.confidence:
                                detections[j] = None
                            else:
                                detections[i] = None
                                break

        mask = [detection is not None for detection in detections]
        features = features[mask]

        detections = [detection for detection in detections if detection is not None]

        return detections, features

    def update(self, frame):
        """
        Updates state of multi-target tracker based on input frame.
        It does this by updating the states of all internal single-target trackers.

        :param frame: new video frame based on which the state of the tracker will be updated
        """
        detections = self.detector.detect(frame)
        detection_images = [detection.get_object() for detection in detections]
        features = self.descriptor.get_features(detection_images)

        detections, features = MultiIdTracker.__rds(detections, features)

        if self.single_id_trackers:
            old_features = np.vstack([id_tracker.get_data().feature_vector for id_tracker in self.single_id_trackers])

            distance_matrix = squared_euclidean_distance(old_features, features)

            distance_matrix_tmp = np.empty_like(distance_matrix)

            for t_idx, tracker in enumerate(self.single_id_trackers):
                predicted_box_coordinates = tracker.get_data().kalman_filter.predict()
                for d_idx, detection in enumerate(detections):
                    point1_t = (predicted_box_coordinates[0, 0], predicted_box_coordinates[0, 1])
                    point2_t = (predicted_box_coordinates[0, 2], predicted_box_coordinates[0, 3])
                    iou_ratio = iou(point1_t, point2_t, detection.point1, detection.point2)
                    distance_matrix_tmp[t_idx, d_idx] = distance_matrix[t_idx, d_idx] / (iou_ratio + 0.2)

            matches, undetected, new_detections = greedy_match(distance_matrix_tmp, threshold=500.0)

            for tracker_idx, detection_idx in matches:
                self.single_id_trackers[tracker_idx].update(features[detection_idx], detections[detection_idx])

            for new_detection_idx in new_detections:
                new_id_tracker = IdTracker(features[new_detection_idx],
                                           detections[new_detection_idx],
                                           self.count, self.producer, self.camera_id)

                self.single_id_trackers.append(new_id_tracker)
                self.count += 1

            for tracker_idx in reversed(undetected):
                if self.single_id_trackers[tracker_idx].count < 4\
                        or self.single_id_trackers[tracker_idx].get_data().kalman_filter.kf.P[0, 0] > 1100:
                    del self.single_id_trackers[tracker_idx]

        else:
            for detection, feature_vector in zip(detections, features):
                new_id_tracker = IdTracker(feature_vector, detection, self.count, self.producer, self.camera_id)
                self.single_id_trackers.append(new_id_tracker)
                self.count += 1
