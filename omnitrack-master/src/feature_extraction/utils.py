import numpy as np


def squared_euclidean_distance(vectors1, vectors2):
    """Calculates squared euclidean distance of vectors in vectors1 and vectors2. Rows in vectors1 and vectors2 represent
    individual vectors. All vectors should have same dimensions, i.e. vectors1 and vectors2 should have the same
    number of columns.

    :param vectors1: 2D matrix, each row represents a vector
    :param vectors2: 2D matrix, each row represents a vector, vectors1.shape[1]==vectors2.shape[1] should hold
    :returns: 2D matrix of squared euclidean distances, element (i,j) is the distance between
    vector vectors1[i] (i-th row of vectors1) and vector vectors2[j] (j-th row of vectors2)
    """

    n1 = vectors1.shape[0]
    n2 = vectors2.shape[0]

    matrix1 = np.repeat(np.expand_dims(vectors1, axis=0), n2, axis=0)
    matrix2 = np.repeat(np.expand_dims(vectors2, axis=0), n1, axis=0)
    matrix1 = np.swapaxes(matrix1, 0, 1)

    diff = matrix1 - matrix2
    distance = np.sum(np.square(diff), axis=2)

    return distance


def cosine_distance(vectors1, vectors2):
    """Calculates cosine distance of vectors in vectors1 and vectors2. Rows in vectors1 and vectors2 represent
    individual vectors. All vectors should have same dimensions, i.e. vectors1 and vectors2 should have the same
    number of columns.

    :param vectors1: 2D matrix, each row represents a vector
    :param vectors2: 2D matrix, each row represents a vector, vectors1.shape[1]==vectors2.shape[1] should hold
    :returns: 2D matrix of cosine distances, element (i,j) is the distance between
    vector vectors1[i] (i-th row of vectors1) and vector vectors2[j] (j-th row of vectors2)
    """

    norm1 = np.linalg.norm(vectors1, axis=1)
    norm2 = np.linalg.norm(vectors2, axis=1)

    normalized_vector1 = vectors1 / norm1[:, np.newaxis]
    normalized_vector2 = vectors2 / norm2[:, np.newaxis]

    distance = 1 - np.matmul(normalized_vector1, normalized_vector2.transpose())

    return distance


def greedy_match(distance_matrix, threshold):
    """
    Returns matches in form of a list of index pairs, based on distance between the pairs given in distance_matrix.
    Function uses a greedy approach, i.e. it selects a pair with smallest distance as a match and repeats that
    sequentially for all remaining pairs until all pairs are exhausted or a maximum distance threshold is reached.

    :param distance_matrix: numpy matrix of distances between pairs
    :param threshold: maximum distance threshold, all pairs with distance larger than this threshold will not be matched
    :return: list of matched pairs, list of unmatched indices from dimension 0 of distance matrix,
    list of unmatched indices from dimension 1 of distance matrix
    """

    matches = []
    unmatched0 = list(range(0, distance_matrix.shape[0]))
    unmatched1 = list(range(0, distance_matrix.shape[1]))

    while not (len(unmatched0) == 0 or len(unmatched1) == 0) and np.min(distance_matrix) <= threshold:
        min_el = np.min(distance_matrix)
        i, j = np.where(distance_matrix == min_el)
        i, j = i[0], j[0]

        matches.append((i, j))

        unmatched0.remove(i)
        unmatched1.remove(j)

        distance_matrix[i, :] = np.inf
        distance_matrix[:, j] = np.inf

    return matches, unmatched0, unmatched1
