from abc import ABC, abstractmethod


class BaseFeatureExtractor(ABC):
    def __init__(self):
        self.objectDetection = None

    @abstractmethod
    def get_features(self, images):
        pass
