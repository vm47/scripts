import cv2
import torch
from torch import cuda
from torchreid.utils.torchtools import load_pretrained_weights
import numpy
from . import BaseFeatureExtractor


class TorchreidFeatureExtractor(BaseFeatureExtractor):
    """Feature extractor class based on Torchreid. Extracts (description) features from image of a person.
    Uses a PyTorch (Torchreid) model as backend.
    """

    def __init__(self, args):
        """
        Initialize feature extractor instance.

        :param args: dictionary with arguments:
         - model: PyTorch (Torchreid) model
         - weights_path: path to pretrained weights file
         - img_size: tuple (height, width) used for resizing model input image
         - use_cuda: boolean variable, if true cuda will be used
        """

        self.use_cuda = args['use_cuda']
        self.img_size = args['img_size']

        self.model = self.__transform_to_cuda(args['model'])
        self.model.eval()  # set model to evaluation mode

        self.__load_weights(args['weights_path'])

    def __transform_to_cuda(self, arg):
        """Transforms arg to CUDA PyTorch tensor, i.e. moves the tensor from CPU to GPU
        if use_cuda==True and cuda is available.

        :param arg: any object from PyTorch that can be moved to GPU
        :return: cuda version of arg (arg.cuda()) if use_cuda==True and cuda is available, otherwise return arg itself
        """

        if self.use_cuda and cuda.is_available():
            return arg.cuda()

        return arg

    def __load_weights(self, weights_path):
        """Load specified weights to model.

        :param weights_path: path to a file containing weights
        """

        load_pretrained_weights(self.model, weights_path)

    def get_features(self, img: numpy.ndarray):
        """Computes features for input image.

        :param numpy array img: image of a person for which features will be computed
        :return numpy array: features of the input image
        """

        img = cv2.resize(img, (self.img_size[1], self.img_size[0]), interpolation=cv2.INTER_AREA)

        # move color chanel dimension from index 2 (-1) to index 0
        img = numpy.moveaxis(img, -1, 0)

        tensor_img = torch.from_numpy(img).type('torch.FloatTensor').unsqueeze(dim=0)
        tensor_img = self.__transform_to_cuda(tensor_img)

        tensor_features = self.model(tensor_img)
        numpy_features = tensor_features.detach().cpu().numpy()

        return numpy_features
