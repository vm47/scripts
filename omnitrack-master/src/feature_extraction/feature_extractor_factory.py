# from .torchreid_feature_extractor import TorchreidFeatureExtractor
from .triplet_feature_extractor import TripletFeatureExtractor
# from .dgnet_feature_extractor import Dgnet_feature_extractor
from enum import Enum


class FeatureExtractorFactory:
    class ExtractorType(Enum):
        TORCHREID = 1
        TRIPLET = 2
        DGNET = 3

    @staticmethod
    def create_extractor(extractor_type, args):
        if extractor_type is FeatureExtractorFactory.ExtractorType.TORCHREID:
            return None  #  TorchreidFeatureExtractor(args)
        elif extractor_type is FeatureExtractorFactory.ExtractorType.TRIPLET:
            return TripletFeatureExtractor(args)
        elif extractor_type is FeatureExtractorFactory.ExtractorType.DGNET:
            return None  # RETURN DGNET FEATURE EXTRACTOR AFTER MERGE
        else:
            raise ValueError('Feature extractor type: ' + str(extractor_type) + ' not supported!')
