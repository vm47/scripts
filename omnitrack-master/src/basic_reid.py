import cv2
import argparse

from object_detection import Yolov3Detector
from feature_extraction import TripletFeatureExtractor
from feature_extraction.utils import squared_euclidean_distance, greedy_match



parser = argparse.ArgumentParser(description="Example of a simple reid tracking system")

parser.add_argument("--video-path", required=True, type=str, metavar="PATH", help="path to a video to be processed")
parser.add_argument("--protobuf-path", required=True, type=str, metavar="PATH", help="path to a protobuf file")
parser.add_argument("--detectable-classes-path", required=True, type=str, metavar="PATH",
                    help="path to a file containing detectable classes")
parser.add_argument("--input-size", default=(416,416), type=tuple,
                    help="size (height, width) to which input image will be"
                         "resized before being passed to the detection network")

parser.add_argument("--model-name", default='resnet_v1_50', type=str, help="name of feature extractor model")
parser.add_argument("--head-name", default='fc1024', type=str, help="name of the head of feature extractor model")
parser.add_argument("--extractor-weights-path", required=True, type=str, metavar="PATH",
                    help="path to a file containing extractor weights")


args = parser.parse_args()


protobuf_file = args.protobuf_path
detectable_classes_file = args.detectable_classes_path
detection_classes = ['person']

detector = Yolov3Detector(protobuf_file, detectable_classes_file, detection_classes, args.input_size)

model_name = args.model_name
head_name = args.head_name
extractor_weights_file = args.extractor_weights_path

feature_extractor = TripletFeatureExtractor(model_name, head_name, extractor_weights_file)


video_path = args.video_path

cap = cv2.VideoCapture(video_path)

pid = None

while(cap.isOpened()):
    ret, frame = cap.read()

    detections = detector.detect(frame)

    images = [detection.get_object() for detection in detections]
    features = feature_extractor.get_features(images)

    if pid:
        matches, _, _ = greedy_match(squared_euclidean_distance(old_feature, features), threshold=2000.0)
        idx1, idx2 = matches[0]
        pid = detections[idx2]
        old_feature = features[[idx2], :]
    else:
        pid = detections[1]
        old_feature = features[[1], :]

    cv2.rectangle(frame, pid.point1, pid.point2, (0, 255, 0), 1)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
