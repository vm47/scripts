class ObjectDetection:
    """
    Class represents object detection instances.
    All information about object detection instance is contained in this class.
    """

    def __init__(self, class_type, confidence, point1, point2, image):
        """
        Initialize object detection instance.

        :param class_type: class of detected object (for example 'dog', 'cat', 'car', ...)
        :param confidence: confidence of detection
        :param point1: vertex of detection rectangle
        :param point2: vertex of detection rectangle opposite to p1
        :param image: image in which object was detected
        """

        self.class_type = class_type

        self.confidence = confidence

        self.point1 = point1
        self.point2 = point2

        self.image = image

    def get_object(self):
        """
        :return: subarray (subimage) of detected object
        """

        x1, y1 = self.point1
        x2, y2 = self.point2

        return self.image[y1:y2, x1:x2]
