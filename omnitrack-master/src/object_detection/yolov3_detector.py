import cv2
import numpy as np
import tensorflow as tf

from .tensorflow_yolov3.core.utils import read_coco_names, read_pb_return_tensors, cpu_nms
from .base_detector import BaseDetector


class Yolov3Detector(BaseDetector):
    """
    Class representing YOLO v3 object detector. The class is implemented in Tensorflow.
    Architecture and configuration of the network is specified in a set of configuration files
    which are passed as arguments to constructor.
    """

    def __init__(self, args):
        """
        Initialize YOLO v3 detector.

        :param args: dictionary with arguments:
         - protobuf_file: path to protobuf file which contains architecture and weights of the detection network
         - detectable_classes_file: path to file containing list of classes network was trained to detect
         - detection_classes: list of classes network should detect during inference, should be a
                                  subset of detectable classes (classes contained in detectable_classes_file)
         - network_input_size: size (height, width) to which input image will be resized before being passed to
                                   the detection network
        """
        self.input_tensor, self.output_tensor = read_pb_return_tensors(tf.get_default_graph(), args['protobuf_file'],
                                                                       ["Placeholder:0", "concat_9:0", "mul_6:0"])

        self.classes = read_coco_names(args['detectable_classes_file'])
        self.detection_classes = args['detection_classes']
        self.num_classes = len(self.classes)
        self.network_input_size = args['network_input_size']
        self.sess = tf.Session()

    def __transform_points(self, box, img_size):
        """
        Helper function used to transform detections from coordinates
        of resized image to coordinates of the original image.

        :param box: detection that is output directly from detection network
        :param img_size: size of the original image in format (height, width)
        :return: two points of detected object in coordinate system of the original (non-resized) image
        """
        for i, coordinate in enumerate(box):
            box[i] = max(coordinate, 0)

        p1 = (int(box[0] * img_size[1] / self.network_input_size[0]),
              int(box[1] * img_size[0] / self.network_input_size[1]))

        p2 = (int(box[2] * img_size[1] / self.network_input_size[0]),
              int(box[3] * img_size[0] / self.network_input_size[1]))

        return p1, p2

    def detect(self, image):
        """Detect objects specified in detection_classes.

        :param image: image in which objects should be detected
        :return: list of object detections (ObjectDetection instances)
        """

        network_input_height = self.network_input_size[0]
        network_input_width = self.network_input_size[1]
        img_resized = cv2.resize(image, (network_input_width, network_input_height))
        img_resized = img_resized / 255.

        boxes, scores = self.sess.run(self.output_tensor, feed_dict={self.input_tensor: np.expand_dims(img_resized,
                                                                                                       axis=0)})

        boxes, scores, labels = cpu_nms(boxes, scores, self.num_classes, score_thresh=0.9, iou_thresh=0.5)

        labels = [self.classes[label] for label in labels]

        object_detections = []

        for box, label, score in zip(boxes, labels, scores):
            if label in self.detection_classes:
                p1, p2 = self.__transform_points(box, image.shape)
                object_detection = BaseDetector.create_object_detection(label, score, p1, p2, image)
                object_detections.append(object_detection)

        return object_detections
