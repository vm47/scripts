from abc import ABC, abstractmethod
from .object_detection import ObjectDetection


class BaseDetector(ABC):
    def __init__(self):
        self.objectDetection = None

    @abstractmethod
    def detect(self, image):
        pass

    @staticmethod
    def create_object_detection(class_type, confidence, point1, point2, image):
        return ObjectDetection(class_type, confidence, point1, point2, image)
