import cv2
import numpy as np
from .base_detector import BaseDetector


class YoloDetector(BaseDetector):
    """
    Class representing YOLO object detector. Architecture and configuration of the network
    is specified in a set of configuration files which are passed as arguments to constructor.
    """

    def __init__(self, args):  # weights_file, config_file, detectable_classes_file, detection_classes):
        """
        Initialize YOLO detector (architecture is specified in input files).

        :param args: dictionary with arguments:
         - config_file: path to file containing network architecture configuration
         - weights_file: path to file containing network weights
         - detectable_classes_file: path to file containing list of classes network was trained to detect
         - detection_classes: list of classes network should detect during inference, should be a
                subset of detectable classes (classes contained in detectable_classes_file)
        """

        self.yolo_net = cv2.dnn.readNet(args['weights_file'], args['config_file'])

        with open(args['detectable_classes_file'], 'r') as f:
            self.classes = [line.strip() for line in f.readlines()]

        self.detection_classes = args['detection_classes']

    def get_output_layers(self):
        layer_names = self.yolo_net.getLayerNames()

        output_layers = [layer_names[i[0] - 1] for i in self.yolo_net.getUnconnectedOutLayers()]

        return output_layers

    @staticmethod
    def __process_yolo_detection(detection, img_shape):
        """Processes detection and returns detection information if detection confidence is greater than 0.5,
        else returns None.

        :param detection: unprocessed detection outputted from YOLO network
        :param img_shape: shape of input image (height, width)
        :return: (class_id, confidence, rectangle) tuple if confidence greater than 0.5, else None
        """

        height, width = img_shape
        scores = detection[5:]
        class_id = np.argmax(scores)
        confidence = scores[class_id]

        if confidence > 0.5:
            center_x = int(detection[0] * width)
            center_y = int(detection[1] * height)
            w = int(detection[2] * width)
            h = int(detection[3] * height)
            x = center_x - w / 2
            y = center_y - h / 2
            rectangle = [x, y, w, h]

            return class_id, confidence, rectangle

        else:
            return None

    @staticmethod
    def __transform_box_to_object_detection_instance(box, image, class_type):
        """Constructs an ObjectDetection instance from detection box [x, y, w, h]."""

        height, width, _ = image.shape

        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        x1 = max(0, round(x))
        y1 = max(0, round(y))
        x2 = min(width, round(x + w))
        y2 = min(height, round(y + h))

        object_detection = BaseDetector.create_object_detection(class_type, (x1, y1), (x2, y2), image)

        return object_detection

    def detect(self, image):
        """Detect objects specified in detection_classes.

        :param image: image in which objects should be detected
        :return: list of object detections (ObjectDetection instances)
        """

        height, width, _ = image.shape

        blob = cv2.dnn.blobFromImage(image, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
        self.yolo_net.setInput(blob)

        outs = self.yolo_net.forward(self.get_output_layers())

        class_ids = []
        confidences = []
        boxes = []
        conf_threshold = 0.5
        nms_threshold = 0.4

        # Process detections from YOLO network
        for out in outs:
            for detection in out:
                processed_detection = YoloDetector.__process_yolo_detection(detection, (height, width))
                if processed_detection:
                    class_id, confidence, rectangle = processed_detection
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append(rectangle)

        # Perform non maximum suppression (NMS) to eliminate multiple or conflicting detections of the same object
        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

        object_detections = []

        # Transform detections that are left after NMS from box format to ObjectDetection class format
        for i in indices:
            i = i[0]
            class_type = self.classes[class_ids[i]]
            if class_type in self.detection_classes:
                box = boxes[i]
                object_detection = YoloDetector.__transform_box_to_object_detection_instance(box, image, class_type)
                object_detections.append(object_detection)

        return object_detections
