import cv2
import tensorflow as tf
from darkflow.net.build import TFNet
from .base_detector import BaseDetector


class DarkflowDetector(BaseDetector):
    """
    Class representing YOLO object detector. The class is implemented in Darkflow (Tensorflow)
    Architecture and configuration of the network is specified in a set of configuration files
    which are passed as arguments to constructor.
    """

    def __init__(self, args):
        """
        Initialize YOLO detector (architecture is specified in input files).

        :param args: dictionary with arguments:
         - config_file: path to file containing network architecture configuration
         - weights_file: path to file containing network weights
         - detectable_classes_file: path to file containing list of classes network was trained to detect
         - detection_classes: list of classes network should detect during inference, should be a
                subset of detectable classes (classes contained in detectable_classes_file)
        """
        options = {"model": args['config_file'],
                   "load": args['weights_file'],
                   "threshold": 0.5,
                   "config": args['detectable_classes_file'],
                   "gpu": 1.0}

        self.detection_classes = args['detection_classes']

        self.tfnet = TFNet(options)

    @staticmethod
    def __transform_json_to_object_detection_instance(json_detection, image):
        """Constructs an ObjectDetection instance from json format returned by Darkflow."""
        point1 = (json_detection['topleft']['x'], json_detection['topleft']['y'])
        point2 = (json_detection['bottomright']['x'], json_detection['bottomright']['y'])
        detection = BaseDetector.create_object_detection(json_detection['label'], point1, point2, image)

        return detection

    def detect(self, image):
        """Detect objects specified in detection_classes.

        :param image: image in which objects should be detected
        :return: list of object detections (ObjectDetection instances)
        """

        darkflow_format_detections = self.tfnet.return_predict(image)

        filtered_detections = [detection for detection in darkflow_format_detections
                               if detection['label'] in self.detection_classes]

        detections = [DarkflowDetector.__transform_json_to_object_detection_instance(detection, image)
                      for detection in filtered_detections]

        return detections
