# from .darkflow_detector import DarkflowDetector
from .yolo_detector import YoloDetector
from .yolov3_detector import Yolov3Detector
from enum import Enum


class ObjectDetectorFactory:
    """
    Class for creating different kinds of Object detectors
    """

    class DetectorType(Enum):
        DARKFLOW = 1
        YOLO = 2
        YOLOV3 = 3

    @staticmethod
    def create_detector(detector_type, args):
        """
        :param detector_type: Enum element for defining which type of object detector to create
        :param args: Dictionary with constructor arguments for specific object detector
        """
        if detector_type is ObjectDetectorFactory.DetectorType.DARKFLOW:
            return None  # DarkflowDetector(args)
        elif detector_type is ObjectDetectorFactory.DetectorType.YOLO:
            return YoloDetector(args)
        elif detector_type is ObjectDetectorFactory.DetectorType.YOLOV3:
            return Yolov3Detector(args)
        else:
            raise ValueError('Detector type: ' + str(detector_type) + ' not supported!')
