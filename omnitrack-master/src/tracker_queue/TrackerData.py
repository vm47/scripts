from dataclasses import dataclass
from src.tracking.box_kalman_filter import BoxKalmanFilter
from numpy import ndarray


@dataclass
class TrackerData:
    tracker_id: int
    feature_vector: ndarray
    kalman_filter: BoxKalmanFilter
    camera_id: int
