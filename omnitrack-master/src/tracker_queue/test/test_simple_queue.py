from . import TrackerQueueFactory
from unittest import TestCase, main


class TestSimpleQueue(TestCase):
    def setUp(self):
        TrackerQueueFactory.TRACKER_TYPE = 'SIMPLE'
        self.data = [0, 127, 255, 65535]

        queue = TrackerQueueFactory.create_tracker()
        self.producer = queue.create_producer()
        self.consumer = queue.get_consumer()

    def test_01_deque_empty_queue(self):
        self.assertEqual(self.consumer.dequeue(), None)

    def test_02_deque(self):
        self.producer.enqueue([0, 127, 255, 65535])
        result = self.consumer.dequeue()

        self.assertEqual(result, self.data)

    def test_03_multiple_deque(self):
        for data in self.data:
            self.producer.enqueue(data)

        self.assertEqual(self.consumer.dequeue(), 0)
        self.assertEqual(self.consumer.dequeue(), 127)
        self.assertEqual(self.consumer.dequeue(), 255)
        self.assertEqual(self.consumer.dequeue(), 65535)
        self.assertEqual(self.consumer.dequeue(), None)


if __name__ == '__main__':
    main()
