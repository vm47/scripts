from queue import Queue
from .TrackerQueue import TrackerQueue, TrackerQueueProducer, TrackerQueueConsumer


class SimpleTrackerQueue(TrackerQueue):
    class SimpleTrackerProducer(TrackerQueueProducer):
        def __init__(self, queue):
            self.queue = queue

        def enqueue(self, data):
            self.queue.put(data)

    class SimpleTrackerConsumer(TrackerQueueConsumer):
        def __init__(self, queue):
            self.queue = queue

        def dequeue(self):
            try:
                return self.queue.get(block=False)
            except Exception:
                return None

    __queue = None

    @staticmethod
    def create_producer():
        SimpleTrackerQueue.__init_queue()
        return SimpleTrackerQueue.SimpleTrackerProducer(SimpleTrackerQueue.__queue)

    @staticmethod
    def get_consumer():
        SimpleTrackerQueue.__init_queue()
        return SimpleTrackerQueue.SimpleTrackerConsumer(SimpleTrackerQueue.__queue)

    @staticmethod
    def __init_queue():
        if SimpleTrackerQueue.__queue is None:
            SimpleTrackerQueue.__queue = Queue()
