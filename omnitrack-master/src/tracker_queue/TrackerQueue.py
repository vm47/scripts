from abc import ABC, abstractmethod


class TrackerQueue(ABC):
    """
    Abstract class used to define methods to implement. It creates producers (entity that enqueues data) and consumers
    (entity that dequeue data). In general one queue is used where multiple producers enqueue and a single consumer
    dequeue data.
    """
    @staticmethod
    @abstractmethod
    def create_producer():
        """
        Abstract static method for producer creation.
        :return: Producer that should implement abstract class TrackerQueueProducer
        """
        pass

    @staticmethod
    @abstractmethod
    def get_consumer():
        """
        Abstract static method for getting consumer.
        :return: Consumer that should implement abstract class TrackerQueueConsumer
        """
        pass


class TrackerQueueProducer(ABC):
    """
    Abstract class used to define methods to implement.
    """
    @abstractmethod
    def enqueue(self, data):
        pass


class TrackerQueueConsumer(ABC):
    """
    Abstract class used to define methods to implement.
    """
    @abstractmethod
    def dequeue(self):
        pass
