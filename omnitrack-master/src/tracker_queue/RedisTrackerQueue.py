from .TrackerQueue import TrackerQueue, TrackerQueueProducer, TrackerQueueConsumer
from redis import StrictRedis
from pickle import dumps, loads


class RedisTrackerQueue(TrackerQueue):
    _redis_connection = StrictRedis(
        host='localhost',
        port=6379,
        password='',
        decode_responses=True
    )
    _redis_key = 'single-camera-features'
    _cnt = 0

    class RedisProducer(TrackerQueueProducer):
        def __init__(self):
            self.producer = RedisTrackerQueue._redis_connection

        def enqueue(self, data):
            self.producer.rpush(RedisTrackerQueue._redis_key, str(dumps(data)))

    class RedisConsumer(TrackerQueueConsumer):
        def __init__(self):
            self.consumer = RedisTrackerQueue._redis_connection

        def dequeue(self):
            data = self.consumer.lpop(RedisTrackerQueue._redis_key)

            if data is not None:
                data = loads(eval(data))

            return data

    @staticmethod
    def create_producer():
        return RedisTrackerQueue.RedisProducer()

    @staticmethod
    def get_consumer():
        return RedisTrackerQueue.RedisConsumer()
