from .TrackerQueue import TrackerQueue, TrackerQueueProducer, TrackerQueueConsumer
import redislite
from pickle import dumps, loads


class RedisLiteTrackerQueue(TrackerQueue):
    _redis_connection = redislite.StrictRedis()
    _redis_key = 'single-camera-features'
    _cnt = 0

    class RedisLiteProducer(TrackerQueueProducer):
        def __init__(self):
            self.producer = RedisLiteTrackerQueue._redis_connection

        def enqueue(self, data):
            self.producer.rpush(RedisLiteTrackerQueue._redis_key, str(dumps(data)))

    class RedisLiteConsumer(TrackerQueueConsumer):
        def __init__(self):
            self.consumer = RedisLiteTrackerQueue._redis_connection

        def dequeue(self):
            data = self.consumer.lpop(RedisLiteTrackerQueue._redis_key)

            if data is not None:
                data = loads(eval(data))

            return data

    @staticmethod
    def create_producer():
        return RedisLiteTrackerQueue.RedisLiteProducer()

    @staticmethod
    def get_consumer():
        return RedisLiteTrackerQueue.RedisLiteConsumer()
