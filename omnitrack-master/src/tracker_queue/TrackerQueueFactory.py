from .KafkaTrackerQueue import KafkaTrackerQueue
from .SimpleTrackerQueue import SimpleTrackerQueue
from .RedisTrackerQueue import RedisTrackerQueue
from .RedisLiteTrackerQueue import RedisLiteTrackerQueue


class TrackerQueueFactory:

    # TRACKER_TYPE = 'KAFKA'
    TRACKER_TYPE = 'SIMPLE'
    # TRACKER_TYPE = 'REDIS'
    # TRACKER_TYPE = 'REDIS_LITE'

    @staticmethod
    def create_tracker():
        if TrackerQueueFactory.TRACKER_TYPE == 'KAFKA':
            return KafkaTrackerQueue()
        elif TrackerQueueFactory.TRACKER_TYPE == 'SIMPLE':
            return SimpleTrackerQueue()
        elif TrackerQueueFactory.TRACKER_TYPE == 'REDIS':
            return RedisTrackerQueue()
        elif TrackerQueueFactory.TRACKER_TYPE == 'REDIS_LITE':
            return RedisLiteTrackerQueue()
        else:
            raise ValueError('Tracker type: ' + str(TrackerQueueFactory.TRACKER_TYPE) + ' not supported!')
