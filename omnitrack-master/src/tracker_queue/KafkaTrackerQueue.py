from .TrackerQueue import TrackerQueue, TrackerQueueProducer, TrackerQueueConsumer
from kafka import KafkaProducer, KafkaConsumer
from pickle import dumps, loads
from dataclasses import dataclass
from typing import Any


BOOTSTRAP_SERVERS = ['localhost:9092']
SERIALIZER = dumps
DESERIALIZER = loads
TOPIC_NAME = 'single-camera-features'
GROUP_NAME = 'my-group'


class KafkaTrackerQueue(TrackerQueue):
    class KafkaTrackerProducer(TrackerQueueProducer):
        def __init__(self, bootstrap_servers, value_serializer, topic_name):

            self.producer = KafkaProducer(bootstrap_servers=bootstrap_servers, value_serializer=value_serializer)
            self.topic_name = topic_name

        def enqueue(self, data):
            self.producer.send(self.topic_name, data)
            self.producer.flush()

    class KafkaTrackerConsumer(TrackerQueueConsumer):
        def __init__(self, bootstrap_servers, value_deserializer, topic_name, group_name):

            self.consumer = KafkaConsumer(
                topic_name,
                bootstrap_servers=bootstrap_servers,
                auto_offset_reset='earliest',
                enable_auto_commit=True,
                group_id=group_name,
                value_deserializer=lambda x: value_deserializer(x),
                consumer_timeout_ms=1000
            )

            self.record_iterator = iter(self.consumer)

        def dequeue(self):
            next_item = next(self.record_iterator, None)

            if next_item is not None:
                next_item = next_item.value

            return next_item

    @dataclass
    class KafkaConfig:
        bootstrap_servers: list
        value_serializer: Any
        value_deserializer: Any
        topic_name: str
        group_name: str


    @staticmethod
    def get_config():
        # TO_DO: Read from file
        return KafkaTrackerQueue.KafkaConfig(
            bootstrap_servers=BOOTSTRAP_SERVERS,
            value_serializer=SERIALIZER,
            value_deserializer=DESERIALIZER,
            topic_name=TOPIC_NAME,
            group_name=GROUP_NAME
        )

    @staticmethod
    def create_producer():
        config = KafkaTrackerQueue.get_config()
        return KafkaTrackerQueue.KafkaTrackerProducer(
            bootstrap_servers=config.bootstrap_servers,
            value_serializer=config.value_serializer,
            topic_name=config.topic_name
        )

    @staticmethod
    def get_consumer():
        config = KafkaTrackerQueue.get_config()
        return KafkaTrackerQueue.KafkaTrackerConsumer(
            bootstrap_servers=config.bootstrap_servers,
            value_deserializer=config.value_deserializer,
            topic_name=config.topic_name,
            group_name=config.group_name
        )
