#!/bin/bash

mkdir src/parameters
mkdir src/parameters/market1501_weights

wget -P src/parameters https://storage.googleapis.com/hs-shared/parameters/coco.names
wget -P src/parameters https://storage.googleapis.com/hs-shared/parameters/yolov3_cpu_nms.pb

wget -P src/parameters/market1501_weights https://storage.googleapis.com/hs-shared/parameters/market1501_weights/checkpoint-25000.data-00000-of-00001
wget -P src/parameters/market1501_weights https://storage.googleapis.com/hs-shared/parameters/market1501_weights/checkpoint-25000.index
wget -P src/parameters/market1501_weights https://storage.googleapis.com/hs-shared/parameters/market1501_weights/checkpoint-25000.meta
