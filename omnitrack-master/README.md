# OMNITRACK

Multiple camera person / object tracker.


## Installation instructions

Install Anaconda for Python3.7 (https://www.anaconda.com/distribution/)

Create conda environment:
conda create --name tracker

Activate tracker environment:
conda activate tracker

Instal setup tools:
conda install -c anaconda setuptools

Install numpy:
pip install numpy

Install opencv:
pip install opencv-python

Install cython:
conda install -c anaconda cython 

Install tensorflow with gpu support:
conda install -c anaconda tensorflow-gpu

Install filterpy (contains kalman filter):
conda install -c conda-forge filterpy

Download neural network weights:
./download_weights.sh

## Apache Kafka setup

Apache kafka depends on Java. OpenJDK can be installed from the official package repository with the following command:
```
sudo apt install openjdk-8-jdk
```

Next it needs zookeeper that also can be installed from the official package repository. Execute command:
```
sudo apt install zookeeperd
```
After zookeeper is successfully installed it is possible to:
 * check zookeeper status:              `sudo systemctl status zookeeper`
 * start zookeeper:                     `sudo systemctl start zookeeper`
 * start zookeeper on system startup:   `sudo systemctl enable zookeeper`

Now kafka can be downloaded with the command:
```
wget http://ftp.carnet.hr/misc/apache/kafka/2.2.0/kafka_2.12-2.2.0.tgz
```
Create a directory **Kafka/** in the **/opt** directory with the following command:
```
sudo mkdir /opt/Kafka
```

To extract the Apache Kafka archive in the **/opt/Kafka** directory run the command:
```
sudo tar xvzf kafka_2.12-2.2.0.tgz -C /opt/Kafka
```

After the archive is extracted open **/etc/profile** with the command:
```
sudo nano /etc/profile
```

Add the following lines to the end of the file and save it.
```
export KAFKA_HOME="/opt/Kafka/kafka_2.12-2.2.0"
export PATH="$PATH:${KAFKA_HOME}/bin"
```

Now open the ~/.bashrc file with the following command:
```
sudo nano ~/.bashrc
```

Add the following line to the end of the file and save it.
```
alias sudo='sudo env PATH="$PATH"'
```

Now restart your computer

Check if kafka variables are successfully added:
 * `echo $KAFKA_HOME`
 * `echo $PATH`
 
Make a symbolic link of Kafka server.properties file as follows:
```
sudo ln -s $KAFKA_HOME/config/server.properties /etc/kafka.properties
```

## Apache Kafka usage

Start Apache Kafka server with the following command:
```sudo kafka-server-start.sh /etc/kafka.properties```

Create a test Topic **testing** on Apache Kafka server with the following command:
```
sudo kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic testing
```
Now run the following command to use Kafka Producer API to send some message to the **testing** topic:
```
sudo kafka-console-producer.sh --broker-list localhost:9092 --topic testing
```
Once you press *Enter* you should see a new greater than (`>`) sign.
Just type in something and press *Enter* to start a new line.

Now you can use the Kafka Consumer API to fetch the messages/lines from the testing topic with the following command:
```
sudo kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testing --from-beginning
```
You should be able to see the messages or lines you have written using the Producer.
If you write a new message using the Producer API, it should also be displayed instantly on the Consumer side.

## Redis setup

To install Redis go to the terminal and type the following command:
```
sudo apt-get install redis-server
```

Start redis:
```
redis-server
```
