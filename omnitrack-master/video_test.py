import cv2
import os
from os import listdir
import numpy as np
from random import randint
import time

from src.object_detection import ObjectDetectorFactory
from src.feature_extraction import TripletFeatureExtractor
from src.feature_extraction.utils import squared_euclidean_distance, greedy_match
from src.tracking import MultiIdTracker


protobuf_file = './src/parameters/yolov3_cpu_nms.pb'
detectable_classes_file = './src/parameters/coco.names'
detection_classes = ['person']

detector = ObjectDetectorFactory.create_detector(protobuf_file, detectable_classes_file, detection_classes)


model_name = 'resnet_v1_50'
head_name = 'fc1024'
extractor_weights_file = './src/parameters/market1501_weights/checkpoint-25000'

feature_extractor = TripletFeatureExtractor(model_name, head_name, extractor_weights_file)


dir_path = '/home/toni/Downloads/Crowd_PETS09/S2/L1/Time_12-34/View_008'

frame_names = listdir(dir_path)
frame_names.sort()
paths = [os.path.join(dir_path, name) for name in frame_names]

tracker = MultiIdTracker(detector, feature_extractor)


for path in paths:
    frame = cv2.imread(path)

    detections = detector.detect(frame)

    tracker.update(frame)

    for id_tracker in tracker.single_id_trackers:
        cv2.rectangle(frame, id_tracker.object_detection.point1, id_tracker.object_detection.point2, id_tracker.color, 2)
        cv2.putText(frame, str(id_tracker.id), id_tracker.object_detection.point1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.0, id_tracker.color)
        #time.sleep(0.02)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
