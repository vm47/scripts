#!/usr/bin/env python3
import argparse
import os
from pathlib import Path
import textwrap

DATA_PATH_NAME = 'data_paths.txt'


def main() -> None:
    args = parse_arguments()
    input_dir = args.input_dir
    output_dir = args.input_dir if args.output_dir is None else args.output_dir
    data_paths_name = args.name

    check_dir_existence(input_dir)
    check_dir_existence(output_dir)
    append_to_datapaths_file(input_dir, output_dir, data_paths_name)


def parse_arguments() -> argparse.Namespace:
    global DATA_PATH_NAME
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Create data paths file.', epilog=generate_epilog())
    parser.add_argument('-i', '--input', dest='input_dir', help='Path to directory containing images', required=True)
    parser.add_argument('-n', '--name', dest='name', help=f'Data paths file name (Default: {DATA_PATH_NAME})',
                        default=DATA_PATH_NAME)
    parser.add_argument('-o', '--output', dest='output_dir', help='Path to output directory (Default: same as input)')

    return parser.parse_args()


def generate_epilog() -> str:
    return textwrap.dedent('''
    Steps involved:
     - Creates data paths file if doesn't exist
     - Appends absolute paths to images in data paths file.
    ''')


def check_dir_existence(dir_path: str) -> None:
    dir = Path(dir_path)

    if not dir.exists():
        raise Exception(f'{dir_path} doesn\'t exist.')

    if not dir.is_dir():
        raise Exception(f'{dir_path} is not a directory.')


def append_to_datapaths_file(in_dir_path: str, out_dir_path: str, data_paths_name: str) -> None:
    in_dir = Path(in_dir_path)
    out_path = os.path.join(out_dir_path, data_paths_name)

    for dir_content in in_dir.iterdir():
        extension = ''
        content_split_by_dot = str(dir_content).split('.')

        if len(content_split_by_dot) > 1:
            extension = content_split_by_dot[-1]

        if dir_content.is_file() and (extension == 'jpg' or extension == 'png'):
            with open(out_path, 'a+') as out_file:
                out_file.write(str(dir_content.resolve()) + '\n')


if __name__ == '__main__':
    main()
