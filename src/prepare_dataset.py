#!/usr/bin/env python3

import argparse
import cv2
import json
import os
from pathlib import Path
import pandas as pd
from typing import List
import textwrap


def main() -> None:
    args = parse_arguments()

    if args.vid is None:
        print('Path to video not defined...')

    else:
        path_to_video, path_to_csv, path_to_output, path_to_name_file = get_paths_from_arguments(args)
        path_to_repaired_csv = generate_path_to_repaired_csv(path_to_csv)
        path_to_data_dir = generate_path_to_data_dir(path_to_output, path_to_video)

        create_repaired_csv(path_to_csv)
        create_data_dir(path_to_data_dir)
        ensure_names_file_existence(path_to_name_file, path_to_repaired_csv)
        create_yolo_files(path_to_video, path_to_repaired_csv, path_to_data_dir, path_to_name_file)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='YOLO Dataset preparator', epilog=generate_epilog())
    parser.add_argument('-c', '--csv', dest='csv', help='Path to csv file (Default: same as video)')
    parser.add_argument('-n', '--name', dest='name', help='Path to name file')
    parser.add_argument('-o', '--out', dest='out', help='Path to output (Default: same as video)')
    parser.add_argument('-v', '--video', dest='vid', help='Path to video', required=True)

    return parser.parse_args()


def generate_epilog() -> str:
    return textwrap.dedent('''
    Steps involved:
     - creates new 'repaired_[CSV_FILE_NAME].csv' file
     - creates data dir if not exists
     - saves every frame in data dir as image
     - creates txt file with bounding boxes information for every frame in data dir
     - only includes annotations whose class matches those defined in the .name file
     - if .name file is not supplied or doesn't exists, it gets created as [VIDEO_NAME].names 
       and all annotations are included
     - appends frames paths to 'data_paths.txt' file for every frame image extracted from video
     
    Notes:
     - It is advisable to specify the absolute path to the video
    ''')


def get_paths_from_arguments(args: argparse.Namespace) -> tuple:
    args = prepare_arguments(args)

    path_to_video = args.vid
    path_to_csv = args.csv
    path_to_output = args.out
    path_to_name_file = args.name

    return path_to_video, path_to_csv, path_to_output, path_to_name_file


def prepare_arguments(args: argparse.Namespace) -> argparse.Namespace:
    path_to_video = args.vid

    if args.csv is None:
        video_path_as_list = path_to_video.split('.')
        args.csv = '.'.join(video_path_as_list[:-1]) + '.csv'

    if args.out is None:
        video_dir, video_file = os.path.split(path_to_video)
        args.out = video_dir

    if args.name is None:
        video_dir, video_file = os.path.split(path_to_video)
        video_file_as_list = video_file.split('.')
        names_file_name = '.'.join(video_file_as_list[:-1]) + '.names'

        args.name = os.path.join(args.out, names_file_name)

    return args


def create_repaired_csv(path_to_csv: str) -> None:
    print('CREATING REPAIRED CSV FILE...')
    path_to_repaired_csv = generate_path_to_repaired_csv(path_to_csv)

    with open(path_to_csv, 'r') as f:
        new_file = ''

        for line in f.readlines():
            new_line = repair_csv_line(line)

            new_file += new_line

    with open(path_to_repaired_csv, 'w') as f:
        f.write(new_file)

    print('CREATING REPAIRED CSV FILE DONE!')


def generate_path_to_repaired_csv(path_to_csv: str) -> str:
    path_to_csv = os.path.normpath(path_to_csv)
    path_split = path_to_csv.split(os.sep)
    new_file_name = 'repaired_' + str(path_split[-1])

    return os.path.join(os.sep.join(path_split[:-1]), new_file_name)


def generate_path_to_data_dir(path_to_output_dir, path_to_video) -> str:
    path_to_video_dir, video_file_name = os.path.split(path_to_video)
    data_dir_name = 'data_' + '.'.join(video_file_name.split('.')[:-1])
    return os.path.join(path_to_output_dir, data_dir_name)


def repair_csv_line(line: str) -> str:
    new_line = ''
    first_quot = None
    last_quot = None

    for i in range(len(line)):
        letter = line[i]

        if letter == '"':
            if first_quot is None:
                first_quot = i
            else:
                last_quot = i

    for i in range(len(line)):
        letter = line[i]

        if letter == ',':
            if i < first_quot or i > last_quot:
                letter = ';'

        new_line += letter

    return new_line


def create_data_dir(path_to_data_dir: str) -> None:
    Path(path_to_data_dir).mkdir(parents=True, exist_ok=True)


def ensure_names_file_existence(path_to_names_file: str, path_to_repaired_csv: str) -> None:
    names_file_path = Path(path_to_names_file)
    if not names_file_path.exists():
        Path(names_file_path.parent).mkdir(parents=True, exist_ok=True)
        names = generate_names(path_to_repaired_csv)
        create_names_file(path_to_names_file, names)


def generate_names(path_to_repaired_csv: str) -> list:
    df = get_repaired_csv_dataframe(path_to_repaired_csv)
    names = list()

    for row in df.iterrows():
        frame_info = json.loads(row[1][0])
        boxes = frame_info.get('boxes', [])

        for box in boxes:
            box_class = box.get('boxClass')

            if box_class is not None and box_class not in names:
                names.append(box_class)

    return names


def create_names_file(path_to_names_file: str, names: list) -> None:
    print('\nCREATING NAMES FILE...')

    with open(path_to_names_file, 'w') as names_file:
        for name in names:
            if name[-1] != '\n':
                name += '\n'

            names_file.write(name)

    print('CREATING NAMES FILE DONE!\n')


def create_yolo_files(path_to_video: str, path_to_csv: str, path_to_data_dir: str, path_to_name_file: str) -> None:
    video_capture = cv2.VideoCapture(path_to_video)
    df = get_repaired_csv_dataframe(path_to_csv)
    num_of_rows = int(df.count())
    path_to_output, data_dir_name = os.path.split(path_to_data_dir)
    classes_of_interest = generate_classes_of_interest(path_to_name_file)

    print('\nCREATING YOLO FILES...\n')

    for row, row_num in zip(df.iterrows(), [i + 1 for i in range(num_of_rows)]):
        frame_id = row[0]
        frame_info = json.loads(row[1][0])

        video_capture.set(cv2.CAP_PROP_POS_FRAMES, frame_id - 1)
        success, frame = video_capture.read()

        if success:
            frame_path = os.path.join(path_to_data_dir, str(frame_id) + '.jpg')
            cv2.imwrite(frame_path, frame)
            write_frame_info(frame_id, frame_info, frame.shape, path_to_data_dir, classes_of_interest)
            append_frame_path_to_train_file(path_to_output, frame_path)
            print_progress(row_num, num_of_rows)

    video_capture.release()
    print('\nDONE!')


def get_repaired_csv_dataframe(path_to_csv: str) -> pd.DataFrame:
    df = pd.read_csv(path_to_csv, sep=';', header=None, names=['col_' + str(i) for i in range(8)], index_col=1)
    df.drop(['col_' + str(i) for i in range(8) if i != 1 and i != 3], axis=1, inplace=True)

    return df


def generate_classes_of_interest(path_to_name_file: str) -> dict:
    classes_of_interest = dict()
    with open(path_to_name_file, 'r') as name_file:
        for class_id, line in enumerate(name_file):
            classes_of_interest[line.strip().lower()] = class_id

    return classes_of_interest


def write_frame_info(frame_id: int, frame_info: dict, frame_shape: tuple, path_to_data_dir: str,
                     classes_of_interest: dict) -> None:
    annotation = ''

    for box in frame_info['boxes']:
        box_class = box.get('boxClass')

        if box_class is not None and classes_of_interest.get(box_class.lower()) is not None:
            yolo_coords = convert_to_yolo_coords(frame_shape, box['coords'])
            line = str(classes_of_interest.get(box_class.lower())) + ' ' + ' '.join(
                [str(coord) for coord in yolo_coords]) + '\n'
            annotation += line

    with open(os.path.join(path_to_data_dir, str(frame_id) + '.txt'), 'w') as f:
        f.write(annotation)


def convert_to_yolo_coords(img_shape: tuple, box_coords: List[float]) -> List[float]:
    dw = img_shape[1]
    dh = img_shape[0]

    x = box_coords[0] * dw
    y = box_coords[1] * dh
    w = box_coords[2] * dw
    h = box_coords[3] * dh

    x += (w / 2)
    y += (h / 2)

    x /= dw
    y /= dh
    w /= dw
    h /= dh

    return [x, y, w, h]


def append_frame_path_to_train_file(output_path: str, frame_path: str) -> None:
    file_name = 'data_paths.txt'
    train_file_path = os.path.join(output_path, file_name)

    with open(train_file_path, 'a+') as file:
        file.write(frame_path + '\n')


def print_progress(row_number: int, total_number_of_rows: int) -> None:
    percentage = int((row_number / total_number_of_rows) * 100)
    print(f'\r{percentage}% COMPLETED...', end='')


if __name__ == '__main__':
    main()
