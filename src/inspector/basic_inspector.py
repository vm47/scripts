from . import utils
from .action import Action
from .display_operator import DisplayOperator
from .file_operator import FileOperator
from .image_information_visualizer import BaseVisualizer
import cv2
from datetime import datetime
from os.path import split as path_split, join as path_join
from typing import Optional
import numpy as np


class BasicInspector:
    NAME = 'Inspector'

    def __init__(self, file_path: str) -> None:
        self._parent_dir, self._file_name = path_split(file_path)
        self._file_operator = FileOperator(file_path)
        self._display_operator = DisplayOperator(self.NAME)
        self._img_information_visualizers = list()
        self._img_path = ''
        self._img = None
        self._is_active = True

    def run(self) -> None:
        self._prepare()
        self._run_main_loop()
        self._cleanup()

    def attach_img_info_visualizer(self, visualizer: BaseVisualizer) -> None:
        self._img_information_visualizers.append(visualizer)

    def _prepare(self) -> None:
        self._display_operator.create_window()
        self._img_path = self._file_operator.read_current_line()

    def _run_main_loop(self):
        while self._is_active:
            self._load_img()
            self._draw_img_info()
            self._print_img_info()
            self._display_img()
            self._process_command()

    def _cleanup(self) -> None:
        self._display_operator.destroy_window()

    def _load_img(self) -> None:
        self._img = cv2.imread(self._img_path)

    def _draw_img_info(self) -> None:
        if self._img is not None:
            for visualizer in self._img_information_visualizers:
                visualizer.draw_img_info(self._img_path, self._img)

            if self._file_operator.is_position_marked_for_removal():
                utils.draw_cross(self._img)
        else:
            self._img = self._create_error_img(f'Couldn\'t open {self._img_path}')

    def _print_img_info(self) -> None:
        print(f'\r{self._file_operator.current_position + 1}/{self._file_operator.count_positions()}'
              f' :: {self._img_path} ', end='')

    def _display_img(self) -> None:
        self._display_operator.show_img(self._img)

    def _process_command(self) -> None:
        action = self._define_action()
        self._execute_action(action)

    def _define_action(self) -> Action:
        action = self._wait_action_key_press()
        while action is Action.UNDEFINED:
            action = self._wait_action_key_press()
        return action

    @staticmethod
    def _wait_action_key_press() -> Action:
        key = cv2.waitKey(100) & 0xFF
        return Action.find_by_key(key)

    def _execute_action(self, action: Action) -> None:
        action_info = None

        if action is Action.QUIT:
            self._is_active = False
            self._print_action_info('EXITING...')
        elif action is Action.PREVIOUS:
            self._img_path = self._file_operator.read_previous_line()
        elif action is Action.NEXT:
            self._img_path = self._file_operator.read_next_line()
        elif action is Action.EXPORT:
            self._file_operator.export_lines()
            action_info = 'EXPORT COMPLETE...'
        elif action is Action.SAVE:
            img_path = self._generate_img_path()
            cv2.imwrite(img_path, self._img)
            action_info = f'IMAGE SAVED TO: {img_path}'
        elif action is Action.MARK:
            self._file_operator.mark_position_for_exclusion()
        elif action is Action.UNMARK:
            self._file_operator.unmark_position_for_exclusion()
        else:
            action_info = f'Action: {action} is undefined...'

        self._print_action_info(action_info)

    def _generate_img_path(self) -> str:
        img_prefix = ''.join(str(datetime.now().timestamp()).split('.'))
        orig_img_dir_path, orig_img_name = path_split(self._img_path)
        _, orig_img_dir_name = path_split(orig_img_dir_path)

        img_name = img_prefix + '_' + '_'.join((orig_img_dir_name, orig_img_name))
        return path_join(self._parent_dir, img_name)

    @staticmethod
    def _print_action_info(action_info: Optional[str]) -> None:
        if action_info is not None:
            print('\n' + str(action_info))

    @staticmethod
    def _create_error_img(error_msg: str, width: int = 1280, height: int = 720) -> np.ndarray:
        error_img = np.zeros((height, width, 3))
        utils.write_text(error_img, error_msg, (0, 0.1), (0, 0, 255))
        return error_img
