import cv2
import numpy as np


def draw_cross(img: np.ndarray, thickness: int = 10) -> None:
    h, w, _ = img.shape
    x1, y1 = 0, 0
    x2, y2 = w, h
    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), thickness)
    cv2.line(img, (x1, y2), (x2, y1), (0, 0, 255), thickness)


def draw_bb(img: np.ndarray, bb: tuple, color=(0, 255, 0), thickness=2) -> None:
    x = bb[0]
    y = bb[1]
    width = x + bb[2]
    height = y + bb[3]
    cv2.rectangle(img, (x, y), (width, height), color, thickness)


def write_text(image, text: str, position: tuple, color: tuple, thickness: float = 2., scale: float = 1.) -> None:
    h, w, _ = image.shape
    image_size = (w, h)
    coordinates = image_position(image_size, position)

    thickness = max(int(thickness * float(h) / 1000), 1)
    scale = scale * float(h) / 1000

    cv2.putText(
        image,
        text,
        coordinates,
        cv2.FONT_HERSHEY_DUPLEX,
        scale,
        (0, 0, 0),
        thickness * 3)

    cv2.putText(
        image,
        text,
        coordinates,
        cv2.FONT_HERSHEY_DUPLEX,
        scale,
        color,
        thickness)


def convert_coords_yolo_to_cv2(shape, box) -> tuple:
    x, y, w, h = _adjust_cords_by_shape(shape, box)

    return round(x), round(y), round(w), round(h)


def convert_coords_gt_to_cv2(shape, box) -> tuple:
    x, y, w, h = _adjust_cords_by_shape(shape, box)

    x -= (w / 2)
    y -= (h / 2)

    return round(x), round(y), round(w), round(h)


def _adjust_cords_by_shape(shape, box) -> tuple:
    dw = shape[1]
    dh = shape[0]

    x = box[0] * dw
    y = box[1] * dh
    w = box[2] * dw
    h = box[3] * dh

    return x, y, w, h


def image_position(image_size: tuple, position: tuple):
    x = int(image_size[0] * position[0])
    y = int(image_size[1] * position[1])
    return x, y
