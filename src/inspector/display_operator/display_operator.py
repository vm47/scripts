import cv2
import numpy as np


class DisplayOperator:
    def __init__(self, name: str) -> None:
        self._name = name

    def create_window(self, width: int = 1280, height: int = 720):
        cv2.namedWindow(self._name, cv2.WINDOW_KEEPRATIO)
        cv2.resizeWindow(self._name, width, height)

    def show_img(self, img: np.ndarray) -> None:
        cv2.imshow(self._name, img)

    def destroy_window(self) -> None:
        cv2.destroyWindow(self._name)
