from os.path import join as path_join, split as path_split


class FileOperator:
    def __init__(self, path_to_file: str) -> None:
        self._path_to_file = path_to_file
        self._current_position = 0
        self._positions = [0]
        self._positions_to_remove = []

        with open(path_to_file, 'r') as file:
            line = file.readline()
            while line != '':
                self._positions.append(file.tell())
                line = file.readline()

            self._eof_pos = self._positions.pop()

    @property
    def current_position(self) -> int:
        return self._current_position

    def read_current_line(self) -> str:
        return self._read_line_by_position(self._current_position)

    def read_previous_line(self) -> str:
        if self._current_position > 0:
            self._current_position -= 1

        return self._read_line_by_position(self._current_position)

    def read_next_line(self) -> str:
        if self._current_position < (len(self._positions) - 1):
            self._current_position += 1

        return self._read_line_by_position(self._current_position)

    def mark_position_for_exclusion(self) -> None:
        if self._current_position not in self._positions_to_remove:
            self._positions_to_remove.append(self._current_position)

    def unmark_position_for_exclusion(self) -> None:
        for i in range(len(self._positions_to_remove)):
            if self._positions_to_remove[i] == self._current_position:
                self._positions_to_remove.pop(i)
                break

    def is_position_marked_for_removal(self) -> bool:
        return self._current_position in self._positions_to_remove

    def export_lines(self) -> None:
        self._create_export_files()
        for pos in range(len(self._positions)):
            export_path = self._generate_export_path(pos not in self._positions_to_remove)
            with open(export_path, 'a') as file:
                file.write(self._read_line_by_position(pos) + '\n')

    def _create_export_files(self) -> None:
        to_keep_path = self._generate_export_path(True)
        to_remove_path = self._generate_export_path(False)

        with open(to_keep_path, 'w') as file:
            file.write('')

        with open(to_remove_path, 'w') as file:
            file.write('')

    def count_positions(self) -> int:
        return len(self._positions)

    def _generate_export_path(self, are_lines_to_keep: bool) -> str:
        dir_path, file_name = path_split(self._path_to_file)
        name_extention = '_to_keep.txt' if are_lines_to_keep else '_to_exclude.txt'

        return path_join(dir_path, '.'.join(file_name.split('.')[:-1]) + name_extention)

    def _read_line_by_position(self, position: int) -> str:
        with open(self._path_to_file, 'r') as file:
            file.seek(self._positions[position])
            line = file.readline()

        return line[:-1]

    def _generate_path_to_file_with_removed_lines(self) -> str:
        parent_dir, file = path_split(self._path_to_file)
        file_name = 'removed_lines_' + file

        return path_join(parent_dir, file_name)
