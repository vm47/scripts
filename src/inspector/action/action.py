from enum import Enum, auto
from .key import Key


class Action(Enum):
    UNDEFINED = auto()
    PREVIOUS = auto()
    NEXT = auto()
    EXPORT = auto()
    SAVE = auto()
    MARK = auto()
    UNMARK = auto()
    QUIT = auto()

    @staticmethod
    def find_by_key(key_value: int) -> 'Action':
        action_keys = Action.create_key_dict()
        key = Key.get_by_value(key_value)
        if key is not None:
            return action_keys[key]
        else:
            return Action.UNDEFINED

    @staticmethod
    def create_key_dict() -> dict:
        return {
            Key.ESC: Action.QUIT,
            Key.LEFT_ARROW: Action.PREVIOUS,
            Key.RIGHT_ARROW: Action.NEXT,
            Key.E: Action.EXPORT,
            Key.S: Action.SAVE,
            Key.X: Action.MARK,
            Key.Y: Action.UNMARK
        }
