from enum import Enum
from typing import Optional


class Key(Enum):
    ESC = 27
    LEFT_ARROW = 81
    RIGHT_ARROW = 83
    E = 101
    S = 115
    X = 120
    Y = 121

    @staticmethod
    def get_by_value(value: int) -> Optional['Key']:
        for key in Key:
            if key.value == value:
                return key
        return None
