from enum import Enum, auto


class Type(Enum):
    UNDEFINED = auto()
    PREVIOUS = auto()
    NEXT = auto()
    EXPORT = auto()
    SAVE = auto()
    MARK = auto()
    UNMARK = auto()
    QUIT = auto()
