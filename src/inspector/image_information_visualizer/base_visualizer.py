from abc import ABC, abstractmethod
import numpy as np
from modified_vtrack_parts.src.config import Config


class BaseVisualizer(ABC):
    def __init__(self, config: Config) -> None:
        self._config = config
        self._class_dict = config.generate_class_dict()
        self._is_writing_class_names_enabled = False

    @abstractmethod
    def draw_img_info(self, img_path: str, img: np.ndarray) -> None:
        pass
