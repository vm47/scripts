from inspector.image_information_visualizer import BaseVisualizer
from inspector import utils
import numpy as np
from typing import List, Tuple


class GroundTruthVisualizer(BaseVisualizer):
    def draw_img_info(self, img_path: str, img: np.ndarray) -> None:
        ground_truths = self._load_ground_truths(img_path)

        for ground_truth in ground_truths:
            self._draw_ground_truth(ground_truth, img)

    def _load_ground_truths(self, img_path: str) -> List[str]:
        ground_truth_path = '.'.join(img_path.split('.')[:-1]) + '.txt'
        ground_truths = list()

        with open(ground_truth_path, 'r') as file:
            for line in file:
                ground_truths.append(line.strip())
        return ground_truths

    def _draw_ground_truth(self, ground_truth: str, img: np.ndarray) -> None:
        class_id, x, y, width, height = self._ground_truth_string_to_tuple(ground_truth)
        class_name = self._class_dict.get(class_id)

        if class_name is not None and self._config.is_class_name_a_class_of_interest(class_name):
            cv2_coords = utils.convert_coords_gt_to_cv2(img.shape, (x, y, width, height))
            utils.draw_bb(img, cv2_coords, thickness=3)

            if self._is_writing_class_names_enabled:
                utils.write_text(img, class_name,
                                 (x - (width / 2), y + 0.02 - (height / 2)),
                                 (0, 255, 0))

    @staticmethod
    def _ground_truth_string_to_tuple(line: str) -> Tuple[int, float, float, float, float]:
        line_list = line.strip().split(' ')

        return (int(line_list[0]), float(line_list[1]), float(line_list[2]), float(line_list[3]),
                float(line_list[4]))
