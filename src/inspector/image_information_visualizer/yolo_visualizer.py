from . import BaseVisualizer
from inspector import utils
from modified_vtrack_parts.src.detector import YoloV3Detector, BBox, Detection
from modified_vtrack_parts.src.config import Config
import numpy as np


class YoloVisualizer(BaseVisualizer):
    def __init__(self, config: Config) -> None:
        super().__init__(config)
        self._detector = YoloV3Detector(config)

    def draw_img_info(self, img_path: str, img: np.ndarray) -> None:
        for detection in self._detector.find_detections(img_path):
            self._draw_detection(detection, img)

    def _draw_detection(self, detection: Detection, img: np.ndarray) -> None:
        bbox = detection.bbox

        cv2_coords = utils.convert_coords_yolo_to_cv2(img.shape, (bbox.xmin, bbox.ymin, bbox.width, bbox.height))
        detection_class_names = [class_name for class_name, confidence in
                                 [detection_class for detection_class in detection.classes]]

        utils.draw_bb(img, cv2_coords, color=(0, 0, 255), thickness=3)

        if self._is_writing_class_names_enabled:
            utils.write_text(img, ', '.join(detection_class_names),
                             (bbox.xmin, bbox.ymin + bbox.height),
                             (0, 0, 255))
