#!/usr/bin/env python3
import argparse
import cv2
import numpy as np
import os
from pathlib import Path
import textwrap


def main() -> None:
    args = parse_arguments()
    file_path = args.file
    new_file_name = args.name
    output_dir_path = args.output
    part_to_replace = args.replace
    replacing_part = args.with_

    output_path = os.path.join(output_dir_path, new_file_name + '.txt')

    check_file_existence(file_path)
    check_dir_existence(output_dir_path)

    ensure_dest_file_existence(output_path)

    with open(file_path, 'r') as file:
        for path in file:
            new_path = replace_part_of_path(path, part_to_replace, replacing_part)
            append_line_to_file(new_path + '\n', output_path)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Replace part of path.', epilog=generate_epilog())

    parser.add_argument('-f', '--file', dest='file', help='Path to file containing paths', required=True)
    parser.add_argument('-n', '--name', dest='name', help='New path file name', required=True)
    parser.add_argument('-o', '--output', dest='output', help='Path to output directory', required=True)
    parser.add_argument('-r', '--replace', dest='replace', help='Part of path to be replaced', required=True)
    parser.add_argument('-w', '--with', dest='with_', help='Replacing part of path', required=True)

    return parser.parse_args()


def generate_epilog() -> str:
    return textwrap.dedent('''
    Steps involved:
     - Reads paths contained in [FILE].
     - For each path replace [REPLACE] with [WITH].
     - Save new file in [OUTPUT] as [NAME].txt (If file exists, new paths get appended).
    ''')


def check_file_existence(file_path: str) -> None:
    file = Path(file_path)

    if not file.exists():
        raise Exception(f'{file_path} doesn\'t exist.')

    if not file.is_file():
        raise Exception(f'{file_path} is not a file.')


def check_dir_existence(output_dir_path: str) -> None:
    directory = Path(output_dir_path)

    if not directory.exists():
        raise Exception(f'{output_dir_path} doesn\'t exist.')

    if not directory.is_dir():
        raise Exception(f'{output_dir_path} is not a directory.')


def ensure_dest_file_existence(file_path: str) -> None:
    file = Path(file_path)

    file.touch(mode=420, exist_ok=True)


def replace_part_of_path(path: str, part_to_replace: str, replacing_part: str) -> str:
    path_list = path.strip().split(sep=os.sep)

    for i, path_part in enumerate(path_list):
        if path_part == part_to_replace:
            path_list[i] = replacing_part

    return os.sep.join(path_list)


def append_line_to_file(line: str, file_path: str) -> None:
    with open(file_path, 'a+') as file:
        file.write(line)


if __name__ == '__main__':
    main()
