#!/usr/bin/env python3

import argparse
import json
import os
from pathlib import Path
from typing import List


def main() -> None:
    args = parse_arguments()

    with open(args.json, 'r') as jf:
        ann_file = json.load(jf)
        for ann in ann_file.get('annotations'):
            if ann.get('category_id') == 1:
                for img in ann_file.get('images'):
                    if img.get('id') == ann.get('image_id'):
                        img_f_name = img.get('file_name')
                        img_f_name_s = img_f_name.split('.')
                        img_name = '.'.join(img_f_name_s[:-1])
                        yolo_f_name = img_name + '.txt'
                        yolo_p = Path(os.path.join(args.img_dir, yolo_f_name))
                        yolo_p.touch(exist_ok=True)
                        with open(str(yolo_p), 'a') as yf:
                            shape = (float(img.get('height')), float(img.get('width')))
                            bbox = ann.get('bbox')
                            b_coor = [float(bbox[0]), float(bbox[1]), float(bbox[2]), float(bbox[3])]
                            x, y, w, h = convert_to_yolo_coords(shape, b_coor)
                            s = f'1 {x} {y} {w} {h}\n'
                            yf.write(s)
                        break


def convert_to_yolo_coords(img_shape: tuple, box_coords: List[float]) -> List[float]:
    dw = img_shape[1]
    dh = img_shape[0]

    x = box_coords[0] / dw
    y = box_coords[1] / dh
    w = box_coords[2] / dw
    h = box_coords[3] / dh

    x += (w / 2)
    y += (h / 2)
    #
    # x /= dw
    # y /= dh
    # w /= dw
    # h /= dh

    return [x, y, w, h]


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='YOLO Dataset preparator from COCO')
    #  description='YOLO Dataset preparator', epilog=generate_epilog())
    parser.add_argument('-j', '--json', dest='json', help='Path to json file', required=True)
    parser.add_argument('-i', '--img_dir', dest='img_dir', help='Path to images dir', required=True)
    return parser.parse_args()
    
# def generate_epilog() -> str:
#     return textwrap.dedent('''
#     Steps involved:
#      - creates new 'repaired_[CSV_FILE_NAME].csv' file
#      - creates data dir if not exists
#      - saves every frame in data dir as image
#      - creates txt file with bounding boxes information for every frame in data dir
#      - only includes annotations whose class matches those defined in the .name file
#      - if .name file is not supplied or doesn't exists, it gets created as [VIDEO_NAME].names 
#        and all annotations are included
#      - appends frames paths to 'data_paths.txt' file for every frame image extracted from video

#     Notes:
#      - It is advisable to specify the absolute path to the video
#     ''')

if __name__ == '__main__':
    main()
