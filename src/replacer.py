#!/usr/bin/env python3
import os


FILENAME = '20200618000000-20200618002251.txt'
OUT = 'out'


def main() -> None:
    lines = ''
    with open(FILENAME, 'r') as f:
        for line in f:
            s_path, s_n = os.path.split(line)
            sname = s_n.split('.')
            lines += '.'.join(sname[:-1]) + '\n'
    
    with open(os.path.join(OUT, FILENAME), 'w') as f:
        f.write(lines)
    

if __name__ == '__main__':
    main()