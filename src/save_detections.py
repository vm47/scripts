#!/usr/bin/env python3

import argparse
import cv2
import os
from pathlib import Path
from datetime import datetime
from inspector import YoloVisualizer
from typing import Optional
from modified_vtrack_parts.src.config import Config


def main() -> None:
    args = parse_arguments()
    file_path = args.file
    config_path = args.config
    new_dir_name = args.name

    new_dir_path = generate_new_dir_path(file_path, new_dir_name)

    create_dir(new_dir_path)
    visualizer = YoloVisualizer(Config.load_from_file(config_path))

    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            print(line)
            img = cv2.imread(line)
            visualizer.draw_img_info(line, img)
            cv2.imwrite(os.path.join(new_dir_path, line.split(os.sep)[-1]), img)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Save detections')
    parser.add_argument('-f', '--file', dest='file', help='Path to dataset file', required=True)
    parser.add_argument('-c', '--config', dest='config', help='Path to configuration file', required=True)
    parser.add_argument('-n', '--name', dest='name', help='New directory name', default=None)

    return parser.parse_args()


def generate_new_dir_path(path_to_file: str, new_dir_name: Optional[str] = None) -> str:
    path = ''

    if new_dir_name is None:
        splitted = path_to_file.strip().split('.')
        appendix = datetime.now().isoformat().replace('.', '').replace(':', '')
        path += '.'.join(splitted[:-1]) + '_' + appendix

    else:
        splitted = path_to_file.split(os.sep)
        path += os.path.join(os.sep.join(splitted[:-1]), new_dir_name)

    return path


def create_dir(dir_path: str) -> None:
    directory = Path(dir_path)
    directory.mkdir(exist_ok=True)


def is_content_image(content_name: str) -> bool:
    splittet = content_name.strip().split('.')
    if splittet[-1] == 'jpg':
        return True
    else:
        return False


if __name__ == '__main__':
    main()
