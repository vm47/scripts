#!/usr/bin/env python3

import argparse
import textwrap
from inspector import BasicInspector, GroundTruthVisualizer, YoloVisualizer
from modified_vtrack_parts.src.config import Config


def main() -> None:
    args = parse_arguments()
    check_args(args)

    inspector = define_inspector(args)
    inspector.run()


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Dataset inspector', epilog=generate_epilog())
    parser.add_argument('-f', '--file', dest='file', help='Path to dataset file', required=True)
    parser.add_argument('-gt', '--ground_truth', dest='has_gt', help='Use ground truth inspector', action='store_true')
    parser.add_argument('-yolo', '--yolo', dest='has_yolo', help='Use yolo inspector', action='store_true')
    parser.add_argument('-c', '--config', dest='config', help='Path to configuration file.')

    return parser.parse_args()


def generate_epilog() -> str:
    return textwrap.dedent('''
    Expects path to text file that contains paths to images.
    
    Inspector types:
     - gt:          Ground-Truth inspector, reads annotations & displays bounding-boxes.
     - yolo:        Yolo inspector, displays detection bounding boxes.
     
     Note:
      - Ground truth & yolo inspectors need path to configuration file
    
    Navigation:
     - Left arrow:  Show previous image
     - Right arrow: Show next image
     - e:           Export images to keep & to be excluded.
                    Creates 2 files, [FILE]_to_keep.txt and [FILE]_to_exclude.txt
     - s:           Saves current image in the same directory as [FILE].
     - x:           Mark image to be excluded
     - y:           Unmark image to be excluded
     - Esc:         Exits dataset inspector
    ''')


def check_args(args: argparse.Namespace) -> None:
    is_config_needed = False

    if args.has_gt or args.has_yolo:
        is_config_needed = True

    if is_config_needed and args.config is None:
        raise ValueError(f'Configuration path not defined. See --help for more information.')


def define_inspector(args: argparse.Namespace) -> BasicInspector:
    inspector = BasicInspector(args.file)
    config_file_path = args.config
    config = Config.load_from_file(config_file_path)

    if args.has_gt:
        gt_visualizer = GroundTruthVisualizer(config)
        inspector.attach_img_info_visualizer(gt_visualizer)

    if args.has_yolo:
        yolo_visualizer = YoloVisualizer(config)
        inspector.attach_img_info_visualizer(yolo_visualizer)

    return inspector


if __name__ == '__main__':
    main()
