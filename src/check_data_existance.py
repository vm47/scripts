#!/usr/bin/env python3
import argparse
import cv2
from pathlib import Path


def main() -> None:
    args = parse_arguments()
    dataset_file_path = args.file

    dataset_path = Path(dataset_file_path)
    if dataset_path.is_file():
        not_existing_paths = []
        with open(dataset_file_path, 'r') as file:
            for line in file:
                data_file_path = line.strip()
                data_path = Path(data_file_path)
                if not data_path.exists():
                    not_existing_paths.append(data_file_path)
                else:
                    img = cv2.imread(data_file_path)
                    try:
                        h, w, _ = img.shape
                    except:
                        not_existing_paths.append(data_file_path)

        print(f'Amount of non existing files: {len(not_existing_paths)}')
        for path in not_existing_paths:
            print(path)

    else:
        print(f'File "{dataset_file_path}" does not exist...')


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Check datapaths existance')
    parser.add_argument('-f', '--file', dest='file', help='Path to dataset file', required=True)

    return parser.parse_args()


if __name__ == '__main__':
    main()
