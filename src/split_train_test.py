#!/usr/bin/env python3

import argparse
from os.path import split as path_split, join as path_join
from random import randint
from typing import List


def main() -> None:
    args = parse_arguments()
    split_train_test(args.file, int(args.percent))


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='YOLO Dataset train & test splitter', epilog=generate_epilog())
    parser.add_argument('-f', '--file', dest='file', help='Path to dataset file', required=True)
    parser.add_argument('-p', '--percent', dest='percent', help='Percent of test set (Default 10)', default=10)

    return parser.parse_args()


def generate_epilog() -> str:
    return """
    Steps involved:
    
     - Reads dataset file with paths to images
     - Selects passed percentage of randomly selected rows
     - Creates train.txt & test.txt files in the same location as dataset text file
    """


def split_train_test(dataset_file_path: str, test_percentage: int) -> None:
    print('SPLIT DATA INTO TRAIN & TEST\n')
    total_data_amount = count_total_data(dataset_file_path)
    test_data_amount = round(total_data_amount * (test_percentage / 100))
    print('SPLITTING DATA RANDOMLY...')
    test_indexes = create_test_indexes(total_data_amount, test_data_amount)
    print('SPLITTING DATA RANDOMLY FINISHED...\n')
    data_dir, data_file = path_split(dataset_file_path)
    print('CREATING TEST & TRAIN FILES')

    with open(dataset_file_path, 'r') as dataset_file:
        for index, line in zip(range(total_data_amount), dataset_file):
            file_name = 'test.txt' if index in test_indexes else 'train.txt'
            with open(path_join(data_dir, file_name), 'a+') as final_file:
                final_file.write(line)

            print_progress(index + 1, total_data_amount)

    print('\nCREATING TEST & TRAIN FILES FINISHED')


def count_total_data(dataset_file_path: str) -> int:
    cnt = 0

    with open(dataset_file_path, 'r') as file:
        for _ in file:
            cnt += 1

    return cnt


def create_test_indexes(total_data_amount: int, test_data_amount: int) -> List[int]:
    indexes = list()

    while len(indexes) < test_data_amount:
        rnd_index = randint(0, total_data_amount - 1)
        if rnd_index not in indexes:
            indexes.append(rnd_index)

    return indexes


def print_progress(row_number: int, total_number_of_rows: int) -> None:
    percentage = int((row_number / total_number_of_rows) * 100)
    print(f'\r{percentage}% COMPLETED...', end='')


if __name__ == '__main__':
    main()
