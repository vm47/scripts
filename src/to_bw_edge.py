#!/usr/bin/env python3
import argparse
import cv2
import numpy as np
import os
from pathlib import Path
import textwrap


def main() -> None:
    args = parse_arguments()
    input_dir = args.input_dir

    check_dir_existence(input_dir)
    output_dir = generate_output_dir(input_dir)
    create_output_dir(output_dir)
    create_files_from_input_dir_to_output_dir(input_dir, output_dir)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Turn images to black & white edges.', epilog=generate_epilog())
    parser.add_argument('-i', '--input', dest='input_dir', help='Path to directory containing images', required=True)

    return parser.parse_args()


def generate_epilog() -> str:
    return textwrap.dedent('''
    Steps involved:
     - Creates new directory [OUTPUT_DIR] as '[INPUT_DIR]_BW_EDGE'
     - Converts all images in [INPUT_DIR] to black & white edges and saves them in [OUTPUT_DIR]
     - Copies all annotation data to [OUTPUT_DIR]
    ''')


def check_dir_existence(dir_path: str) -> None:
    dir = Path(dir_path)

    if not dir.exists():
        raise Exception(f'{dir_path} doesn\'t exist.')

    if not dir.is_dir():
        raise Exception(f'{dir_path} is not a directory.')


def generate_output_dir(dir_path: str) -> str:
    out_dir_path = dir_path
    while out_dir_path[-1] == os.sep and len(out_dir_path) > 1:
        out_dir_path = out_dir_path[:-1]

    return out_dir_path + '_bw_edge'


def create_output_dir(dir_path: str) -> None:
    out_dir = Path(dir_path)
    out_dir.mkdir()


def create_files_from_input_dir_to_output_dir(in_dir_path: str, out_dir_path) -> None:
    in_dir = Path(in_dir_path)

    for dir_content in in_dir.iterdir():
        path_but_extension = ''
        extension = ''
        content_split_by_dot = str(dir_content).split('.')

        if len(content_split_by_dot) > 1:
            path_but_extension = '.'.join(content_split_by_dot[:-1])
            extension = content_split_by_dot[-1]

        content_name = dir_content.name
        if dir_content.is_file() and (extension == 'jpg' or extension == 'png'):
            img_path = os.path.join(out_dir_path, content_name)

            print(img_path)

            img = cv2.imread(str(dir_content))
            bw_img = to_bw(img)
            cv2.imwrite(img_path, bw_img)

            annotation = Path('.'.join((path_but_extension, 'txt')))
            try:
                with open(str(annotation), 'r') as in_fp:
                    annotation_content = in_fp.read()
                    with open(os.path.join(out_dir_path, annotation.name), 'w') as out_fp:
                        out_fp.write(annotation_content)
            except:
                print(f'Annotation: "{str(annotation)}" couldn\'t be copied. '
                      f'Probably doesn\'t exist or has different name.')


def to_bw(img: np.ndarray) -> np.ndarray:
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    eq_hist_img = cv2.equalizeHist(gray_img)

    grad_x_img = cv2.Sobel(eq_hist_img, cv2.CV_8U, 1, 0)
    grad_y_img = cv2.Sobel(eq_hist_img, cv2.CV_8U, 0, 1)

    abs_grad_x_img = cv2.convertScaleAbs(grad_x_img)
    abs_grad_y_img = cv2.convertScaleAbs(grad_y_img)

    grad = cv2.addWeighted(abs_grad_x_img, 0.5, abs_grad_y_img, 0.5, 0)

    canny_img = cv2.Canny(eq_hist_img, 200, 200)
    final_img = cv2.addWeighted(grad, 1, canny_img, 1, 0)

    return final_img


if __name__ == '__main__':
    main()
