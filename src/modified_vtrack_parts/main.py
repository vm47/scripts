#!/usr/bin/env python3

from src.detector import YoloV3Detector, GroundTruthDetector, BBox
from src.config import Config
import cv2
import numpy as np

DATASET = '/home/zdravko/work/projects/single_article_recognition/articles/jack_daniels/test.txt'
CONFIG = '/home/zdravko/work/projects/single_article_recognition/my_src/jack_daniels.yaml'


def main() -> None:
    global DATASET, CONFIG

    window_name = 'TEST'
    ds_path = DATASET
    config = Config.load_from_file(CONFIG)
    yolo_detector = YoloV3Detector(config)
    gnd_detector = GroundTruthDetector(config)

    cv2.namedWindow(window_name, cv2.WINDOW_KEEPRATIO)
    cv2.resizeWindow(window_name, 1280, 720)

    with open(ds_path, 'r') as file:
        for line in file:
            img = cv2.imread(line.strip())

            detections = yolo_detector.find_detections(line.strip())
            for detection in detections:
                bbox = detection.bbox
                draw_bb(img, convert_to_opencv_bbox(img.shape, bbox))

            detections = gnd_detector.find_detections(line.strip())
            for detection in detections:
                bbox = detection.bbox
                draw_bb(img, convert_to_opencv_bbox(img.shape, bbox), (0, 0, 255))

            cv2.imshow(window_name, img)

            key = cv2.waitKey(1) & 0xFF

    cv2.destroyWindow(window_name)


def draw_bb(img: np.ndarray, bb: tuple, color=(0, 255, 0), thickness=2) -> None:
    cv2.rectangle(img, (bb[0], bb[1]), (bb[0] + bb[2], bb[1] + bb[3]), color, thickness)


def convert_to_opencv_bbox(img_shape: tuple, bbox: BBox) -> tuple:
    dw = img_shape[1]
    dh = img_shape[0]

    x = round(bbox.xmin * dw)
    y = round(bbox.ymin * dh)
    w = round(bbox.width * dw)
    h = round(bbox.height * dh)

    return x, y, w, h


if __name__ == '__main__':
    main()
