from ..config import Config
from ..detector import Detector, GroundTruthDetector, YoloV3Detector
from ..detection_metrics import Evaluator
from ..detection_metrics.utils import MethodAveragePrecision
import cv2


class DetectorReport:
    DEFAULT_IOU_THRESHOLD = 0.5

    def __init__(self, config: Config, dataset_path: str) -> None:
        self._yolov3_detector = YoloV3Detector(config)
        self._ground_truth_detector = GroundTruthDetector(config)
        self._yolov3_bbox_detections = []
        self._ground_bbox_truth_detections = []
        self._nbr_of_frames = 0

        self._load_detections(dataset_path)

    def _load_detections(self, dataset_path: str) -> None:
        with open(dataset_path, 'r') as file:
            for line in file:
                img_path = line.strip()
                self._yolov3_bbox_detections += self._create_bbox_detections(self._yolov3_detector, img_path)
                self._ground_bbox_truth_detections += self._create_bbox_detections(self._ground_truth_detector,
                                                                                   img_path)
                self._nbr_of_frames += 1

    @staticmethod
    def _create_bbox_detections(detector: Detector, img_path: str) -> list:
        img = cv2.imread(img_path)
        img_height, img_width, _ = img.shape
        detections = detector.find_detections(img_path)
        bbox_detections = []

        for detection in detections:
            bb = detection.bbox.scale(img_width, img_height)

            bbox_detections.append([
                img_path,
                detection.main_class(),
                detection.main_confidence(),
                [bb.xmin, bb.ymin, bb.xmax, bb.ymax]
            ])

        return bbox_detections

    def calc_voc_metrics(self, iou_threshold: float = DEFAULT_IOU_THRESHOLD) -> list:

        return Evaluator().GetPascalVOCMetricsFromLists(
            self._yolov3_bbox_detections,
            self._ground_bbox_truth_detections,
            sorted(self._yolov3_detector.classes_of_interest),
            IOUThreshold=iou_threshold,
            method=MethodAveragePrecision.EveryPointInterpolation,
        )

    def get_report_values(self, iou_threshold: float = DEFAULT_IOU_THRESHOLD):
        execution_metrics = self.calc_voc_metrics(iou_threshold)

        acc_ap = sum([cm['AP'] for cm in execution_metrics])
        valid_classes = len([cm for cm in execution_metrics if cm['total positives'] > 0])

        result = dict(
            total_frame_count=self._nbr_of_frames,
            detection_count=len(self._yolov3_bbox_detections),
            ground_truth_count=len(self._ground_bbox_truth_detections),
            metrics=execution_metrics,
            accumulated_ap=float(acc_ap),
            mean_ap=(acc_ap / valid_classes) if valid_classes > 0 else float(0)
        )

        return result

    @staticmethod
    def plot_precision_recall_curve(metrics_result: dict) -> None:
        Evaluator().plot_precision_recall_curve(metrics_result['metrics'])
