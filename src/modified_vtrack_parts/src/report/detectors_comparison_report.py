from . import DetectorReport
from ..config import Config
from ..detection_metrics import Evaluator
from os import listdir as os_listdir, sep as os_sep
from os.path import abspath as os_path_abspath


class DetectorsComparisonReport:
    def __init__(self, config_path: str, dataset_path: str, weights_folder_path: str) -> None:
        self._config = Config.load_from_file(config_path)
        self._dataset_path = dataset_path
        self._weights_folder_path = weights_folder_path
        self._total_frames_count = None
        self._ground_truth_count = None

    @property
    def total_frame_count(self) -> int:
        return self._total_frames_count

    @property
    def ground_truth_count(self) -> int:
        return self._ground_truth_count

    def create_results(self) -> list:
        results = []

        for folder_content in sorted(os_listdir(self._weights_folder_path)):
            path = os_path_abspath(os_sep.join([self._weights_folder_path, folder_content]))
            if self._is_weights_file(path):
                tmp_cnf = self._create_config_with_weights_path(path)
                detector_report = DetectorReport(tmp_cnf, self._dataset_path)
                report_values = detector_report.get_report_values()
                del detector_report

                result = {
                    'weights_file_path': path,
                    'accumulated_ap': report_values.get('accumulated_ap'),
                    'mean_ap': report_values.get('mean_ap'),
                    'detection_count': report_values.get('detection_count'),
                    'html_charts': self._generate_html_charts(report_values)
                }

                results.append(result)

                if self._total_frames_count is None:
                    self._total_frames_count = report_values.get('total_frame_count')

                if self._ground_truth_count is None:
                    self._ground_truth_count = report_values.get('ground_truth_count')

        return results

    @staticmethod
    def _is_weights_file(folder_content_path: str) -> bool:
        splitted_path = folder_content_path.split('.')
        return len(splitted_path) > 1 and splitted_path[-1] == 'weights'

    def _create_config_with_weights_path(self, weights_path: str) -> Config:
        return Config(
            self._config.lib_file_path,
            self._config.model_folder_path,
            self._config.config_file_path,
            weights_path,
            self._config.meta_file_path,
            self._config.threshold,
            self._config.hier_threshold,
            self._config.nms,
            self._config.classes_of_interest
        )

    @staticmethod
    def _generate_html_charts(report_values: dict) -> str:
        charts = Evaluator().generate_precision_recall_curves_uri(
            results=report_values.get('metrics')
        )

        string = ''
        for chart in charts:
            html = f'<img src = "{chart}"/>\n\n'
            string += html

        return string
