from .detection import Detection
from .detector import Detector
from .yolov3_engine import YoloV3Engine
import cv2
from typing import List
from ..config import Config


class YoloV3Detector(Detector):

    def __init__(self, config: Config) -> None:
        super().__init__(config.classes_of_interest)
        self._engine = YoloV3Engine(config)

    def find_detections(self, path_to_img) -> List[Detection]:
        image = cv2.imread(path_to_img)

        detections = self._engine.detect(image)
        return self.filter_classes(detections)
