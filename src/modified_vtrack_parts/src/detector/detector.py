from .detection import Detection
from abc import ABC, abstractmethod
from typing import List, Optional


class Detector(ABC):
    def __init__(self, classes_of_interest: Optional[List[str]]) -> None:
        self._classes_of_interest = list() if classes_of_interest is None else classes_of_interest

    def filter_classes(self, detections: List[Detection]) -> List[Detection]:
        if not self._classes_of_interest:
            return detections

        coi_set = set(self._classes_of_interest)
        filtered_detections: List[Detection] = list()

        for detection in detections:
            name_set = {c[0] for c in detection.classes}
            if coi_set.intersection(name_set):
                filtered_detections.append(detection)

        return filtered_detections

    @property
    def classes_of_interest(self):
        return self._classes_of_interest

    @abstractmethod
    def find_detections(self, path_to_img: str) -> List[Detection]:
        pass
