from .bbox import BBox
from .detection import Detection
from .detector import Detector
from os import path
from typing import List
from ..config import Config


class GroundTruthDetector(Detector):
    def __init__(self, config: Config) -> None:
        super().__init__(config.classes_of_interest)

        self._classes = []

        self._populate_classes(path.join(config.model_folder_path, config.meta_file_path))

    def _populate_classes(self, meta_file_path: str) -> None:
        names_path = ''
        with open(meta_file_path, 'r') as meta_file:
            for line in meta_file:
                if line[:6] == 'names=':
                    names_path = line.strip()[6:]
                    break

        with open(names_path, 'r') as names_file:
            for line in names_file:
                self._classes.append(line.strip())

    def find_detections(self, path_to_img: str) -> List[Detection]:
        img_dir_path, img_name = path.split(path_to_img)
        gnd_truth_file_name = '.'.join(img_name.split('.')[:-1]) + '.txt'
        gnd_truth_path = path.join(img_dir_path, gnd_truth_file_name)

        yolo_detections = []
        detections = []

        with open(gnd_truth_path, 'r') as file:
            for line in file:
                yolo_detections.append(line.strip().split(' '))

        for yolo_detection in yolo_detections:
            detections.append(self._yolo_to_detection(yolo_detection))

        return detections

    def _yolo_to_detection(self, yolo_detection: list) -> Detection:
        bbox = self._create_bbox(yolo_detection)

        return Detection(classes=[(self._classes[int(yolo_detection[0])], 1.)], bbox=bbox)

    def _create_bbox(self, yolo_detection: list) -> BBox:
        x = float(yolo_detection[1])
        y = float(yolo_detection[2])
        w = float(yolo_detection[3])
        h = float(yolo_detection[4])

        xmin = x - (w / 2)
        ymin = y - (h / 2)
        xmax = x + (w / 2)
        ymax = y + (h / 2)

        return BBox(xmin=xmin, ymin=ymin, xmax=xmax, ymax=ymax)
