import numpy as np
from typing import NamedTuple


class BBox(NamedTuple):
    """Bounding box.

    Represents a rectangle which sides are either vertical or horizontal, parallel
    to the x or y axis.
    """

    xmin: float
    ymin: float
    xmax: float
    ymax: float

    @property
    def width(self):
        """Returns bounding box width."""
        return self.xmax - self.xmin

    @property
    def height(self):
        """Returns bounding box height."""
        return self.ymax - self.ymin

    @property
    def area(self):
        """Returns bound box area."""
        return self.width * self.height

    @property
    def valid(self):
        """Returns whether bounding box is valid or not.

        Valid bounding box has xmin <= xmax and ymin <= ymax which is equivalent to
        width >= 0 and height >= 0.
        """
        return self.width >= 0 and self.height >= 0

    def scale(self, sx, sy):
        """Returns scaled bounding box."""
        return BBox(xmin=sx * self.xmin,
                    ymin=sy * self.ymin,
                    xmax=sx * self.xmax,
                    ymax=sy * self.ymax)

    def translate(self, dx, dy):
        """Returns translated bounding box."""
        return BBox(xmin=dx + self.xmin,
                    ymin=dy + self.ymin,
                    xmax=dx + self.xmax,
                    ymax=dy + self.ymax)

    def map(self, f):
        """Returns bounding box modified by applying f for each coordinate."""
        return BBox(xmin=f(self.xmin),
                    ymin=f(self.ymin),
                    xmax=f(self.xmax),
                    ymax=f(self.ymax))

    def topleft(self):
        return self.xmin, self.ymin

    def bottomright(self):
        return self.xmax, self.ymax

    def bottom_center(self):
        return (self.xmin + self.xmax) / 2., self.ymax

    def center(self):
        return (self.xmin + self.xmax) / 2., (self.ymin + self.ymax) / 2.

    @staticmethod
    def intersect(a, b):
        """Returns the intersection of two bounding boxes (may be invalid)."""
        return BBox(xmin=max(a.xmin, b.xmin),
                    ymin=max(a.ymin, b.ymin),
                    xmax=min(a.xmax, b.xmax),
                    ymax=min(a.ymax, b.ymax))

    @staticmethod
    def union(a, b):
        """Returns the union of two bounding boxes (always valid)."""
        return BBox(xmin=min(a.xmin, b.xmin),
                    ymin=min(a.ymin, b.ymin),
                    xmax=max(a.xmax, b.xmax),
                    ymax=max(a.ymax, b.ymax))

    @staticmethod
    def iou(a, b):
        """Returns intersection-over-union value."""
        intersection = BBox.intersect(a, b)
        if not intersection.valid:
            return 0.0
        area = intersection.area
        return area / (a.area + b.area - area)

    @staticmethod
    def from_cwh(cx, cy, w, h):
        """Returns bounding box from center_x, center_y, width, height values"""
        return BBox(cx - w / 2, cy - h / 2, cx + w / 2, cy + h / 2)

    """
    Returns bounding box in [x, y, a, r] format where (x, y) are center coordinates, a is area, r is aspect ratio
    """

    def to_z_format(self):
        cx, cy = self.center()
        a = self.width * self.height
        r = self.width / self.height
        return np.array([cx, cy, a, r]).reshape((4, 1))

    """
    Takes [x, y, a, r] (where (x, y) are center coordinates, a is area, r is aspect ratio) and returns bounding box
    """

    @staticmethod
    def from_z_format(kalman_format):
        x, y, a, r = kalman_format[:4]
        w = np.sqrt(a * r)
        h = a / w
        return BBox.from_cwh(x, y, w, h)

    @staticmethod
    def distance(a, b):
        """Returns distance between central coordinates."""

        ax, ay = a.center()
        bx, by = b.center()
        x_diff = bx - ax
        y_diff = by - ay

        return np.sqrt((x_diff * x_diff) + (y_diff * y_diff))

    @staticmethod
    def from_tlwh(top, left, w, h):
        return BBox(top, left, top + w, left + h)
