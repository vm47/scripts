from .bbox import BBox
from typing import NamedTuple, Tuple, Optional, List


class Detection(NamedTuple):
    classes: List[Tuple[str, float]]  # list of tuples of class name and detection confidence
    bbox: BBox
    object_id: Optional[int] = None

    def main_class(self) -> Optional[str]:
        if not self.classes:
            return None
        return sorted(self.classes, key=lambda x: x[1], reverse=True)[0][0]

    def main_confidence(self) -> Optional[float]:
        if not self.classes:
            return None
        return sorted(self.classes, key=lambda x: x[1], reverse=True)[0][1]
