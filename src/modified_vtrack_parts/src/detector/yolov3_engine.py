from .detection import Detection
from .bbox import BBox
import cv2
import numpy as np
from ctypes import CDLL, RTLD_GLOBAL, POINTER
from ctypes import c_char_p, c_float, c_int, c_void_p
from ctypes import pointer, Structure
from typing import List
from os import path
from ..config import Config


class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]


class DETECTION(Structure):
    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]


class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]


class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]


class YoloV3Engine:
    DEFAULT_CONFIG = 'yolov3.cfg'
    DEFAULT_WEIGHTS = 'yolov3.weights'
    DEFAULT_META = 'coco.data'
    DEFAULT_THRESHOLD = 0.5
    DEFAULT_HIER_THRESHOLD = 0.5
    DEFAULT_NMS = 0.45

    def __init__(self, config: Config) -> None:
        self.lib_file_path = config.lib_file_path
        self.model_folder_path = config.model_folder_path
        self.config_file_path = self.generate_model_file_path(config.config_file_path, YoloV3Engine.DEFAULT_CONFIG)
        self.weights_file_path = self.generate_model_file_path(config.weights_file_path, YoloV3Engine.DEFAULT_WEIGHTS)
        self.meta_file_path = self.generate_model_file_path(config.meta_file_path, YoloV3Engine.DEFAULT_META)

        self.threshold = config.threshold if config.threshold is not None else YoloV3Engine.DEFAULT_THRESHOLD
        self.hier_threshold = config.hier_threshold if config.hier_threshold is not None else \
            YoloV3Engine.DEFAULT_HIER_THRESHOLD
        self.nms = config.nms if config.nms is not None else YoloV3Engine.DEFAULT_NMS

        # noinspection PyTypeChecker
        self.lib = CDLL(YoloV3Engine.encoded(config.lib_file_path), RTLD_GLOBAL)
        self.lib.network_width.argtypes = [c_void_p]
        self.lib.network_width.restype = c_int
        self.lib.network_height.argtypes = [c_void_p]
        self.lib.network_height.restype = c_int

        self.load_net = self.lib.load_network
        self.load_net.argtypes = [c_char_p, c_char_p, c_int]
        self.load_net.restype = c_void_p

        self.load_meta = self.lib.get_metadata
        self.lib.get_metadata.argtypes = [c_char_p]
        self.lib.get_metadata.restype = METADATA

        self.get_network_boxes = self.lib.get_network_boxes
        self.get_network_boxes.argtypes = [c_void_p,
                                           c_int,
                                           c_int,
                                           c_float,
                                           c_float,
                                           POINTER(c_int),
                                           c_int,
                                           POINTER(c_int)]
        self.get_network_boxes.restype = POINTER(DETECTION)

        self.free_detections = self.lib.free_detections
        self.free_detections.argtypes = [POINTER(DETECTION), c_int]

        self.do_nms_obj = self.lib.do_nms_obj
        self.do_nms_obj.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

        self.free_image = self.lib.free_image
        self.free_image.argtypes = [IMAGE]

        self.predict_image = self.lib.network_predict_image
        self.predict_image.argtypes = [c_void_p, IMAGE]
        self.predict_image.restype = POINTER(c_float)

        self.net = self.load_net(YoloV3Engine.encoded(self.config_file_path),
                                 YoloV3Engine.encoded(self.weights_file_path), 0)
        self.max_size = max(self.lib.network_width(self.net), self.lib.network_height(self.net))
        self.meta = self.load_meta(YoloV3Engine.encoded(self.meta_file_path))

        self.make_image = self.lib.make_image
        self.make_image.argtypes = [c_int, c_int, c_int]
        self.make_image.restype = IMAGE

        ndarray_type = np.ctypeslib.ndpointer(dtype=np.float32)
        self.copy_image_from_ndarray = self.lib.copy_image_from_ndarray
        self.copy_image_from_ndarray.argtypes = [IMAGE, ndarray_type]

        self.c_image = None
        self.d_size = None

        self.labels = [YoloV3Engine.decode(self.meta.names[class_idx]) for class_idx in range(self.meta.classes)]

        self.free_net = self.lib.free_network
        self.free_net.argtypes = [c_void_p]

    def generate_model_file_path(self, file_name: str, default_name: str) -> str:
        return path.join(self.model_folder_path, file_name) \
            if file_name is not None and file_name.strip() != '' \
            else path.join(self.model_folder_path, default_name)

    def detect(self, image: np.ndarray) -> List[Detection]:
        h, w, _ = image.shape
        self.preprocess_image(image, w, h)
        detections, count = self.get_detections(w, h)
        bboxes = self.detections_to_bboxes(detections, count, w, h)
        self.free_detections(detections, count)
        return bboxes

    def preprocess_image(self, image: np.ndarray, w: int, h: int) -> None:
        self.setup_image_buffer(w, h)

        resized = cv2.resize(image, dsize=self.d_size)  # faster transfer between Python and C
        rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)  # OpenCV uses BGR, while YOLO was trained on RGB
        channels_first = np.moveaxis(rgb, -1, 0).copy()  # YOLO uses channels first
        relative = np.divide(channels_first, 255., dtype=np.float32)  # transform value range from [0-255] to [0.-1.]
        self.copy_image_from_ndarray(self.c_image, relative)  # transfer image data to c

    def setup_image_buffer(self, w: int, h: int) -> None:
        if w > h:
            new_w = self.max_size
            new_h = int((new_w / w) * h)
        else:
            new_h = self.max_size
            new_w = int((new_h / h) * w)
        self.c_image = self.make_image(new_w, new_h, 3)
        self.d_size = new_w, new_h

    def get_detections(self, w, h):
        count = c_int(0)
        pnum = pointer(count)
        self.predict_image(self.net, self.c_image)
        detections = self.get_network_boxes(self.net, w, h, self.threshold, self.hier_threshold, None, 0, pnum)
        count = pnum[0]
        if self.nms:
            self.do_nms_obj(detections, count, self.meta.classes, self.nms)
        return detections, count

    def detections_to_bboxes(self, detections, count, w, h):
        sx = 1. / float(w)
        sy = 1. / float(h)
        res = []
        for j in range(count):
            classes = list()
            b = detections[j].bbox

            for i in range(self.meta.classes):
                if detections[j].prob[i] > 0:
                    class_name = self.labels[i]
                    class_score = detections[j].prob[i]
                    classes.append((class_name, class_score))

            if classes:
                res.append(Detection(classes=classes, bbox=BBox.from_cwh(b.x, b.y, b.w, b.h).scale(sx, sy)))

        res = sorted(res, key=lambda x: -max([c[1] for c in x.classes]))
        return res

    def release_resources(self):
        if self.c_image is not None:
            self.free_image(self.c_image)
            self.c_image = None

    @staticmethod
    def encoded(s: str) -> bytes:
        return s.encode('utf-8')

    @staticmethod
    def decode(b: bytes) -> str:
        return b.decode('utf-8')

    def __del__(self):
        self.free_net(self.net)
