from os.path import join as path_join
from typing import NamedTuple, Dict
import yaml


class Config(NamedTuple):
    lib_file_path: str
    model_folder_path: str
    config_file_path: str
    weights_file_path: str
    meta_file_path: str
    threshold: float
    hier_threshold: float
    nms: float
    classes_of_interest: list

    @staticmethod
    def load_from_file(path_to_file: str) -> 'Config':
        with open(path_to_file, 'r') as file:
            content = file.read()

        config = yaml.load(content, yaml.SafeLoader)

        return Config(
            lib_file_path=config['lib_file_path'],
            model_folder_path=config['model_folder_path'],
            config_file_path=config['config_file_path'],
            weights_file_path=config['weights_file_path'],
            meta_file_path=config['meta_file_path'],
            threshold=float(config['threshold']),
            hier_threshold=float(config['hier_threshold']),
            nms=float(config['nms']),
            classes_of_interest=config['classes_of_interest']
        )

    def generate_class_dict(self) -> Dict[int, str]:
        path_to_names_file = self._find_path_to_names()
        class_dict = dict()

        with open(path_to_names_file, 'r') as file:
            for i, name in enumerate(file):
                class_dict[i] = str(name).strip()

        return class_dict

    def _find_path_to_names(self) -> str:
        path_to_names = ''

        with open(path_join(self.model_folder_path, self.meta_file_path), 'r') as file:
            for line in file:
                if line[:6].lower() == 'names=':
                    path_to_names = line[6:].strip()
                    break

        return path_to_names

    def is_class_name_a_class_of_interest(self, class_name: str) -> bool:
        is_a_class_of_interest = False
        class_name = class_name.lower()

        for cof_name in self.classes_of_interest:
            if class_name == cof_name.lower():
                is_a_class_of_interest = True
                break

        return is_a_class_of_interest

