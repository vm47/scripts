#! /bin/sh

set -o errexit
set -o nounset

SCRIPT_DIR=$(dirname "$0")
BASE_DIR=$(cd "${SCRIPT_DIR}"/.. && pwd)
REPORTS_DIR="${BASE_DIR}"/reports
SRC_DIR="${BASE_DIR}"/src
ANALYTICS_DIR="${SRC_DIR}"/report
HTML_TEMPLATE="${BASE_DIR}"/templates/nbconvert/html/traces_comparison_report.tpl

IPYNB=detectors_comparison_analisys.ipynb
TMP_REPORT_IPYNB=tmp_report.ipynb
REPORT_NAME=detectors_comparison_metrics
CONFIG_PATH=''
DATASET_PATH=''
WEIGHTS_FOLDER_PATH=''

print_help() {
  printf "usage: %s\n" "$0"
  printf "\t\t\t\t[-c | --config <config_file_path>] [-d | --dataset <dataset_file_path>]\n"
  printf "\t\t\t\t[-h | --help] [-n | --name <report_name>] [-w | --weights <weights_folder_path>]\n"
  printf "\n"
  printf "Script creates detectors comparison metrics report in html & pdf\n"
  printf "Output directory: %s\n" "$REPORTS_DIR"
  printf "\n"
  printf "Required arguments:\n"
  printf "%s <config_file_path>\n" "-c, --config"
  printf "\t\t\tPath to config file.\n"
  printf "\n"
  printf "%s <dataset_file_path>\n" "-d, --dataset"
  printf "\t\t\tPath to dataset file.\n"
  printf "\n"
  printf "%s <weights_folder_path>\n" "-w, --weights"
  printf "\t\t\tPath to directory containing weights files.\n"
  printf "\n"
  printf "\n"
  printf "Optional arguments:\n"
  printf "%s\n" "-h, --help"
  printf "\t\t\tPrints this help page.\n"
  printf "\n"
  printf "%s <report_name>\n" "-n, --name"
  printf "\t\t\tReport name (Default: detectors_comparison_metrics).\n"
  printf "\n"
  exit 1
}

if [ $# -eq 0 ]; then
  printf "No arguments supplied!\n\n"
  print_help
fi

while [ -n "${1:-}" ]; do
  case $1 in
  -c | --config)
    shift
    CONFIG_PATH="$1"
    ;;
  -d | --dataset)
    shift
    DATASET_PATH="$1"
    ;;
  -h | --help)
    print_help
    ;;
  -n | --name)
    shift
    REPORT_NAME="$1"
    ;;
  -w | --weights)
    shift
    WEIGHTS_FOLDER_PATH="$1"
    ;;
  *) print_help ;;
  esac
  shift
done

HTML_REPORT="${REPORT_NAME}".html
PDF_REPORT="${REPORT_NAME}".pdf

mkdir -p "${REPORTS_DIR}"

papermill -p dataset_file_path "${DATASET_PATH}" -p config_file_path "${CONFIG_PATH}" \
  -p weights_folder_path "${WEIGHTS_FOLDER_PATH}" "${ANALYTICS_DIR}"/"${IPYNB}" "${REPORTS_DIR}"/"${TMP_REPORT_IPYNB}"

jupyter nbconvert --to html --no-input "${REPORTS_DIR}"/"${TMP_REPORT_IPYNB}" --output \
  "${REPORTS_DIR}"/"${HTML_REPORT}" --template="${HTML_TEMPLATE}"

rm -rf "${REPORTS_DIR:?}"/"${TMP_REPORT_IPYNB}"

wkhtmltopdf "${REPORTS_DIR}"/"${HTML_REPORT}" "${REPORTS_DIR}"/"${PDF_REPORT}"

echo
echo "HTML report: ${REPORTS_DIR}/${HTML_REPORT}"
echo "PDF  report: ${REPORTS_DIR}/${PDF_REPORT}"
exit 1
