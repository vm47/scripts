{% extends 'basic.tpl' %}


{% block header %}
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  {% set nb_title = nb.metadata.get('title', '') or resources['metadata']['name'] %}
  <title>{{nb_title}}</title>
  {% block styles %}
  {% endblock styles %}
</head>
{% endblock header %}


{% block body %}
<body>
  <div tabindex="-1" id="notebook" class="border-box-sizing">
    <div class="container" id="notebook-container">
      {{ super() }}
    </div>
  </div>
  {% block scripts %}
  {% endblock scripts %}
</body>
{% endblock body %}


{% block footer %}
{{ super() }}
</html>
{% endblock footer %}
