{% block tertiary_width_table %}

<style type="text/css">

table {
  padding: 0 0.5em;
  width: 30%;
  margin-bottom: 1rem;
  color: #212529;
  border-spacing: 0px;
}

table th{
  background-color: #AAA;
}

table th,
table td {
  vertical-align: top;
  text-align: left;
  border-top: 1px solid #dee2e6;
  border: 1px solid #dee2e6;
}

table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #dee2e6;
  border-bottom-width: 2px;
  cursor: pointer;
}

table tbody + tbody {
  border-top: 2px solid #dee2e6;
}

table tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

table tbody tr:hover {
  color: #212529;
  background-color: rgba(0, 0, 0, 0.075);
}

</style>

{% endblock tertiary_width_table %}
