{% extends './base.tpl' %}

{% block styles %}
{% include './styles/page.tpl' %}
{% include './styles/text.tpl' %}
{% include './styles/full_width_table.tpl' %}
{% endblock styles %}


{% block scripts %}
{% include './scripts/make_tables_sortable.tpl' %}
{% include './scripts/make_table_headers_always_visible.tpl' %}
{%  endblock scripts %}
