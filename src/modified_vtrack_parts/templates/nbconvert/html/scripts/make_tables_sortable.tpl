{% block make_tables_sortable %}

<script type="application/javascript">

(function init() {
  makeTablesSortable();

  function makeTablesSortable() {
    let headerCells = document.getElementsByTagName("th");

    for (let i = 0; i < headerCells.length; i++) {
      let cell = headerCells[i];
      cell.setAttribute("data-is_sort_ascending", true);
      cell.setAttribute("data-is_sorted", false);
      cell.innerText += " ";
      cell.addEventListener("click", handleOnTableHeaderCellClick);
    }
  }

  function handleOnTableHeaderCellClick() {
    let isSortAscending = (this.getAttribute("data-is_sort_ascending") === "true");

    let headRow = this.parentElement;
    let tableHeader = headRow.parentElement;
    let headerCells = tableHeader.getElementsByTagName("th");
    let table = tableHeader.parentElement;
    let tableBody = table.getElementsByTagName("tbody")[0];
    let tableRows = tableBody.getElementsByTagName("tr");

    quickSortTableRows(tableRows, this.cellIndex, isSortAscending);
    updateHeaderCells(headerCells, this, isSortAscending);

    function quickSortTableRows(tableRows, targetCellIndex, isSortAscending) {
      let partitions = [];
      partitions.push(createQuickSortPartition(0, tableRows.length - 1));


      while (partitions.length > 0) {
        let partition = partitions.pop();

        partition.arrangePartition(tableRows, targetCellIndex, isSortAscending)

        if (partition.pivotPosition > partition.start) {
          partitions.push(createQuickSortPartition(partition.start, partition.pivotPosition - 1));
        }

        if (partition.pivotPosition < partition.stop) {
          partitions.push(createQuickSortPartition(partition.pivotPosition + 1, partition.stop));
        }
      }
    }

    function createQuickSortPartition(start, stop) {
      return {
        start: start,
        stop: stop,
        pivotPosition: stop,
        arrangePartition: function(tableRows, targetCellIndex, isSortAscending) {
          this.pivotPosition = this.calculatePivotPosition();
          let pivot = tableRows[this.pivotPosition].getElementsByTagName("td")[targetCellIndex];
          this.switchRows(tableRows, this.pivotPosition, this.stop);
          this.pivotPosition = this.start;

          for (let i = this.start; i < this.stop; i++){
            let rowCell = tableRows[i].getElementsByTagName("td")[targetCellIndex];
            let cellCompare = this.compareCells(rowCell, pivot);

            if((!isSortAscending && cellCompare === 1) || (isSortAscending && cellCompare === -1)) {
              this.switchRows(tableRows, this.pivotPosition++, i);
            }
          }

          this.changeRowPosition(tableRows[this.stop], this.pivotPosition);
        },
        calculatePivotPosition: function() { return this.start + Math.round((this.stop - this.start) / 2); },
        switchRows: function(rows, i, j) {
          let tmpRow = rows[i].innerHTML;
          rows[i].innerHTML = rows[j].innerHTML;
          rows[j].innerHTML = tmpRow;
        },
        compareCells: function(cellA, cellB){
          if (!isNaN(cellA.textContent) && !isNaN(cellB.textContent)) {
            return this.compareNumberCells(cellA, cellB);
          } else {
            return cellA.textContent.toLowerCase().localeCompare(cellB.textContent.toLowerCase());
          }
        },
        compareNumberCells: function(cellA, cellB){
          let numA = parseFloat(cellA.textContent);
          let numB = parseFloat(cellB.textContent);

          if (numA < numB) {
            return -1;
          } else if (numA > numB) {
            return 1;
          } else {
            return 0;
          }
        },
        changeRowPosition: function(row, newPosition) {
          let tableBody = row.parentElement;
          tableBody.insertBefore(row, tableBody.children[newPosition]);
        }
      }
    }

    function updateHeaderCells(headerCells, targetCell, isSortAscending) {
      for (i = 0; i < headerCells.length; i++){
        hCell = headerCells[i];
        if (hCell.getAttribute("data-is_sorted") === "true") {
          text = hCell.innerText;
          text = text.slice(0, text.length - 2);
          hCell.innerText = text;
          hCell.setAttribute("data-is_sorted", false);
          hCell.setAttribute("data-is_sort_ascending", true);
        }
      }

      if (isSortAscending) {
        targetCell.innerText += " ▼";
      } else {
        targetCell.innerText += " ▲";
      }

      isSortAscending = !isSortAscending;
      targetCell.setAttribute("data-is_sort_ascending", isSortAscending);
      targetCell.setAttribute("data-is_sorted", true);
    }
  }
})();

</script>

{% endblock make_tables_sortable %}
