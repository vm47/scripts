{% block make_table_headers_always_visible %}

<script type="application/javascript">

(function init() {
  makeTableHeadersAlwaysVisible();

  function makeTableHeadersAlwaysVisible() {
    let fixedTableId = "fixed_table";
    let tableHeaders = document.getElementsByTagName("thead");

    for (let i = 0; i < tableHeaders.length; i++){
      tableHeaders[i].setAttribute("data-is_header_fixed", false);
    }

    document.addEventListener("scroll", handleOnScroll, false);
    window.addEventListener("resize", handleOnResize, false);

    function handleOnScroll(event){
      ensureTableHeaderVisibility();
    }

    function handleOnResize(event){

      for (let i = 0; i < tableHeaders.length; i++){
        if (tableHeaders[i].getAttribute("data-is_header_fixed") === "true") {
          destroyFixedTableHeader(tableHeaders[i]);
          createFixedTableHeader(tableHeaders[i]);
        }
      }
    }

    function ensureTableHeaderVisibility() {

      for (let i = 0; i < tableHeaders.length; i++){
        let header = tableHeaders[i];
        let body = header.parentElement.getElementsByTagName("tbody")[0];

        if (
          !(header.parentElement.getAttribute("id") === fixedTableId)
          && isElementTopBeyondScreenTop(header)
          && !isElementBottomBeyondScreenTop(body)
          && !(header.getAttribute("data-is_header_fixed") === "true")
        ){
          createFixedTableHeader(header);
        } else if (
          !(header.parentElement.getAttribute("id") === fixedTableId)
          && (
            (isElementBottomBeyondScreenTop(body) && header.getAttribute("data-is_header_fixed") === "true")
            || (!isElementTopBeyondScreenTop(header) && header.getAttribute("data-is_header_fixed") === "true")
          )
        ) {
          destroyFixedTableHeader(header)
        }
      }
    }

    function isElementTopBeyondScreenTop(element) {
      let rect = element.getBoundingClientRect();
      return rect.top <= 0;
    }

    function isElementBottomBeyondScreenTop(element) {
      let rect = element.getBoundingClientRect();
      return rect.bottom <= 0;
    }

    function createFixedTableHeader(tableHeader){
      let clonedHeader = cloneTableHeader(tableHeader);
      let table = createTableWithHeader(clonedHeader, fixedTableId);
      let htmlBody = document.getElementsByTagName("body")[0];

      table.style.top = 0;
      table.style.position = "fixed";

      htmlBody.appendChild(table);
      tableHeader.setAttribute("data-is_header_fixed", true);
    }

    function cloneTableHeader(header) {
      let clonedHeader = header.cloneNode(true);
      let headerCells = header.getElementsByTagName("th");


      for (let i = 0; i < headerCells.length; i++) {
        let clonedCell = clonedHeader.getElementsByTagName("th")[i];
        let cellRect = headerCells[i].getBoundingClientRect();
        let borderWidth = calculateCellBorderWidth(headerCells[i]);

        clonedCell.style.left = cellRect.left + "px";
        clonedCell.style.top = 0 + "px";
        clonedCell.style.position = "fixed";
        clonedCell.style.width = (cellRect.width - borderWidth) + "px";
        clonedCell.style.height = cellRect.height + "px";
        clonedCell.style.textAlign = "center";
      }

      return clonedHeader;
    }

    function calculateCellBorderWidth(cell) {
      let borderWidth = 0;
      let borderLeftWidth = parseInt(getComputedStyle(cell).getPropertyValue("border-left-width"));
      let borderRightWidth = parseInt(getComputedStyle(cell).getPropertyValue("border-right-width"));

      if (!isNaN(borderLeftWidth)) {
        borderWidth += borderLeftWidth;
      }

      if (!isNaN(borderRightWidth)) {
        borderWidth += borderRightWidth;
      }

      return borderWidth;
    }

    function createTableWithHeader(header, tableId) {
      let table = document.createElement("table");
      let tableBody = document.createElement("tbody");
      table.setAttribute("id", tableId);
      table.appendChild(header);
      table.appendChild(tableBody);

      return table;
    }

    function destroyFixedTableHeader(tableHeader) {
      let table = document.getElementById(fixedTableId);
      table.parentElement.removeChild(table);

      tableHeader.setAttribute("data-is_header_fixed", false);
    }
  }
})();

</script>
{% endblock make_table_headers_always_visible %}
