# Streaming Camera Setup

### Prerequisites

* Instalirati ffmpeg player

* Kreirati file 'ffserver.conf'

* Konfiguracfija streama (zapisano u ffserver.conf):

  ````
  HTTPPort    8091                # Port to bind the server to
  HTTPBindAddress  0.0.0.0
  MaxHTTPConnections  2000          #Max number of connections
  MaxClients   1000                   #Max number of clients
  MaxBandwidth    10000000            # Maximum bandwidth per client (set this high enough to exceed stream bitrate)
  
  CustomLog -
  
  <Feed camera-480.ffm>                       #feed za 480 rezoluciju
  File /tmp/camera-480.ffm                    
  FileMaxSize 50M
  ACL allow 127.0.0.1                         #dozvoljeni pristupi u range-u od 0 do 255
  ACL allow localhost
  ACL allow 192.168.0.0 192.168.255.255
  </Feed>
  
  <Feed camera-720.ffm>
  File /tmp/camera-720.ffm
  FileMaxSize 50M
  ACL allow 127.0.0.1
  ACL allow localhost
  ACL allow 192.168.0.0 192.168.255.255
  </Feed>
  
  <Feed camera-1080.ffm>
  File /tmp/camera-1080.ffm
  FileMaxSize 50M
  ACL allow 127.0.0.1
  ACL allow localhost
  ACL allow 192.168.0.0 192.168.255.255
  </Feed>
  
  <Stream camera-480>                         #Stream za rezoluciju 480   
  Feed camera-480.ffm                         #Feed za stream 480 
  Format mpjpeg                               #Format streama (Multipart JPEG (works with Netscape without any plugin))
  VideoFrameRate 40                           #Frame rate je smanjen na 40 zbog zagusenja
  VideoIntraOnly                              #Transmit only intra frames (useful for low bitrates, but kills frame rate).
  VideoBitRate 819200                         
  VideoBufferSize 81920
  VideoSize hd480                             #Podesavanje zeljene rezolucije(sa oznakom hd se moze samo def sirina zeljene rez.)
  VideoQMin 5                                 #The lowest the quantization is, the higher the bit rate and quality will be(the bit-rate is lower than the target, and some padding is added.)
  VideoQMax 51                                #The higher the quantization is, the lower the bit rate and quality will be(the bit-rate is higher than the target ans so the rate-control process will use a higher quantization level.)
  NoAudio
  </Stream>
  
  <Stream camera-720>
  Feed camera-720.ffm
  Format mpjpeg
  VideoFrameRate 40
  VideoIntraOnly
  VideoBitRate 819200
  VideoBufferSize 81920
  VideoSize hd720
  VideoQMin 5
  VideoQMax 51
  NoAudio
  </Stream>
  
  <Stream camera-1080>
  Feed camera-1080.ffm
  Format mpjpeg
  VideoFrameRate 40
  VideoIntraOnly
  VideoBitRate 819200
  VideoBufferSize 81920
  VideoSize hd1080
  VideoQMin 5
  VideoQMax 51
  NoAudio
  </Stream>
  
  ````

Frame rate u serv.conf je na drugoj kameri podesen na 50. Potrebno je provjeriti za svaku kameru kako se ponasaju sa podesenim framerateom!!!
Obe su radile ali u terminalu se pojave upozorenja o "past-duration"

## Pokrenuti stream server naredbom 

```bash
ffserver   -f      /etc/directory-path/ffserver.conf
```

`-f` specificira tocno odredeni fajl, inace ce pokrenuti svoj defalut conf za server iako navedemo tocnu putanju


## Starting ffmpeg under linux -comands

```bash
ffmpeg -rtsp_transport tcp \
  -i rtsp://admin:N7kemakui@192.168.1.173:554 \
  http://localhost:8091/camera-480.ffm \
  http://localhost:8091/camera-720.ffm \
  http://localhost:8091/camera-1080.ffm
````
`-rtsp_transport tcp` omogucava u real time citanje inputa u ovom slucaju rtsp live stream sa kamere
`-re` (Read input in real time) -opcionalno
`-r` (Set frame rate u omjeru i/o)

Postavke streama se definiraju prije svake oznake zasebno `-i` (input)

Moze se unijeti vise inputa koristeci svaki put `-i` ispred novog patha/linka/fajla i sve postavke se
ponistavaju za iduci input te ih je potrebno definirati za svaki input posebno ili u server conf-u

U ovom slucaju je lakse u server conf-u jer je stream sa kamere zahjtevan i dolazi do zagusenja odmah pri
ukljucenju jos jednog input-a

Streaming je uspostavljen za jednu kameru i njen stream, te je za drugu potrebno kreirati novi server conf
fajl i promjeniti HTTP port pregledavanja. Ova opcija je dala najbolje razultate.

Pri pokretanju obe kamere u dva terminala sa tri razlicita strema CPU usage je jako velik te se u jednom od
streamova pojavi kasnjenje. Ali opet bolji rezultat nego pokretati sve skupa.

### Prikazati stream u browseru, na adresi:

```bash
http://localhost:8090/camera-480
http://localhost:8090/camera-720
http://localhost:8090/camera-1080
```

Ako smo u server.conf definirali 8090 kao listening port.

### Za eksportati stream u m3u8 file potreban za js.video player koristit naredbu:

```bash
output.m3u8 
```

Na kraju naredbe u terminalu

### Play stream input sa kamere:
```bash
ffplay -i rtsp://admin:N7kemakui@192.168.1.173:554
```

Dodjeljenu adresu kamere se moze pranaci, postaviti i promjeniti u IVM-S aplikaciji Hikvision-a.

### INFO

Sto je broj iza naredbe `-r` manji, manja je razlika izmedu input frameova i output.
Ne strema frameove prema postavljenom broju nego ih u nekom omjeru dekodira od ulaznog strema.
Na vrijednosti jedan broj frameova sa kamere doseze 350 sto pri drugom zahjtevu za prikaz stvara
veliki delay i greske.

### Multiple outputs

Za svaki output je potrebno definirati feed i stream u odgovarajucem `server.conf` fajlu definiranom
za tu kameru te u terminalu definirati outpute jedan za drugim isto kao i inpute:

```bash
ffmpeg -rtsp_transport tcp \
  -i rtsp://admin:N7kemakui@192.168.1.173:554 \
  http://localhost:8091/camera-480.ffm \
  http://localhost:8091/camera-720.ffm \
  http://localhost:8091/camera-1080.ffm
```

### Informacije i izvori

#### vlc

https://support.actiontiles.com/communities/12/topics/3033-using-vlc-transcode-video-rtsp-stream-to-mjpeg

https://wiki.videolan.org/Documentation:Documentation/

#### other info

https://github.com/cvsandbox/cam2web

https://www.codeproject.com/Articles/1199725/cam-web-Streaming-camera-to-web-as-MJPEG-stream

https://www.codeproject.com/Tags/MJPEG

https://www.datastead.com/_releases/vidgrab_help/htmlhelp/ip_sp_cameras_sp_in_sp_mjpeg_sp_mode.htm

http://www.entner-electronics.com/en/jpeg-codec.html

https://koshka.ddns.net/?p=698

https://stackoverflow.com/questions/2245040/how-can-i-display-an-rtsp-video-stream-in-a-web-page?rq=1

https://wiki.videolan.org/Documentation%3aWebPlugin/

https://www.dacast.com/blog/live-stream-encoding-software/


#### pokusaji sa rtmp protokolom

https://community.ubnt.com/t5/UniFi-Video/3-3-Embed-RTSP-stream-directly-from-camera-into-webpage/td-p/1627476


https://community.ubnt.com/t5/UniFi-Video/Change-RTSP-port-on-UVC/m-p/1605770/highlight/false#M59438


#### obs player

http://www.obsremote.com/gettingstarted.html

https://obsproject.com/forum/threads/how-to-set-up-your-own-private-rtmp-server-using-nginx.12891/

https://www.ndchost.com/wiki/guides/how-to-stream-local-video-using-obs-and-mistserver


#### Stackoverflow question

https://stackoverflow.com/questions/55360330/want-to-embed-the-rtsp-stream-from-ip-camera-hikvision-on-the-web-page-in-mjp



#### SRS
- srs fajl se ne build-a dobro, izbaci gresku za FALLTROUGH, u c++ codu i par fajlova se treba promjeniti
ta linija koju sam pronasao ali sa svom dokumentacijom na netu nije uspjesno-- svaki put nakon spremanja i
ponovnog pokretanja taj fajl vrati promjenjenu liniju koda na staro. Nakon brake funkcije ''httpparser.c''
bi trebalo biti //FALLTROUGH// koju gcc ne prihvaca i izbaci vec spomenutu gresku. Funkcija je na 2093
liniji koda. Sa dokumentacijom sa interneta nisam uspio trajno promjeniti //falltrough// da se izvrsi cijeli
build do kraja.

https://medium.com/@yenthanh/setup-a-rtmp-livestream-server-in-15-minutes-with-srs-1b0046c77267


#### Hikvision

https://ipcamtalk.com/threads/how-to-enable-the-cgi-functions.22597/  (informacije u komantaru o cgi protokolu )

https://ipcamtalk.com/threads/is-ssh-or-telnet-available-in-5-4-5.19050/  ( ssh u kameru )
