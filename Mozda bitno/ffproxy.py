from time import sleep
import subprocess
import tempfile
import os

STREAM_HEADER = """
HTTPPort    {port}               
HTTPBindAddress  0.0.0.0
MaxHTTPConnections  1000         
MaxClients   1000                   
MaxBandwidth    {max_bandwith} 

CustomLog -

<Feed camera-{name}.ffm>
File /tmp/camera-{name}.ffm                    
FileMaxSize 50M
ACL allow 127.0.0.1
ACL allow localhost
ACL allow 192.168.0.0 192.168.255.255
</Feed>

<Stream camera-{name}>
Feed camera-{name}.ffm
Format mpjpeg
VideoFrameRate {frame_rate}
VideoIntraOnly
VideoBitRate {bit_rate} 
VideoBufferSize {buf_size}
VideoSize hd{name}
VideoQMin 5
VideoQMax 51
NoAudio
</Stream>
"""


class FFServer:
    STARTED_OK = 0
    PORT_USED = 1
    FAILED = 2

    def __init__(self, resolution, port, max_bandwith, frame_rate, bit_rate, buf_size):
        self.http_port = port or "8091"
        self.resolution = resolution
        self.filename = None
        self.max_bandwith = max_bandwith or "1000000"
        self.frame_rate = frame_rate or "40"
        self.bit_rate = bit_rate or "81920"
        self.buf_size = buf_size or "81920"
        self.process: subprocess.Popen = None
        self.config = STREAM_HEADER.format(
            port=self.http_port,
            max_bandwith=self.max_bandwith,
            name=self.resolution,
            frame_rate=self.frame_rate,
            bit_rate=self.bit_rate,
            buf_size=self.buf_size)
        # name is a reference of resolution(it should be 480, 720, or 1080)

    def start(self):
        if self.process is not None:
            return
        # create tmp config file
        (fd, self.filename) = tempfile.mkstemp(suffix='.conf')
        with open(self.filename, 'w') as f:
            f.write(self.config)
            print(self.http_port)
            os.close(fd)

        args = ['ffserver', '-f', self.filename]
        self.process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        sleep(1)
        if not self.is_alive():
            if 'Address already in use' in str(self.process.stderr.read()):
                return self.PORT_USED
            return self.FAILED

        return self.STARTED_OK

    def stop(self):
        # stop process
        if self.process is not None:
            self.process.terminate()
            self.process = None
        # delete tmp config file
        if self.filename is not None:
            os.remove(self.filename)
            self.filename = None

    def status(self):
        if self.process is not None:
            ffserver_output = self.process.stdout.read()
            return ffserver_output

    def is_alive(self):
        return self.process is not None and self.process.poll() is None

    def exit_code(self):
        if self.process is not None and self.process.poll() is not None:
            return self.process.returncode


class FFMpeg:
    def __init__(self, camera_url, output):
        self.camera_url = camera_url
        self.output = output
        self.process: subprocess.Popen = None

    def start(self):
        if self.process is None:
            commands = ['ffmpeg', '-rtsp_transport', 'tcp', '-i', self.camera_url, self.output]
            self.process = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def stop(self):
        if self.process is not None:
            self.process.terminate()
            self.process = None

    def status(self):
        if self.process is not None:
            ffmpeg_output = self.process.stdout.read() and self.process.stderr.read()
            return ffmpeg_output


    def is_alive(self):
        return self.process is not None and self.process.poll() is None

    def exit_code(self):
        if self.process is not None and self.process.poll() is None:
            return self.process.returncode


OUTPUT_TEMPLATE = 'http://{ip}:{port}/camera-{name}.ffm'


class StreamingSystem:
    def __init__(self, resolution, port, camera_url, max_bandwith, frame_rate, bit_rate, buf_size):
        self.resolution = resolution
        self.port = port
        self.camera_url = camera_url
        self.max_bandwith = max_bandwith
        self.frame_rate = frame_rate
        self.bit_rate = bit_rate
        self.buf_size = buf_size
        self.ffserver = None

    def _create_server(self, bit_rate, buf_size, frame_rate, max_bandwith, port, resolution):
        for a in range(100):
            p = port + a
            s = FFServer(resolution, p, max_bandwith, frame_rate, bit_rate, buf_size)
            tmp = s.start()
            print(tmp)
            if tmp == FFServer.STARTED_OK:
                return s

    def start(self):
        self.ffserver = self._create_server(self.bit_rate, self.buf_size, self.frame_rate, self.max_bandwith, self.port, self.resolution)
        output = OUTPUT_TEMPLATE.format(ip='127.0.0.1', port=self.ffserver.http_port, name=self.resolution)

        self.ffmpeg = FFMpeg(self.camera_url, output)
        self.ffmpeg.start()

    def stop(self):
        self.ffserver.stop()
        self.ffmpeg.stop()

    def status(self):
        print(self.ffserver.status())
        print(self.ffmpeg.status())
        pass
