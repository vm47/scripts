import RPi.GPIO as GPIO
import time
import subprocess
GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)#Button to GPIO23
GPIO.setup(15, GPIO.OUT)
            

try:
    while True:
        button_state = GPIO.input(23)
        if button_state == False:
            GPIO.output(15,GPIO.LOW) 
            print ("Button pressed...")
            args = ['raspistill', '-o', 'ice5.jpg']
            subprocess.Popen(args)
            time.sleep(1)
            #send_picture = ['sshpass', '-p', 'markovic', 'scp', '/home/pi/Desktop/camera1.png', 'vlado@192.168.0.109:/home/vlado/Desktop']
            #subprocess.Popen(send_picture)
            time.sleep(5)
            GPIO.output(15, GPIO.HIGH)
            break
            
except:
    GPIO.cleanup()
    
