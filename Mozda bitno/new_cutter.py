import argparse
import sys
import datetime
import os
import time

supported_formats = [".mp4",".avi"]
filename = ""
cnt = 0

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--times", required="True")
parser.add_argument("-i", "--input", required="True")

args = parser.parse_args()

if not args.times.endswith(".txt"):
    print("Wrong extension: File {0} containing times needs to be .txt".format(args.times))
    sys.exit()
if args.input[-4:] not in supported_formats:
    print("Unsupported video format of an input file")
  
    sys.exit()
    
filename = args.input

def t_t_s(t):
    return t.hour*3600 + t.minute*60 + t.second

def s_t_t(t):
    return str(datetime.timedelta(seconds=t))

with open(args.times) as f:
    for line in f.readlines():
        print(line)
        times_lst = line.rstrip().split("-")
        hh,mm,ss = times_lst[0].split(",")
        s_time = datetime.time(int(hh),int(mm),int(ss))

        hh,mm,ss = times_lst[1].split(",")
        e_time = datetime.time(int(hh),int(mm),int(ss))

        t_diff = t_t_s(e_time)-t_t_s(s_time) + 1
        cnt = cnt + 1
        os.system("ffmpeg -ss " + str(s_time) + " -i " + filename + " -t " + s_t_t(t_diff)  + " -c copy " + str(time.time()).replace(".","")  + ".mp4")


