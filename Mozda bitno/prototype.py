from __future__ import print_function
import cv2
import matplotlib.pyplot as plt
import numpy as np

from utils.image_manipulation import find_reference,find_rectangle_dims
from utils.config_reader import read

max_lowThreshold = 100
window_name = 'Edge Map'
title_trackbar = 'Min Threshold:'
ratio = 3
kernel_size = 3

MAX_FEATURES = 8500
GOOD_MATCH_PERCENT = 0.2




def align_images(im1, im2, descriptor, matcher):

    keypoints1, descriptors1 = descriptor.detectAndCompute(im1, mask=None)
    keypoints2, descriptors2 = descriptor.detectAndCompute(im2, mask=None)
    # Match features
    matches = matcher.match(descriptors1, descriptors2)
    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=True)

    # Remove not so good matches
    num_good_matches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:num_good_matches]

    # Draw top matches
    im_matches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, outImg=None)
    shape = im_matches.shape
    #im_matches = cv2.resize(im_matches, (shape[0]//2, shape[1]//5))
   # cv2.imshow('matches', im_matches)

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt

    # Find homography
    homography_matrix, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
    # Use homography
    height, width, channels = im2.shape
    im1_warped = cv2.warpPerspective(im1, homography_matrix, (width, height))


    return im1_warped, homography_matrix

def CannyThreshold(val):
    low_threshold = val
    img_blur = cv2.blur(src_gray, (3, 3))
    detected_edges = cv2.Canny(img_blur, low_threshold, low_threshold * ratio, kernel_size)
    mask = detected_edges != 0
    dst = src * (mask[:, :, None].astype(src.dtype))

#    cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)  #dodano
    #dst1=cv2.resize(dst,(1200,1600))   #dodano
#    cv2.imshow(window_name, dst)

    return dst


if __name__ == '__main__':

    conf1_filename = '/home/dluser/Desktop/config/a.json'
    config1 = read(conf1_filename)

    conf2_filename = '/home/dluser/Desktop/config/b.json'
    config2 = read(conf2_filename)

    ref1_filename = '/home/dluser/Desktop/config/00000-a.jpg'
    ref1 = cv2.imread(ref1_filename)

    ref2_filename = '/home/dluser/Desktop/config/00000-b.jpg'
    ref2 = cv2.imread(ref2_filename)

    im_filename = '/home/dluser/Desktop/00080-a.jpg'

    im = cv2.imread(im_filename)
    orb_descriptor = cv2.ORB_create(MAX_FEATURES)  #patchsize=21 ili 51
    bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    ref, imgnum = find_reference(im, [ref1, ref2], orb_descriptor, bf_matcher, 10)

    # Rotate if landscape
    if im.shape[1] > im.shape[0]:
        im = cv2.rotate(im, cv2.ROTATE_90_CLOCKWISE)

    # Match chosen ref image size
    im = cv2.resize(im, (ref.shape[1], ref.shape[0]))
    im_warped, h_mat = align_images(im, ref, orb_descriptor, bf_matcher)
    plt.imshow(im_warped)
    plt.show()

    src = im_warped

    plt.imshow(im_warped)
    plt.show()
    diff = cv2.absdiff(im_warped, ref)
    plt.imshow(diff)
    plt.show()

    src_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)

#    cv2.namedWindow(window_name)
#    cv2.createTrackbar(title_trackbar, window_name, 0, max_lowThreshold, CannyThreshold)

    img = CannyThreshold(70)  #63 za mobilna1
#    cv2.waitKey()
    plt.imshow(img)
    plt.show()

    if imgnum == 0:
        config = config1
    else:
        config = config2

    width, height = find_rectangle_dims(ref)

    for checkbox in config['checkboxes']:
        bbox = checkbox['bbox']
        x = int(bbox[0] * width)+4
        y = int(bbox[1] * height)+5
        w = int(bbox[2] * width*0.5)
        h = int(bbox[3] * height*0.5)

        patch = img[y:y+h, x:x+w]
        num_nonzero = np.count_nonzero(patch)

        if num_nonzero > 5:
            print(checkbox['desc'])
