import os
from pandas import *


def find_results_of_dataset():
    list_of_files = []
    names_of_directories = os.listdir('.')
    for dir in names_of_directories:
        if os.path.isdir(dir):
            if 'results.txt' in os.listdir(dir):
                list_of_files.append(os.sep.join([dir, 'results.txt']))

    return list_of_files


def column_names():
    list_of_names = find_results_of_dataset()
    names_of_columns = [v.split('/')[0] for v in list_of_names]

    return names_of_columns


def read_results_content():
    locations_needed = find_results_of_dataset()
    newlist = []

    for location in locations_needed:
        with open(location) as loc:
            lines = loc.read().split('\n')[:7]
            newlist.append(lines)
    return newlist


def dictionary():
    new_list = read_results_content()
    names = column_names()
    list_of_values = []
    list_of_names = []

    for a in new_list:
        first_list = []
        second_list = []
        for x in a:
            v = x.replace('Recall = ', '')
            y = v.replace('Precision = ', '')
            z = y.replace('=', ':')
            q = z.split(':')
            o = q[0]
            t = q[1]
            second_list.append(t)
            first_list.append(o)
        list_of_values.append(second_list)
        list_of_names.append(first_list)

    df = DataFrame(list_of_values, names, columns=list_of_names[0])
    df.to_csv('Results.csv')
    return list_of_names, list_of_values


dictionary()
