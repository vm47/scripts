import cv2
from shapely.geometry import Polygon
from src.replay.sqlite3 import Sqlite3Player, Sqlite3Recorder

gt_frames = 0
det_list = []
gt_number = 0



class IOU:
    """Class that calculates the IOU of bounding boxes. Input data is from a database where the ground truth
    and detections are written, the second input is directly from the detector"""



    def __init__(self):


        self.bb_ground_truth = None
        self.bb_detector = None
        self.match = False
        # self.connection = False
        self.recorder = None
        self.player = None
        self.session = None
        self.recorder = Sqlite3Recorder()
        self.player = Sqlite3Player()



    def __call__(self, ground_truth_data, detector_data):
        return self.calculate_iou(ground_truth_data, detector_data)

    def get_bb_cords_from_db(self):

        path = self.recorder.last_recording_path()
        self.session = self.player.open_session(path)
        self.player.start_loop()

        ground_truth_data = self


        f_d = self.player.get_frame_data()




    def close_database(self):
        self.player.close_db()



    def calculate_iou(self, ground_truth_data, detector_data):

        gt_points = ground_truth_data
        det_points = detector_data

        gt_points = self.bounding_box_coord(gt_points[0], gt_points[1])
        det_points = self.bounding_box_coord(det_points.point1, det_points.point2)

        # print(gt_points, det_points)

        self.bb_ground_truth = Polygon(gt_points)
        self.bb_detector = Polygon(det_points)

        if self.bb_detector.within(self.bb_ground_truth):
            area = 1
        elif self.bb_ground_truth.within(self.bb_detector):
            area = 1
        else:
            intersection_area = self.bb_ground_truth.intersection(self.bb_detector).area
            union_area = self.bb_ground_truth.union(self.bb_detector).area

            area = (intersection_area / union_area)

        if area > 0.5:
            # print("Bounding boxes match, IOU is {}\n".format(area * 100))
            self.match = True
        return area




class BBDrawer:

    def __init__(self):
        self.gtt = 0
        self.dett = 0

    iu = IOU()
    global gt_number

    for gt in gts:
        gt_number += 1
        self.gtt += 1

        for det in dets:
            if det:
                area = (iu(gt, det))
                    if (area * 100) > 70:
                        self.dett += 1




    def video_stream(self, video):
        return cv2.VideoCapture(video)

    def close_stream(self):
        self.stream.release()




def detector_accuracy(params):
    global gt_frames
    gt = params[0]
    det = params[1]
    det_list.append(det)

    if gt == 0:
        return 0
    elif gt != 0:
        gt_frames += 1
        return det / gt



    # def write_result():
    #     tnd = 0
    #     for f in det_list:
    #         tnd += f
    #
    #     av = (list_of_averages[-1]) * 100
    #     ac = str('Average detector accuracy : ' + (str(round(av))) + ' %' + '\n')
    #     fr = str('Total number of frames : ' + str(frame.frame_num) + '\n')
    #     fgt = str('Total number of frames with ground truth bb : ' + str(gt_frames) + '\n')
    #     det = str('Total number of detector detections : ' + str(tnd) + '\n')
    #     gt = str('Total number of ground truth bounding boxes : ' + str(gt_number) + '\n')
    #
    #     file = open(file_name + '.txt', mode='w', encoding='utf-8')
    #
    #     file.writelines(ac)
    #     file.writelines(fr)
    #     file.writelines(fgt)
    #     file.writelines(det)
    #     file.writelines(gt)
    #     file.close()
    #
    # write_result()
