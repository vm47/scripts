import datetime
import os
import cv2
import numpy as np

from printer import Printer
from utils.config_reader import read
from utils.image_manipulation import find_reference, find_rectangle_dims

ratio = 3
kernel_size = 3
MAX_FEATURES = 8500
GOOD_MATCH_PERCENT = 0.5


def align_images(im1, im2, descriptor, matcher):
    keypoints1, descriptors1 = descriptor.detectAndCompute(im1, mask=None)
    keypoints2, descriptors2 = descriptor.detectAndCompute(im2, mask=None)
    # Match features
    matches = matcher.match(descriptors1, descriptors2)
    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=True)

    # Remove not so good matches
    num_good_matches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:num_good_matches]

    # Draw top matches
    im_matches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, outImg=None)
    shape = im_matches.shape
    im_matches = cv2.resize(im_matches, (shape[0]//2, shape[1]//5))

    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)

    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt

         # Find homography
    homography_matrix, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
        # Use homography
    height, width, channels = im2.shape
    im1_warped = cv2.warpPerspective(im1, homography_matrix, (width, height))

    return im1_warped, homography_matrix


def cannythreshold(val, src, src_gray, ratio, kernel_size):
    low_threshold = val
    img_blur = cv2.blur(src_gray, (3, 3))
    detected_edges = cv2.Canny(img_blur, low_threshold, low_threshold * ratio, kernel_size)
    mask = detected_edges != 0
    dst = src * (mask[:, :, None].astype(src.dtype))
    return dst


def itemization():
    if os.path.exists("/home/vlado/Desktop/camera1.png"):
        printer = Printer(0x067b, 0x2303, 0, 0x81, 0x02)

        conf1_filename = '/home/vlado/Desktop/rebaca/config/a.json'
        config1 = read(conf1_filename)

        conf2_filename = '/home/vlado/Desktop/rebaca/config/b.json'
        config2 = read(conf2_filename)

        ref1_filename = '/home/vlado/Desktop/rebaca/config/00000-a.jpg'
        ref1 = cv2.imread(ref1_filename)

        ref2_filename = '/home/vlado/Desktop/rebaca/config/00000-b.jpg'
        ref2 = cv2.imread(ref2_filename)

        im_filename = '/home/vlado/Desktop/camera1.png'

        im = cv2.imread(im_filename)
        orb_descriptor = cv2.ORB_create(MAX_FEATURES)  #patchsize=21 ili 51
        bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

        ref, imgnum = find_reference(im, [ref1, ref2], orb_descriptor, bf_matcher, 10)
        # Rotate if landscape
        if im.shape[1] > im.shape[0]:
            im = cv2.rotate(im, cv2.ROTATE_90_CLOCKWISE)

            # Match chosen ref image size
        im = cv2.resize(im, (ref.shape[1], ref.shape[0]))
        im_warped, h_mat = align_images(im, ref, orb_descriptor, bf_matcher)
        src = im_warped
        #diff = cv2.absdiff(im_warped, ref)
        src_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)

        img = cannythreshold(30, src, src_gray, ratio, kernel_size)  #63 za mobilna1

        if imgnum == 0:
            config = config1
        else:
            config = config2

        width, height = find_rectangle_dims(ref)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        item_list = []

        for checkbox in config['checkboxes']:
            bbox = checkbox['bbox']
            x = int(bbox[0] * width)+5
            y = int(bbox[1] * height)+6
            w = int(bbox[2] * width)
            h = int(bbox[3] * height*0.75)

            patch = img[y:y+h, x:x+w]
            num_nonzero = np.count_nonzero(patch)

            if num_nonzero > 60:
                item_list.append(checkbox['desc'] + "/" + checkbox['desc_eng'])
                print(checkbox['desc'])
                print(checkbox['desc_eng'])
                print(num_nonzero)
        printer.print(item_list)

        if os.path.exists("/home/vlado/Desktop/camera1.png"):
             dt = str(datetime.datetime.now())
             newname = 'camera' + dt + '.png'
             os.rename("/home/vlado/Desktop/camera1.png", "/home/vlado/Desktop/Obradene_fotografije/"+newname)
