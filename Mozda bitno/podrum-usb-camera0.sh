#!/bin/bash



#has connection, assuming that it has connection
HC=1
#connection lost first time
CLFT=0
#seconds without connection
SWC=0
#something is wrong with our Internet connection, assuming that it has connection
SWWOIC=1
#something is wrong with theirs Internet connection, assuming that it has connection
SWWTIC=1
#notification has been fired
NHBF=0
#which side lost connection
WSLC=""
#timestamp when connection was lost
TWCWL=""
#Email has been sent
EHBS=0
#port
PORT=555
#ffmpeg PID
FPID=0
#video name counter
CNT=0
#send e-mail flag
SEF=0
#debugging is ON
DIO=0
#time untill mail is sent
TUMIS=0
#time for how much script will be running
TFHMSWBR=10
#resolution for recording
RFR=3840x2160
#video name
VN=""

while [ ! $# -eq 0 ]
do
	case "$1" in
		--help | -h)
			cat help.txt
            exit 1
			;;
		--time | -t)
			TFHMSWBR=$2
			;;
        --email | -e)
            SEF=1
            TUMIS=$2
            ;;
        --debugging | -d)
            DIO=1
            ;;
        --resolution | -r)
            RFR=$2
            ;;
        --name | -n)
            VN=$2
            ;;
	esac
	shift
done

function make_Day() {
    DATE=$(date +%d.%m.)
    mkdir $DATE
    mv ./*.mp4 $DATE
    if [ -f log.txt ]; then
        mv ./log.txt $DATE
    fi

    pth="/home/marko/"$DATE

    cmd_out=$(rsync -avz -e 'ssh -i /root/.ssh/id_rsa' $pth backuphsar@192.168.1.135:/home/backuphsar/BACKUP/ 2>&1)

    echo $cmd_out > t.txt

}

function debugg() {
    if [[ "$DIO" -eq  "1" ]]; then
        echo $1
    fi
}

function check_IPE() {
    if ! ps -p $FPID > /dev/null
    then
        start_FFMPEG
    fi
}

function start_FFMPEG() {
    D=$(date +%d.%m.)
    ffmpeg -i http://192.168.1.168:8091/camera0 -t $(($TFHMSWBR+200)) -s $RFR -c copy  $D$CNT".mp4" &
    FPID=$!
    CNT=$((CNT+1))
}

function send_Mail() {
    sendEmail -f marko.kljakovic.snimke@gmail.com -t marko.kljakovic.snimke@gmail.com -u This is just a test -m "Hello World" -s smtp.gmail.com:587 -xu marko.kljakovic.snimke -xp snimke123\!Snimke -o tls=yes
}

function check_Connection() {
    #PORT 91
	connection_status=$(nc -zv 192.168.1.159 $PORT -w 2 2>&1)
    if [[ "$connection_status" =~ "Network is unreachable" ]]; then
        HC=0
        SWWOIC=0
        WSLC="OUR"
    elif [[ "$connection_status" =~ "succeeded" ]]; then
        connection_status=$(nc -zv 192.168.1.159 $PORT -w 2 2>&1)
        if [[ "$connection_status" =~ "Network is unreachable" ]]; then
            HC=0
            SWWOIC=0
            WSLC="OUR"
            elif [[ "$connection_status" =~ "succeeded" ]]; then
            HC=1
            SWWOIC=1
            SWWTIC=1
        else
            HC=0
            SWWTIC=0
            WSLC="THEIR"
        fi
    else
        HC=0
        SWWTIC=0
        WSLC="THEIR"
    fi

}

while true; do

    check_Connection
    sleep 1
    if [[ "$HC" -eq "0" ]]; then
        if [[ "$CLFT" -eq "0" ]]; then
            kill $FPID
            SWC=$SECONDS
            CLFT=1
            TWCWL=$(date 2>&1)
            NHBF=0
        fi
    else
        check_IPE
    fi
    if [[ "$SWWOIC" -eq "1" ]] && [[ "$SWWTIC" -eq "1" ]] && [[ "$CLFT" -eq "1" ]] && [[ "$NHBF" -eq "0" ]]; then
        NHBF=1
        CLFT=0
        echo "$TWCWL -- Connection has been lost from $WSLC side for $(($SECONDS-$SWC)) seconds" >> log.txt
    fi


    if [[ "$SEF" -eq "1" ]]; then
        debugg "Set email flag is working"
        if [[ "$SWWOIC" -eq "1" ]] && [[ "$SWWTIC" -eq "0" ]] && [[ "$(($SECONDS-$SWC))" -gt "$TUMIS" ]] && [[ "$EHBS" -eq "0" ]]; then
            EHBS=1
            send_Mail
        fi
    fi

    if [[ "$SECONDS" -gt "$TFHMSWBR" ]]; then
        kill $FPID
        sleep 1
        break
    fi

done

#make_Day



exit 1
