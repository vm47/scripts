# Hikvison cameras setup and display

Za ispravno povezane IP kamere na istu mrezu (subnet), potreban je SADP tool, aplikacija od Hikvision koja ce
prva detektirati prikljucene uređaje na istoj mrezi.

Nakon sto mozemo vidjeti IP adrese uređaja, u SADP-u ih mozemo fiksirati I mjenjati, a zatim instalirati IVMS-400,
aplikaciju za prikaz I snimanje streama sa kamere, ili IVMS Lite, bez dosta opcija za management ali puno manje memorije
trosi za rad. I dodati zeljene adrese urađaja za nadzor.

Za prikazati stream sa druge mreze, tj. Kamera koja nije fizicki na istom mjestu kao I ostale, potrebno je taj
uređaj prijaviti na HikConnect, (snimac ili kamere pojedinacno), napraviti korisnicki racun, omoguciti hik cloud u
konfiguraciji kamere, odabrati zeljeni pin za potvrdu prilikom spajanja na HikConnect, takoder u konfiguraciji kamere,
na njenoj ip adresi u Internet Exploreru.

Napravljena su dva hikclouda. Jedan sa dvije kamere iz zgrade Croatia Osiguranja:

    user: husar47
    pass: N7kemakui
    ver. code: N7kemakui

I drugi

    user: hhusar2019
    pass: ZGhhusar2019
    ver.code: 1Q2W3E.

U prvom accountu su dodani serijski brojevi kamera u Splitu, a u drugom, serijski broj kamere iz Zagreba.

Nakon prijave na IVMS aplikaciju sa podacima za korisnicki racun sa kamerom iz Zagreba, unutar iste ce se pojaviti
stream sa prijavljenog uređaja, bilo to kamera ili snimač.

A zatim pronaci adrese, ostale dvije kamere sa vlastite pod mreze, I dodati ih naknadno.

Stream sa clouda, se moze snimati I pregledavati na isti nacin kao I vlastite kamere, ali se nemoze pristupati
konfiguraciji (mjenjati postavke izvan IVMS-a) dodanog uredaja bez njegove IP adrese.

## Opcije za snimanje

Svaka IP kamera ima opciju lokalnog snimanja, na svoju SD disk (karticu).

Druga opcija je koristenje IVMS aplikacije, koja radi samo na windowsu. Kamera je povezana na svitch. A racunalo na
istoj mrezi sa instaliranom aplikacijom snima njen output, na putanju koju vec odredimo. Moguce je definirati drugaciju
putanju, na vanjski disk, ili NAS.

Detaljne upute:
https://us.hikvision.com/sites/default/files/tb/quick_start_guide_of_hikvision_ip_camera_synology_nas_connection_v1.1_0.pdf


Treca opcija je koristiti snimac sa vlastitim softwerom, koji automatski snima na dodjeljeni prostor.
Najbolja opcija sa najmanje smetnji.

Popis network video recordera:
https://us.hikvision.com/en/products/video-recorders/network-video-recorder

EZVIZ stranica na kojoj se moze pogledati stream nakon sto se napravi hikcloud. Na internet exploreru !!


### Kamera Zagreb (choco caffe – vanjska)

    Bandwith: 1,2 MBps

Podaci za spajanje preko Teamviewera:

    Team viewer id (Zagreb laptop-choko): 1282920137
    pass: N7kemakui
    Zagreb laptop pass: 12qw12qw

Hikconnect account za kameru iz zagreba:

	username: hhusar 2019
	pwd: ZGhhusar2019
	verifikacijski kod je: 1Q2W3E

Ip adresa kamere 192.168.8.101 (u palacinkari)

	na portu: 91 mozemo pristupiti konfiguraciji kamere (inace 80)
	username: choko
	pwd: Choko2019

Admin podaci za kameru iz Zg-a

    user: admin
    pwd: 2019Husar2019
