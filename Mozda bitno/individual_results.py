import os


def find_folders():
    list_of_files = []
    names_of_directories = os.listdir('.')

    for name in names_of_directories:
        if str(name) == 'in':
            list_of_files.append(name)
        elif str(name) == 'out':
            list_of_files.append(name)

    return list_of_files


def find_results():
    f = find_folders()
    cwd = os.getcwd()
    folder = cwd + '/' + f[0]
    folder_content = os.listdir(folder)

    just_folders_with_det = []

    for item in folder_content:
        path = folder + '/' + item
        if os.path.isdir(path):
            just_folders_with_det.append(path)

    return just_folders_with_det


def extract_results():
    jfd = find_results()
    results_path = []

    for path in jfd:
        f = os.listdir(path)

        for item in f:

            if '.txt' in item:
                results_path.append(path + '/' + item)

    return results_path


def read_results():
    full_path = extract_results()
    detector_detections_total_num = 0
    ground_truth_total_num = 0

    for x in full_path:
        f = open(x, mode='r', encoding='utf-8')
        line = f.readlines()
        arg_gt = line[-1]
        arg_det = line[-2]

        det_num_full = arg_det.split(':')
        gt_num_full = arg_gt.split(':')

        det_num = int(det_num_full[-1])
        gt_num = int(gt_num_full[-1])

        detector_detections_total_num += det_num
        ground_truth_total_num += gt_num

    return detector_detections_total_num / ground_truth_total_num, detector_detections_total_num, ground_truth_total_num


def average_percentage_method():
    path = extract_results()

    just_percentage = 0
    total_res = 0

    for y in path:
        f = open(y, mode='r', encoding='utf-8')

        first_line = f.readline()
        perc_fir_lin = first_line.split(':')
        percentage = (perc_fir_lin[-1])
        perc = percentage.strip().strip('%')
        just_percentage += int(perc)

        total_res += 1

    return just_percentage / total_res


def write_result():

    decimal_result = read_results()[0]
    average_percentage = round(average_percentage_method())
    percentage = round(decimal_result * 100)
    total_det = read_results()[1]
    total_gt = read_results()[2]
    name = find_folders()[0]

    file_name = ('Results_for_' + name + '.txt')

    a = str('Average detector accuracy (in total - det/gt) : ' + str(percentage) + '%' + '\n')
    b = str('Total number of detections : ' + str(total_det) + '\n')
    c = str('Total number of ground truth : ' + str(total_gt) + '\n')
    d = str('Total percentage by average percentage method : ' + str(average_percentage) + '%' + '\n')

    file = open(file_name, mode='w', encoding='utf-8')

    file.writelines(a)
    file.writelines(b)
    file.writelines(c)
    file.writelines(d)
    file.close()


write_result()
