import argparse
import csv
import os

path1 = '../'


def find_results_of_dataset():
    list_of_files = []

    names_of_directories = os.listdir('..')

    for dir in names_of_directories:
        if os.path.isdir('../' + dir):
            if 'results.txt' in os.listdir('../' + dir):
                list_of_files.append(os.sep.join(['../' + dir, 'results.txt']))

    return list_of_files


def column_names():
    list_of_names = find_results_of_dataset()
    names = sorted([v.split('/')[-2] for v in list_of_names])

    return names


def read_results_content():
    locations_needed = find_results_of_dataset()
    new_list = []

    for location in locations_needed:
        with open(location, "r") as loc:
            rdr = list(csv.reader(loc))[:7]
            # with open('newfile', 'w+') as s:
            #     s.write(rdr + '\n')
            new_list.append(rdr)
    return new_list
# column_names()

read_results_content()
print(read_results_content())

def create_pandas_dataframe():

#     from pandas import DataFrame
#
# Cars = {'Brand': ['Honda Civic','Toyota Corolla','Ford Focus','Audi A4'],
#         'Price': [22000,25000,27000,35000]
#         }
#
# df = DataFrame(Cars, columns= ['Brand', 'Price'])
#
# export_csv = df.to_csv (r'C:\Users\Ron\Desktop\export_dataframe.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path
#
# print (df)

    pass


def write_results_to_csv():
    pass


'''
if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-dir", "--working_directory", required=False,
                    help="path to directory where all generated videos and "
                         "files are", default=path)
    ap.add_argument("-name", "--estimation", required=True, help="path to ground estimation txt")
    ap.add_argument("-out", "--output", required=True, help="path where to save output files")
    
    args = vars(ap.parse_args())
'''
