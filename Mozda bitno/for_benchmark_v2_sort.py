import argparse
from pyexcel_ods import get_data
import os
from pathlib import Path

ap = argparse.ArgumentParser()
ap.add_argument("-gt", "--ground_truth_table", required=False, help="path to ground truth txt")
ap.add_argument("-est", "--estimation", required=True, help="path to ground estimation txt")
ap.add_argument("-out", "--output", required=True, help="path where to save output files")
args = vars(ap.parse_args())

def return_txt(file_path, output_path, name="ground_truth.txt"):
    '''Returns txt file from .ods file type'''

    def write_to_file():
        '''Func that only writes to file'''

        with open(output_path+"/"+name, "w") as f:
            for frame, flag in data:
                f.write(str(frame)+" "+str(flag)+"\n")

    data = get_data(file_path)
    data = list(data.items())
    # print(data)
    #Sheet1 with all cols and rows
    data = data[0][1]
    #samo spremi one redove koji imaju out ili in u sebi
    data = [bitno for bitno in data for b in bitno if b=='OUT' or b=='IN']
    #izbaci eventualno prazno polje u svakom pojedinom podnizu u nizu
    data = [[b for b in bitno if b] for bitno in data]
    #spremi samo drugi stupac svakog podniza, jer on je broj frames
    frame_num = [b for el in data for b in el if el.index(b)==1]
    #spremi flag da li je ulaz ili izlaz
    flags = [f for el in data for f in el if el.index(f)==2]

    #frame_num = frame_num[:30]
    #flags = flags[:30]

    data = zip(frame_num, flags)

    #path to check
    f = Path(output_path+"/"+name)
    if f.is_file():
        print("File ground_truth.txt exists in output folder.")

        while True:
            rewrite = input("Do you want to rewrite it (y/n): ")

            if not rewrite:
                print("Input must be y or n")
                rewrite = input("Do you want to rewrite it (y/n): ")

            if rewrite=='y':
                #rewrites existing txt file
                write_to_file()
                break
            elif rewrite=='n':
                break
    else:
        #create new txt file
        write_to_file()

def to_list(file_path):
    '''Converts txt file data to a 1D list'''
    with open(file_path) as gt:
        truth = [el.split(' ') for el in gt]
        frame = [int(f) for el in truth for f in el if el.index(f)==0]
        flag = [str(f.replace('\n', '')) for el in truth for f in el if (el.index(f)==1 and ('OUT' in f.upper() or 'IN' in f.upper()))]
    #returns list of frame numbers, list of flags(in, out) and lenght of input
    return frame, flag, len(truth)

def generate(ar):

    #generate txt
    return_txt(ar["ground_truth_table"], ar["output"])

    gts_frame, gts_flag, len_gt = to_list(ar["output"]+"/ground_truth.txt")
    est_frame, est_flag, len_est = to_list(ar["estimation"])

    # print(est_frame, est_flag)

    gts = list(zip(gts_frame, gts_flag))
    
    def func(t):
        return t[0]

    estimations = list(zip(est_frame, est_flag))
    estimations.sort(key = func)
    #print(estimations)

    gts_in = [g for g in gts if g[1].upper() == 'IN']
    gts_out = [g for g in gts if g[1].upper() == 'OUT']

    estimations_in = [g for g in estimations if g[1].upper() == 'IN']
    estimations_out = [g for g in estimations if g[1].upper() == 'OUT']

    true_positive = []
    true_positive_in = []
    true_positive_out = []

    gts_remain = gts.copy()

    for gt in gts:
        for est in estimations:
            if 0 <= (gt[0]-est[0]) <= 10 or 0 >= (gt[0]-est[0]) >= -10:

                #TRUE POSITIVE IN
                if gt[1].upper() == est[1].upper() == 'in'.upper():

                    #TRUE POSITIVE ALL
                    true_positive.append((gt[0], est[0]))

                    true_positive_in.append((gt[0], est[0], 'IN'))

                    estimations.remove(est)
                    gts_remain.remove(gt)

                    break
                #TRUE POSITIVE OUT
                elif gt[1].upper() == est[1].upper() == 'out'.upper():

                    #TRUE POSITIVE ALL
                    true_positive.append((gt[0], est[0]))

                    true_positive_out.append((gt[0], est[0], 'OUT'))

                    estimations.remove(est)
                    gts_remain.remove(gt)

                    break

    # print(len(estimations))
    # print(len(gts_remain))

    #OPTIONAL - for convert_to_time.py
    #for isolation of specific frames
    with open(ar["output"]+"/no_detect.txt", "w") as f:
        for i in gts_remain:
            f.write(str(i[0])+","+str(i[1])+"\n")
    #
    with open(ar["output"]+"/false_detect.txt", "w") as f:
        for i in estimations:
            f.write(str(i[0])+","+str(i[1])+"\n")

    #ISPISUJE BROJ GROUND TRUTHA I DUZLJINU ESTIMACIJA

    with open(ar["output"]+"/results.txt", "w", newline='\n') as results:
        results.write("Ground truth: "+ str(len_gt) + '\n')
        results.write("Estimations: "+ str(len_est) + '\n')
        results.write("True positives all: "+ str(len(true_positive)) + '\n')
        results.write("False positives all: "+ str(abs(len(true_positive)-len_est)) + '\n')
        results.write("Estimations / GT = " + str(100 * len_est / len_gt) + '%\n')
        results.write("Recall = TP / GT = " + str(100 * len(true_positive) / len_gt) + '%\n')
        results.write("Precision = TP / Estimations = " + str(100 * len(true_positive) / len_est) + '%\n\n')

        results.write("Ground truth in: "+ str(len(gts_in)) + '\n')
        results.write("Estimations in: "+ str(len(estimations_in)) + '\n')
        results.write("True positives in: "+ str(len(true_positive_in)) + '\n')
        results.write("False positives in: "+ str(abs(len(true_positive_in)-len(estimations_in))) + '\n')
        results.write("Estimations / GT = " + str(100 * len(estimations_in) / len(gts_in)) + '%\n')
        results.write("Recall = TP / GT = " + str(100 * len(true_positive_in) / len(gts_in)) + '%\n')
        results.write("Precision = TP / Estimations = " + str(100 * len(true_positive_in) / len(estimations_in)) + '%\n\n')

        results.write("Ground truth out: "+ str(len(gts_out)) + '\n')
        results.write("Estimations out: "+ str(len(estimations_out)) + '\n')
        results.write("True positives out: "+ str(len(true_positive_out)) + '\n')
        results.write("False positives out: "+ str(abs(len(true_positive_out)-len(estimations_out))) + '\n')
        results.write("Estimations / GT = " + str(100 * len(estimations_out) / len(gts_out)) + '%\n')
        results.write("Recall = TP / GT = " + str(100 * len(true_positive_out) / len(gts_out)) + '%\n')
        results.write("Precision = TP / Estimations = " + str(100 * len(true_positive_out) / len(estimations_out)) + '%\n\n')


        # true_positive_all = round((len(true_positive)/len_gt)*100, 2)
        # true_positive_in = round((len(true_positive_in)/len(gts_in))*100, 2)
        # true_positive_out = round((len(true_positive_out)/len(gts_out))*100, 2)

if __name__ == "__main__":
    generate(args)
