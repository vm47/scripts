# OMNITRACK

Multiple camera person / object tracker.


## Installation instructions

Install Anaconda for Python3.7 (https://www.anaconda.com/distribution/)

Create conda environment:
conda create --name tracker

Activate tracker environment:
conda activate tracker

Instal setup tools:
conda install -c anaconda setuptools

Install numpy:
pip install numpy

Install opencv:
pip install opencv-python

Install cython:
conda install -c anaconda cython 

Install tensorflow with gpu support:
conda install -c anaconda tensorflow-gpu

Install filterpy (contains kalman filter):
conda install -c conda-forge filterpy

Download neural network weights:
./download_weights.sh

