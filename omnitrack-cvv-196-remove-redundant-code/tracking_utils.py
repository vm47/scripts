import time
import os
import cv2
from shapely.geometry import LineString, Polygon, Point
import json
from src.database_writer.database_writer import DatabaseWriter

class FileWriter:

    def __init__(self, input_file_name):
        self.input_file_name = input_file_name
        self.data = {}

    def update_data(self, data):
        self.data.update(data)

    def write_to_json(self):

        with open(self.input_file_name+".json", 'w') as file:
            json.dump(self.data, file, indent=4)


def run_tracking(tracker, path_to_video, video_record=False):

    file_name = os.path.splitext(os.path.basename(path_to_video))[0]

    entry_estimation = open("ulasci.txt", "w")
    time_elapsed_per_frame = open("time_elapsed.txt", "w")

    cap = cv2.VideoCapture(path_to_video)
    counter = 0
    # for video recording
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter("./output.mp4", fourcc, 20, (1280, 720), True)

    writer = FileWriter(file_name)

    db_writer = DatabaseWriter("counter", "toni")

    while True:
        counter += 1
        # dict to write to json
        # {id: bb}
        multiple_ids = {}

        prev_time = time.time()

        res, frame = cap.read()

        if not res:
            print("Video end!")
            break

        tracker.update(frame)
        #db_writer.write_to_db(int(cap.get(cv2.CAP_PROP_POS_FRAMES)), tracker.single_id_trackers)

        for id_tracker in tracker.single_id_trackers:

            # kalman filter points
            point1 = (int(id_tracker.kalman_filter.get_state()[0, 0]), int(id_tracker.kalman_filter.get_state()[0, 1]))
            point2 = (int(id_tracker.kalman_filter.get_state()[0, 2]), int(id_tracker.kalman_filter.get_state()[0, 3]))

            cv2.rectangle(frame, point1, point2, id_tracker.color, 2)
            cv2.putText(frame, str(id_tracker.id), point1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.0, id_tracker.color)

            print(int(cap.get(cv2.CAP_PROP_POS_FRAMES)), id_tracker.id)

            multiple_ids[str(id_tracker.id)] = (point1, point2)


        #draw frame number on window
        cv2.putText(frame, str(int(cap.get(cv2.CAP_PROP_POS_FRAMES))), (170, 130), cv2.FONT_HERSHEY_COMPLEX_SMALL, 3.0, (0, 0, 255))

        writer.update_data({str(int(cap.get(cv2.CAP_PROP_POS_FRAMES))): multiple_ids})

        cv2.imshow('Tracker', frame)
        # cv2.waitKey(0)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        if video_record:
            out.write(frame)

    writer.write_to_json()

    cap.release()
    out.release()

def set_visible_gpus(gpus):
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpus)
