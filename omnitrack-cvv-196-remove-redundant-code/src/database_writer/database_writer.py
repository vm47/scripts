import psycopg2


class DatabaseWriter:
    def __init__(self, database, user):
        self.conn = psycopg2.connect("dbname="  + database + " user=" + user)
        self.cur = self.conn.cursor()

    def insert_tracker_data(self, frame, person_id, p1, p2):
        self.cur.execute("SELECT * FROM tracker;")
        print(self.cur.fetchone())
        x1, y1 = p1
        x2, y2 = p2
        self.cur.execute("insert into tracker(frame, person_id, x1, y1, x2, y2) values (%s, %s, %s, %s, %s, %s);",
                         (frame, person_id, x1, y1, x2, y2))

    def insert_detector_data(self, frame, person_id, p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        self.cur.execute("insert into detector(frame, person_id, x1, y1, x2, y2) values (%s, %s, %s, %s, %s, %s);",
                         (frame, person_id, x1, y1, x2, y2))

    def write_to_db(self, frame, id_trackers):
        for id_tracker in id_trackers:
            person_id = id_tracker.id
            coordinates = id_tracker.kalman_filter.get_state()
            tracker_p1 = (int(coordinates[0][0]), int(coordinates[0][1]))
            tracker_p2 = (int(coordinates[0][2]), int(coordinates[0][3]))
            detector_p1 = id_tracker.object_detection.point1
            detector_p2 = id_tracker.object_detection.point2
            self.insert_tracker_data(frame, person_id, tracker_p1, tracker_p2)
            self.insert_detector_data(frame, person_id, detector_p1, detector_p2)
            self.conn.commit()
