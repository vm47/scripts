from random import randint

from . import BoxKalmanFilter


class IdTracker:
    """
    Class that represents a single tracked identity.
    It tracks and updates the state of single identity (person).
    """

    def __init__(self, feature_vector, detection, id):
        """
        Initialize IdTracker instance using feature_vector and object_detection as initial state of tracker.
        New IdTracker instance should be created only when new person is detected.

        :param feature_vector: initial descriptor for the tracked object
        :param detection: first detection of a new identity
        """
        self.feature_vector = feature_vector
        self.object_detection = detection
        self.kalman_filter = BoxKalmanFilter(detection.point1 + detection.point2)
        self.count = 0
        self.id = id
        self.color = (randint(0, 255), randint(0, 255), randint(0, 255))

    #NEW
        self.traj_id = id
        self.traj_color = self.color
        self.traj_points = []
        self.has_intersected = False

    def add_point(self, point):
        self.traj_points.append(point)

    def intersect_with_line(self, input_line, traj_line):

        check = traj_line.intersects(input_line)

        return check

    def set_point(self):
        self.points = {}
    #END


    def __update_feature_vector(self, feature_vector):
        """Updates feature vector to new state based on feature vector of the last detection."""
        self.feature_vector = 0.94 * self.feature_vector + 0.06 * feature_vector

    def update(self, feature_vector, detection):
        """Updates the state of tracked object.

        :param feature_vector: feature vector of the last detection of the tracked ID
        :param detection: last detection of the tracked ID
        """
        self.object_detection = detection
        self.__update_feature_vector(feature_vector)
        self.kalman_filter.update(detection.point1 + detection.point2)
        self.count += 1

