class Counter:
    def __init__(self, estimator_list):
        self.estimator_list = estimator_list
        self.entrance_count = 0
        self.exit_count = 0
        self.map = {}

    def count(self, frame_number, id_to_bbox_map):
        estimation_results_list = \
            list(map(lambda estimator: estimator.estimate(frame_number, id_to_bbox_map), self.estimator_list))

        for ls in estimation_results_list:
            for id, frame, in_p, out_p in ls:
                if id not in self.map.keys():
                    self.map[id] = []

                self.map[id].append((frame, in_p, out_p))


        # if len(self.estimator_list) == 1:
        #     return estimation_results_list[0]
        #


    def get_results(self):
        event_dict = {}
        for id, ls in self.map.items():
            max_el = max(ls, key = lambda ev: ev[1] if ev[1] > ev[2] else ev[2])
            frame_num, in_p, out_p = max_el
            if in_p > out_p:
                event_dict[id] = (frame_num, 'IN')
            else:
                event_dict[id] = (frame_num, 'OUT')

        return event_dict
