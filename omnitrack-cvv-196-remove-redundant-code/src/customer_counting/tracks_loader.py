import json


from src.customer_counting.abstract_tracks_loader import AbstractTracksLoader


class TracksLoader(AbstractTracksLoader):
    def __init__(self, tracks_file):
        with open(tracks_file) as jf:
            self.frame_dict = json.load(jf)

    def num_frames(self):
        return len(self.frame_dict)

    def get_id_bbox_mapping(self, frame_idx):
        return self.frame_dict[str(frame_idx + 1)]
