from abc import ABC, abstractclassmethod

class AbstractEstimator(ABC):
    """Interface that all classes that estimate INs and OUTs should implement. For every new method that estimates
    INs and OUTs new implementation of this class should be writen."""

    @abstractclassmethod
    def estimate(self, frame_number, id_to_bbox_map):
        """
        :param id_to_bbox_map: dictionary that maps id to its bounding box
        :return: list of IN/OUT estimates with (Id, frame_number, probability_IN, probability_OUT) format
        """
