from .abstract_estimator import AbstractEstimator
from shapely.geometry import LineString, Polygon, Point



from src.tracking import IdTracker


class BasicEstimator(AbstractEstimator):
    def __init__(self, entrance_lines, entrance_poly):
        self.entrance_lines = entrance_lines
        self.entrance_poly = entrance_poly
        self.id_trackers = {}

    def estimate(self, frame_number, id_to_bbox_map):
        events = []

        for id, coordinates in id_to_bbox_map.items():

            # NEW
            # index 1 je x, index 0 je y
            # point1 = id_tracker.object_detection.point1
            # point2 = id_tracker.object_detection.point2

            id = int(id)

            if not id in self.id_trackers.keys():
                self.id_trackers[id] = IdTracker(None, None, id)

            id_tracker = self.id_trackers[id]

            # kalman filter points
            point1 = (int(coordinates[0][0]), int(coordinates[0][1]))
            point2 = (int(coordinates[1][0]), int(coordinates[1][1]))

            # tracker_center_point = (int(point1[0] + abs(point1[0]-point2[0])/2), int(point1[1] + abs(point1[1]-point2[1])/2))

            point_to_track = (int(point1[0] + abs(point1[0] - point2[0]) / 2), int(point2[1]))

            id_tracker.add_point(point_to_track)

            if not id_tracker.has_intersected:

                if len(id_tracker.traj_points) == 1 or len(id_tracker.traj_points) == 0:
                    break
                # if i == max(id_tracker.traj_points.keys()):
                #     break

                # print(id_tracker.traj_points)
                line_to_check = LineString([(id_tracker.traj_points[-2][0], id_tracker.traj_points[-2][1]),
                                            (id_tracker.traj_points[-1][0], id_tracker.traj_points[-1][1])])

                # TODO: this should be replaced with single Shapely polygon intersection check
                # TODO: replace id_tracker.has_intersected with state (in or out)
                for line in self.entrance_lines:
                    # FIX FPFN (1)
                    if id_tracker.intersect_with_line(LineString(list(line)), line_to_check):

                        if Point(id_tracker.traj_points[-1][0], id_tracker.traj_points[-1][1]).within(
                                self.entrance_poly) \
                                and not Point(id_tracker.traj_points[-2][0], id_tracker.traj_points[-2][1]).within(
                            self.entrance_poly):

                            events.append((id_tracker.id, frame_number, 0, 1.0))
                            id_tracker.has_intersected = True

                        elif not Point(id_tracker.traj_points[-1][0], id_tracker.traj_points[-1][1]).within(
                                self.entrance_poly) \
                                and Point(id_tracker.traj_points[-2][0], id_tracker.traj_points[-2][1]).within(
                            self.entrance_poly):

                            events.append((id_tracker.id, frame_number, 1.0, 0))
                            id_tracker.has_intersected = True

                        break

        return events
