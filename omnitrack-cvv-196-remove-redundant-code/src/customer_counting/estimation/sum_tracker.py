from shapely.geometry import LineString, Point


class SumTracker:
    def __init__(self,id):
        self.count = 0
        self.id = id

        self.traj_points = []
        self.has_intersected = False
        self.state_estimated = False
        self.intersection_step = None
        self.intersection_frame_number = None
        self.offset = 70
        self.points_before = 0
        self.points_after = 0

    def add_point(self, point):
        self.traj_points.append(point)

    def intersect_with_line(self, entrance_lines, frame_number):
        line_to_check = LineString([(self.traj_points[-2][0], self.traj_points[-2][1]),
                                    (self.traj_points[-1][0], self.traj_points[-1][1])])

        for line in entrance_lines:
            check = LineString(list(line)).intersects(line_to_check)

            if check:
                self.has_intersected = True
                self.intersection_step = self.count
                self.intersection_frame_number = frame_number
                break

    def get_direction_of_moving(self):
        (x1, y1), (x2, y2) = self.get_points()
        return x2 - x1, y2 - y1

    def get_points(self):
        offset = 70

        first_point_idx = self.intersection_step - self.offset if self.intersection_step - self.offset >= 0 else 0
        second_point_idx = self.intersection_step + self.offset if self.intersection_step + self.offset < len(
            self.traj_points) else len(self.traj_points) - 1

        return self.traj_points[first_point_idx:self.intersection_step], self.traj_points[self.intersection_step:second_point_idx]

    def update(self, entrance_lines, frame_number):
        if self.count >= 2 and not self.has_intersected:
            self.intersect_with_line(entrance_lines, frame_number)

        self.count += 1

    def is_mature(self):
        if not self.has_intersected:
            return False
        else:
            return self.count - self.intersection_step > self.offset

    def get_state(self, poly):
        self.state_estimated = True
        before_intersection, after_intersection = self.get_points()

        y_ref = 410

        y_before_intersection = [y for (x, y) in before_intersection]
        y_after_intersection = [y for (x, y) in after_intersection]

        sum_before_intersection = sum([y - y_ref for y in y_before_intersection])
        sum_after_intersection = sum([y - y_ref for y in y_after_intersection])

        return sum_before_intersection, sum_after_intersection
