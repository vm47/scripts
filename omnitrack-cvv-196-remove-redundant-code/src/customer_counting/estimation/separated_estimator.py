from .abstract_estimator import AbstractEstimator

from .trajectory_tracker import TrajectoryTracker


class SeparatedEstimator(AbstractEstimator):
    def __init__(self, entrance_lines, entrance_poly, exit_lines, exit_poly):
        self.entrance_lines = entrance_lines
        self.entrance_poly = entrance_poly
        self.entrance_id_trackers = {}

        self.exit_lines = exit_lines
        self.exit_poly = exit_poly
        self.exit_id_trackers = {}

    def estimate(self, frame_number, id_to_bbox_map):
        events = []
        for id, coordinates in id_to_bbox_map.items():

            id = int(id)

            if not id in self.entrance_id_trackers.keys():
                self.entrance_id_trackers[id] = TrajectoryTracker(id)

            if not id in self.exit_id_trackers.keys():
                self.exit_id_trackers[id] = TrajectoryTracker(id)

            entrance_id_tracker = self.entrance_id_trackers[id]
            entrance_id_tracker.update(self.entrance_lines, frame_number)

            exit_id_tracker = self.exit_id_trackers[id]
            exit_id_tracker.update(self.exit_lines, frame_number)

            point1 = (int(coordinates[0][0]), int(coordinates[0][1]))
            point2 = (int(coordinates[1][0]), int(coordinates[1][1]))

            point_to_track = (int(point1[0] + abs(point1[0] - point2[0]) / 2), int(point2[1]))

            entrance_id_tracker.add_point(point_to_track)
            exit_id_tracker.add_point(point_to_track)

        for id, id_tracker in self.entrance_id_trackers.items():
            if (id_tracker.is_mature() or (str(id) not in id_to_bbox_map.keys() and id_tracker.has_intersected)) and not id_tracker.state_estimated:
                in_p, out_p = id_tracker.get_state(self.entrance_poly)
                events.append((id_tracker.id, id_tracker.intersection_frame_number, in_p, 0))

        for id, id_tracker in self.exit_id_trackers.items():
            if (id_tracker.is_mature() or (str(id) not in id_to_bbox_map.keys() and id_tracker.has_intersected)) and not id_tracker.state_estimated:
                in_p, out_p = id_tracker.get_state(self.entrance_poly)
                events.append((id_tracker.id, id_tracker.intersection_frame_number, 0, out_p))

        return events
