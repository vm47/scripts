from .abstract_estimator import AbstractEstimator

from .trajectory_tracker import TrajectoryTracker


class PointCountingEstimator(AbstractEstimator):
    def __init__(self, entrance_lines, entrance_poly):
        self.entrance_lines = entrance_lines
        self.entrance_poly = entrance_poly
        self.id_trackers = {}

    def estimate(self, frame_number, id_to_bbox_map):
        events = []
        for id, coordinates in id_to_bbox_map.items():

            id = int(id)

            if not id in self.id_trackers.keys():
                self.id_trackers[id] = TrajectoryTracker(id)

            id_tracker = self.id_trackers[id]
            id_tracker.update(self.entrance_lines, frame_number)

            point1 = (int(coordinates[0][0]), int(coordinates[0][1]))
            point2 = (int(coordinates[1][0]), int(coordinates[1][1]))

            point_to_track = (int(point1[0] + abs(point1[0] - point2[0]) / 2), int(point2[1]))

            id_tracker.add_point(point_to_track)

        for id, id_tracker in self.id_trackers.items():
            if (id_tracker.is_mature() or (str(id) not in id_to_bbox_map.keys() and id_tracker.has_intersected)) and not id_tracker.state_estimated:
                in_p, out_p = id_tracker.get_state(self.entrance_poly)
                events.append((id_tracker.id, id_tracker.intersection_frame_number, in_p, out_p))

        return events
