from abc import ABC, abstractmethod


class AbstractTracksLoader(ABC):
    """
    Abstract class that represents object track loader. All tracks annotation loader classes should derive this class.
    The main purpose of this class is to be an interface that any class that serves as a loader of track annotations
    for a specific video should implement. It is an iterable class. The iteration is done over consecutive frames of
    the video. The iterator returns id to bounding box mappings for a frame.
    """
    @abstractmethod
    def num_frames(self):
        """
        :return: number of frames in the video
        """
        pass

    @abstractmethod
    def get_id_bbox_mapping(self, frame_idx):
        """
        Gets the mapping from person ids to corresponding bounding boxes.

        :param frame_idx: index of a specific frame in the video
        :return: dict containing person ids as keys and corresponding bounding boxes as values
        """
        pass

    def __iter__(self):
        return AbstractTracksLoader.__TracksLoaderIterator(self)

    class __TracksLoaderIterator:
        def __init__(self, tracks_loader):
            self.tracks_loader = tracks_loader
            self.next_frame_idx = 0
            self.num_frames = tracks_loader.num_frames()

        def __next__(self):
            i = self.next_frame_idx

            if i < self.num_frames:
                self.next_frame_idx += 1
                return self.tracks_loader.get_id_bbox_mapping(i)

            raise StopIteration

        def __iter__(self):
            return self
