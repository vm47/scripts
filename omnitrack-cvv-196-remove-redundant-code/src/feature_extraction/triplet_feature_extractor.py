import os
import sys
from importlib import import_module

import cv2
import numpy as np
import tensorflow as tf

from .triplet_reid.aggregators import AGGREGATORS

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), 'triplet_reid'))


class TripletFeatureExtractor:
    """
    Feature extractor class based on Triplet-reid. For more details see
    "In Defense of the Triplet Loss for Person Re-Identification" paper (https://arxiv.org/abs/1703.07737).

    Extracts (description) features from image of a person. Uses a Tensorflow model from original Triplet-reid
    github repo as backend.
    """

    @staticmethod
    def __flip_augment(image):
        """ Returns both the original and the horizontal flip of an image. """
        images = tf.concat([image, tf.reverse(image, [1])], axis=0)
        return images

    @staticmethod
    def __five_crops(image, crop_size):
        """ Returns the central and four corner crops of `crop_size` from `image`. """
        image_size = tf.shape(image)[1:3]
        crop_margin = tf.subtract(image_size, crop_size)

        assert_size = tf.assert_non_negative(
            crop_margin, message='Crop size must be smaller or equal to the image size.')
        with tf.control_dependencies([assert_size]):
            top_left = tf.floor_div(crop_margin, 2)
            bottom_right = tf.add(top_left, crop_size)
        center = image[:, top_left[0]:bottom_right[0], top_left[1]:bottom_right[1], :]
        top_left = image[:, :-crop_margin[0], :-crop_margin[1], :]
        top_right = image[:, :-crop_margin[0], crop_margin[1]:, :]
        bottom_left = image[:, crop_margin[0]:, :-crop_margin[1], :]
        bottom_right = image[:, crop_margin[0]:, crop_margin[1]:, :]
        return center, top_left, top_right, bottom_left, bottom_right

    def __init__(self, model_name, head_name, checkpoint, embedding_dim=128, flip_augment=False,
                 crop_augment=False, aggregator=None, net_input_size=(256, 128), pre_crop_size=(288, 144)):
        """ Initialize feature extractor instance.

        :param (str) model_name: name of the model, options in net.NET_CHOICES
        :param (str) head_name: name of the top module of the model, options in head.HEAD_CHOICES
        :param checkpoint: path to model weights
        :param embedding_dim: dimension of the embedding
        :param (boolean) flip_augment: use horizontal flip if True
        :param (str or boolean) crop_augment: specifies if cropping augmentation should be used,
               options: 'center', 'five', 'avgpool' or False if no crop augmentation should be used
        :param aggregator: specifies how augmentations, if they are used, should be aggregated,
                           options: aggregator.AGGREGATORS
        :param net_input_size: size of input images in the form (height, width)
        :param pre_crop_size: size of pre crop images in the form (height, width)
        """
        self.model = import_module('nets.' + model_name)
        self.head = import_module('heads.' + head_name)
        self.checkpoint = checkpoint
        self.embedding_dim = embedding_dim
        self.flip_augment = flip_augment
        self.crop_augment = crop_augment
        self.aggregator = AGGREGATORS[aggregator] if aggregator else None
        self.net_input_size = net_input_size
        self.pre_crop_size = pre_crop_size

        self.images = tf.placeholder(name='input_data', shape=[None, None, None, 3], dtype=tf.float32)
        augmented_images = self.images

        # Augment the data if specified by the arguments.
        # `modifiers` is a list of strings that keeps track of which augmentations
        # have been applied, so that a human can understand it later on.
        self.modifiers = ['original']
        if flip_augment:
            augmented_images = TripletFeatureExtractor.__flip_augment(augmented_images)
            self.modifiers = [o + m for m in ['', '_flip'] for o in self.modifiers]

        if crop_augment == 'center':
            augmented_images = tf.image.resize_images(augmented_images, pre_crop_size)
            center, _, _, _, _ = TripletFeatureExtractor.__five_crops(augmented_images, net_input_size)
            augmented_images = center
            self.modifiers = [o + '_center' for o in self.modifiers]

        elif crop_augment == 'five':
            augmented_images = tf.image.resize_images(augmented_images, pre_crop_size)
            center, top_left, top_right, bottom_left, bottom_right = \
                TripletFeatureExtractor.__five_crops(augmented_images, net_input_size)
            augmented_images = tf.concat([center, top_left, top_right, bottom_left, bottom_right], axis=0)
            self.modifiers = [o + m for o in self.modifiers for m in [
                '_center', '_top_left', '_top_right', '_bottom_left', '_bottom_right']]

        elif crop_augment == 'avgpool':
            augmented_images = tf.image.resize_images(augmented_images, pre_crop_size)
            self.modifiers = [o + '_avgpool' for o in self.modifiers]

        else:
            augmented_images = tf.image.resize_images(augmented_images, net_input_size)
            self.modifiers = [o + '_resize' for o in self.modifiers]

        endpoints, body_prefix = self.model.endpoints(augmented_images, is_training=False)

        with tf.name_scope('head'):
            self.endpoints = self.head.head(endpoints, embedding_dim, is_training=False)

        # set allow_growth=True to fix some memory issues (see https://www.tensorflow.org/guide/using_gpu)
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        # Initialize the network/load the checkpoint.
        print('Restoring from checkpoint: {}'.format(checkpoint))
        tf.train.Saver().restore(self.sess, checkpoint)

    def get_features(self, images):
        """Computes features for input images.

        :param images: list of (numpy arrays) images for which features will be extracted
        :return numpy array: 2D array with features of the input images, index of first dimension
        corresponds to different input images, second dimension is the embedding dimension
        """
        batch_size = len(images)

        images = [cv2.resize(image, (self.net_input_size[1], self.net_input_size[0])) for image in images]
        images = np.stack(images, axis=0)

        embedding = self.sess.run(self.endpoints['emb'], feed_dict={self.images: images})

        if len(self.modifiers) > 1:
            # Pull out the augmentations into a separate first dimension (idx==0).
            embedding = embedding.reshape(len(self.modifiers), batch_size, -1)

            # Aggregate along augmentation dimension according to the specified parameter.
            embedding = self.aggregator(embedding)

        return embedding
