import os
import cv2
import numpy as np
import tensorflow as tf


from .darknet_utils import load_net_custom, load_meta,make_image, network_height, network_width, copy_image_from_bytes, detect_image
from . import ObjectDetection


class Yolov3DarknetDetector:
    """
    Class representing YOLO v3 object detector. The net is implemented in darknet framework.
    Architecture and configuration of the network is specified in a set of configuration files
    which are passed as arguments to constructor.
    """

    def __init__(self, config_path, weight_path , meta_path, network_input_size=(416,416)):
        """
        Initialize YOLO v3 detector.

        :param protobuf_file: path to protobuf file which contains architecture and weights of the detection network
        :param detectable_classes_file: path to file containing list of classes network was trained to detect
        :param detection_classes: list of classes network should detect during inference, should be a
                                  subset of detectable classes (classes contained in detectable_classes_file)
        :param network_input_size: size (height, width) to which input image will be resized before being passed to
                                   the detection network
        """

        self.network_input_size = network_input_size

        self.netMain = None
        self.altNames = None
        self.metaPath = meta_path
        self.metaMain = None

        if self.netMain is None:
            self.netMain = load_net_custom(config_path.encode(
                "ascii"), weight_path.encode("ascii"), 0, 1)  # batch size = 1

        if self.altNames is None:
            try:
                with open(self.metaPath) as metaFH:
                    metaContents = metaFH.read()
                    import re
                    match = re.search("names *= *(.*)$", metaContents,
                                      re.IGNORECASE | re.MULTILINE)
                    if match:
                        result = match.group(1)
                    else:
                        result = None
                    try:
                        if os.path.exists(result):
                            with open(result) as namesFH:
                                namesList = namesFH.read().strip().split("\n")
                                self.altNames = [x.strip() for x in namesList]
                    except TypeError:
                        pass
            except Exception:
                pass

        if self.metaMain is None:
            self.metaMain = load_meta(self.metaPath.encode("ascii"))
            # print(self.metaPath)

        self.darknet_image = make_image(network_width(self.netMain), network_height(self.netMain), 3)

    def __transform_points(self, box, img_size):
        """
        Helper function used to transform detections from coordinates
        of resized image to coordinates of the original image.

        :param box: detection that is output directly from detection network
        :param img_size: size of the original image in format (height, width)
        :return: two points of detected object in coordinate system of the original (non-resized) image
        """

        box = list(box)
        for i, coordinate in enumerate(box):

            box[i] = max(coordinate, 0)

        p1 = (int(box[0] * img_size[1] / self.network_input_size[0]),
              int(box[1] * img_size[0] / self.network_input_size[1]))

        p2 = (int(box[2] * img_size[1] / self.network_input_size[0]),
              int(box[3] * img_size[0] / self.network_input_size[1]))

        return p1, p2

    def __convert_point_from_darknet(self, x, y, w, h):
        """
        Helper function used to get four points from darknet format box detection output

        :param x: x of box center
        :param y: y of box center
        :param w: box width
        :param h: box height
        :return:
        """

        xmin = int(round(x - (w / 2)))
        xmax = int(round(x + (w / 2)))
        ymin = int(round(y - (h / 2)))
        ymax = int(round(y + (h / 2)))
        return xmin, ymin, xmax, ymax


    def detect(self, image):
        """Detect objects specified in detection_classes.

        :param image: image in which objects should be detected
        :return: list of object detections (ObjectDetection instances)
        """

        frame_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        img_resized = cv2.resize(frame_rgb,
                                (network_width(self.netMain),
                                network_height(self.netMain)))

        #darknet_image = make_image(network_width(self.netMain), network_height(self.netMain), 3)
        copy_image_from_bytes(self.darknet_image, img_resized.tobytes())

        res = detect_image(self.netMain, self.metaMain, self.darknet_image, thresh=0.4)


        object_detections = []

        for i in res:

            score = i[1]
            label = i[0]
            box = i[2]

            points = self.__convert_point_from_darknet(*box)
            p1, p2 = self.__transform_points(points, image.shape)

            object_detection = ObjectDetection(label, score, p1, p2, image)
            object_detections.append(object_detection)

        return object_detections

