import argparse
import cv2
import os


from src.customer_counting.estimation.point_counting_estimator import PointCountingEstimator
from src.customer_counting.estimation.sum_estimator import SumEstimator
from src.customer_counting.estimation.separated_estimator import SeparatedEstimator
from src.customer_counting.counter import Counter
from src.customer_counting.tracks_loader import TracksLoader
from shapely.geometry import LineString, Polygon

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--input-video", required=True, help="path to input video")
ap.add_argument("-t", "--tracks-file", required=True, help="path to generated tracks")
ap.add_argument("-vr", "--video_record", required=False, help="if you want to record the video")
args = vars(ap.parse_args())

def entrance_lines():

    #line1 = LineString([(640, 270), (530, 230)])
    line2 = LineString([(590, 320), (510, 410)])
    line3 = LineString([(510, 410), (770, 475)])
    line4 = LineString([(770, 475), (815, 385)])

    lines = [l.coords for l in [line2, line3, line4]]
    lines = [(tuple(map(int, list(p1))), tuple(map(int, list(p2)))) for p1, p2 in lines]

    return lines

def entrance_poly():

    poly = Polygon([(640,270), (530,230), (615,200), (920, 260), (845, 310), (770, 475), (510, 410)])

    return poly

def exit_lines():

    #line1 = LineString([(640, 270), (530, 230)])
    line2 = LineString([(591, 321), (815, 385)])


    lines = [l.coords for l in [line2]]
    lines = [(tuple(map(int, list(p1))), tuple(map(int, list(p2)))) for p1, p2 in lines]

    return lines

def exit_poly():

    poly = Polygon([(591, 321), (815, 385), (815, 185), (591, 121), (591, 321)])

    return poly

def run():
    cap = cv2.VideoCapture(args["input_video"])

    video_name = os.path.splitext(os.path.basename(args["input_video"]))[0]

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter("./" + video_name + ".mp4", fourcc, 20, (1280, 720), True)

    entry_estimation = open(video_name + ".txt", "w")

    #entrance definition
    ent_poly = entrance_poly()
    ent_lines = entrance_lines()
    ex_poly = exit_poly()
    ex_lines = exit_lines()


    tracks_loader = TracksLoader(args["tracks_file"])

    estimator1 = PointCountingEstimator(ent_lines, ent_poly)
    estimator2 = SumEstimator(ent_lines, ent_poly)
    estimator3 = SeparatedEstimator(ent_lines, ent_poly, ex_lines, ex_poly)

    counter = Counter([estimator1, estimator2, estimator3])

    for frame_number, id_to_bbox_map in enumerate(tracks_loader):
        events = counter.count(frame_number, id_to_bbox_map)
        res, frame = cap.read()

        if not res:
            print("Video end!")
            break

        for l in ent_lines:
            cv2.line(frame, l[0], l[1], (0, 0, 255), 3)

        for id, points in id_to_bbox_map.items():
            point1 = (int(points[0][0]), int(points[0][1]))
            point2 = (int(points[1][0]), int(points[1][1]))
            cv2.rectangle(frame, point1, point2, (0, 0, 255), 2)
            cv2.putText(frame, str(id), point1, cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.0, (0, 0, 255))

        cv2.putText(frame, str(int(cap.get(cv2.CAP_PROP_POS_FRAMES))), (170, 130), cv2.FONT_HERSHEY_COMPLEX_SMALL, 3.0,
                    (0, 0, 255))

        out.write(frame)

        cv2.imshow('Tracker', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    event_dict = counter.get_results()

    for id, (frame_number, state) in event_dict.items():
        entry_estimation.write(str(frame_number) + ' ' + state + '\n')


if __name__=="__main__":
    run()
