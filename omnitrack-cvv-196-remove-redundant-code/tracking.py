import os
import argparse
import cv2

from tracking_utils import *
from src.object_detection import Yolov3DarknetDetector
from src.feature_extraction import TripletFeatureExtractor
from src.feature_extraction.utils import squared_euclidean_distance, greedy_match
from src.tracking import MultiIdTracker

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--input_video", required=True, help="path to input video")
ap.add_argument("-g", "--gpu", required=False, help="how many gpus to use")
ap.add_argument("-t", "--tensorflow_tracker", required=False, help="use tensorflow tracker implementation")
ap.add_argument("-d", "--darknet_tracker", required=False, help="use darknet tracker implementation")
ap.add_argument("-do", "--detector_only", required=False, help="use only detector darknet/tensorflow")
ap.add_argument("-vr", "--video_record", required=False, help="if you want to record the video")
ap.add_argument("-de", "--draw_entrance", required=False, help="draw the defined entrance")
args = vars(ap.parse_args())


def run():

    if args["gpu"]:
        print(args["gpu"])
        visible_gpus = args["gpu"]
        set_visible_gpus(str(visible_gpus))

    #for darknet weights
    configPath = "../darknet/cfg/yolov3.cfg"
    weightPath = "../darknet/yolov3.weights"
    metaPath = "../darknet/cfg/coco.data"

    #for tensorflow protobuf
    protobuf_file = './src/parameters/yolov3_cpu_nms.pb'
    detectable_classes_file = './src/parameters/coco.names'
    detection_classes = ['person']

    #for feature extractor
    model_name = 'resnet_v1_50'
    head_name = 'fc1024'
    extractor_weights_file = './src/parameters/market1501_weights/checkpoint-25000'

    detector = Yolov3DarknetDetector(configPath, weightPath, metaPath)
    feature_extractor = TripletFeatureExtractor(model_name, head_name, extractor_weights_file)
    tracker = MultiIdTracker(detector, feature_extractor)

    run_tracking(tracker, args["input_video"], video_record=args["video_record"])

if __name__=="__main__":
    run()




