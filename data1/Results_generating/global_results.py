import argparse
import csv
import os
import re
from itertools import islice

from numpy import mat
from pandas import *


def find_results_of_dataset():
    list_of_files = []

    names_of_directories = os.listdir('..')

    for dir in names_of_directories:
        if os.path.isdir('../' + dir):
            if 'results.txt' in os.listdir('../' + dir):
                list_of_files.append(os.sep.join(['../' + dir, 'results.txt']))

    return list_of_files

def column_names():
    list_of_names = find_results_of_dataset()
    names = [v.split('\\')[0] for v in list_of_names]
    names_of_columns = [v.split('../')[1] for v in  names]

    return names_of_columns


def read_results_content():
    locations_needed = find_results_of_dataset()
    newlist = []

    for location in locations_needed:
        with open(location) as loc:
            lines = loc.read().split('\n')[:7]
            newlist.append(lines)
    return newlist



def dictionary():
    new_list = read_results_content()
    new_dict = {}
    df = DataFrame(new_list)
    df.transpose()
    df.columns = ['1', '2','','','','','']
    df.to_csv('i.csv')
    for a in new_list:

         print(a)


dictionary()

def make_dictionary():
    nl = read_results_content()
    nam = column_names()
    new_dictionary = {}
    new_list = []

    for b in nl:
        dict = {}
        for a in b:

            dict.update({a[0]: a[1]})

    #
    # list2 = [x for x in nel if x != '']
    # list3 = []
    # for a in list2:
    #     c = a.replace('Recall =', '')
    #     d = c.replace('Precision =', '')
    #     e = d.replace('=', ':')
    #     f = e.split(':')
    #
    #     list3.append(f)
    #
    # for a in list3:
    #     n.update({a[0]: a[1]})
    #

    print(n)

  #       for a in b:
  #           c = a.replace('Recall =', '')
  #           d = c.replace('Precision =', '')
  #           e = d.replace('=', ':')
  #           f = e.split(':')
  #           list2 = [x for x in f if x != '']
  #           nel.append(list2)
  #           if f != ['']:
  #               mim = {f[0]: f[1]}
  #               #print(f)
  #               n.update(mim)
  # #  print(n)
  #   nel = [x for x in nel if x != []]
  #   #print(nel)
  #   a = []
  #   for k in nel:
  #       mem = {k[0]: k[1]}
  #       a.append(mem)
    #print(a)
    # df = pandas.DataFrame(a)
    # df = df[['Ground truth', 'Estimations', 'True positives all', 'False positives all', 'Estimations / GT ', ' TP / GT ', ' TP / Estimations ']]
    # df.to_csv('izlaz1.csv', indexlabel=nam)
#make_dictionary()








# def create_pandas_dataframe():
#
#     end_output = {}
#     new_list = read_results_content()
#
#     with open('newfile.txt', 'r') as f:
#         readed_file = f.readlines()
#
#
#
#     names_of_columns = column_names()
#     rows = readed_file
#
#
#     Results = {}
#     #
#     df = pandas.DataFrame(Results, columns=names_of_columns, index=rows)
#
#     df.to_csv('output.csv')
#
#
#
# create_pandas_dataframe()
#
# def write_results_to_csv():
#
#     pass


'''
if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("-dir", "--working_directory", required=False,
                    help="path to directory where all generated videos and "
                         "files are", default=path)
    ap.add_argument("-name", "--estimation", required=True, help="path to ground estimation txt")
    ap.add_argument("-out", "--output", required=True, help="path where to save output files")
    
    args = vars(ap.parse_args())
'''
