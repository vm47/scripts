import argparse
import cv2
import os


def fix(video_path: str, visualize: bool = False, annotated: bool = False) -> None:
    print("Fixing sequential/selective frame fetching... This may take some time.")
    capture = cv2.VideoCapture(video_path)
    if not capture.isOpened():
        capture.release()
        raise FileNotFoundError(f"Can not open {video_path}")

    fps = capture.get(cv2.CAP_PROP_FPS)
    w = int(capture.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    n_frames = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))

    four_cc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    no_ext = os.path.splitext(video_path)[0]
    fixed_path = f"{no_ext}_fixed.avi"
    writer = cv2.VideoWriter(fixed_path, four_cc, fps, (w, h))

    frame_index = 0
    while True:

        if annotated:
            capture.set(cv2.CAP_PROP_POS_FRAMES, frame_index)

        ok, frame = capture.read()
        if not ok:
            break

        writer.write(frame)

        frame_index += 1
        print(f"\r{(frame_index / n_frames) * 100:.2f}%", end="")

        if visualize:
            cv2.imshow("FXR", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    cv2.destroyAllWindows()
    capture.release()
    writer.release()
    print("")
    print("Done!")


def argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description='Fix OpenCV indexing; sequential and selective frame fetching should be the same after the fix')
    parser.add_argument('--input-video', dest='video_path', type=str, metavar='PATH', required=True,
                        help="Location of a video file")
    parser.add_argument('--visualize', dest='visualize', action='store_true',
                        help="Set this flag to visualize frames while iterating through them")
    parser.add_argument('--annotated', dest='annotated', action='store_true',
                        help="Set this flag to fix videos which are already annotated on Overlord")
    return parser


if __name__ == "__main__":
    arguments = argument_parser().parse_args()
    fix(arguments.video_path, arguments.visualize, arguments.annotated)
